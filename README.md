#Envibin

## TODOs
- error handling / global exception handler

## Docker
To build a docker-image run `mvn install dockerfile:build`. Image will be built under envimate/envibin:latest.
Example run: `docker run -p 9000:9000 -t envimate/envibin`.


## Docker-compose
To get everything running properly you'll need to run `docker-compose up` in the `/playground` directory.
This will launch all containers you need for a full setup.
The docker-compose file launches with a docker registry proxied to externalPort 8080 as well.

##cleanup
###delete empty
com.envimate.ubuntu-xenial.vmdk
com.envimate.ubuntu-xenial.vmdk/versions
###delete if meta_data_ is missing
com.envimate.ubuntu-xenial.vmdk/versions/1.0.2


## Tagging

PUT /artifacts/artifact_id/tags/tag_name?version=xyz

```
d com.envimate.ubuntu-xenial.vmdk
d   versions
d      1.0.2
d         labels
f             verified
d          meta_{timestamp}_{size}
f          artifact_id.vmdk
d   tags
l      current  ->     ../versions/1.0.2
l      tested   ->     ../versions/1.0.2
l      tag_name ->     ../versions/1.0.2
```

## Labelling

PUT /artifacts/artifact_id/versions/1.0.2/labels/label_name

```
d com.envimate.ubuntu-xenial.vmdk
d   versions
d      1.0.2
d         labels
f             verified
f             label_name
d          meta_{timestamp}_{size}
f          artifact_id.vmdk
d   tags
l      current  ->     ../versions/1.0.2
l      tested   ->     ../versions/1.0.2
l      tag_name ->     ../versions/1.0.2
```


## Deleting

### label

DELETE /artifacts/artifact_id/versions/1.0.2/labels/label_name

```
d com.envimate.ubuntu-xenial.vmdk
d   versions
d      1.0.2
d         labels
f             verified
d          meta_{timestamp}_{size}
f          artifact_id.vmdk
d   tags
l      current  ->     ../versions/1.0.2
l      tested   ->     ../versions/1.0.2
l      tag_name ->     ../versions/1.0.2
```


### tag

DELETE /artifacts/artifact_id/tags/tag_name

```
d com.envimate.ubuntu-xenial.vmdk
d   versions
d      1.0.2
d         labels
f             verified
d          meta_{timestamp}_{size}
f          artifact_id.vmdk
d   tags
l      current  ->     ../versions/1.0.2
l      tested   ->     ../versions/1.0.2
```

### version

DELETE /artifacts/artifact_id/versions/1.0.2

```
d com.envimate.ubuntu-xenial.vmdk
d   versions
d      1.0.2
d         labels
f             verified
d          meta_{timestamp}_{size}
f          artifact_id.vmdk
d   tags
l      current  ->     ../versions/1.0.2
l      tested   ->     ../versions/1.0.2
```

ERROR - the artifact is referenced as "current" and "tested"

when all tags are removed and the call is attempted again

```
d com.envimate.ubuntu-xenial.vmdk
d   versions
```

### artifact itself

DELETE /artifacts/artifact_id

deletes everything of that artifact even if there are tags

## Downloading

### listing versions
GET /artifacts/artifact_id/versions/

### listing versions with label filter
GET /artifacts/artifact_id/versions?label=tested

1.0.2

### Download a specific version

GET /artifacts/artifact_id/versions/1.0.2

returns the file

### list tags

GET /artifacts/artifact_id/tags

current
released
...

### download a specific TAG or VERSION
GET[BasicAuthHeaders] /artifacts/<ARTIFACT_ID>/presigned/<TAG> || 
GET[BasicAuthHeaders] /artifacts/<ARTIFACT_ID>/presigned/<VERSION> 
-> /<API-KEY>/artifacts/<ARTIFACT_ID>/versions/<VERSION>

GET[BasicAuthHeaders] /artifacts/ubuntu-xenial/presigned/current_stable || 
GET[BasicAuthHeaders] /artifacts/ubuntu-xenial/presigned/1.0.2 
-> /74ba5ce0-ee8f-4f61-94d7-cb8c1a0dc6d8/artifacts/ubuntu-xenial/versions/1.0.2

### list all artifacts

GET /

artifact_id1
artifact_id2
...

### server health

GET /health

## Docker-registry
When run from docker-compose file envibin can also be used directly as a docker-registry.
To verify this works in dev run the following commands:
```
cd playground; docker-compose up -d;
curl localhost:8080/v2/_catalog                                     # Should be empty
docker commit playground_envibin_1 localhost:8080/test/testimate    # Create an image
docker push localhost:8080/test/testimate                           # Push image
curl localhost:8080/v2/_catalog                                     # Should now contain the repository
```


## Cleanup

1. envibin takes 2 parameters on startup - Storage directory path and storage max capacity
2. We keep the artifact version number even
3. later implement label priority
4. Cleanup on each upload


http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#howto-multipart-file-upload-configuration


#modules
envimate-jenkins-cd (CF)
- ECR
    - Policy: CREATE-REPOSITORY <- admin (not backoffice-jenkins-role )
    - Repository: jenkins-with-binaries-docker
        - Policy: - RW <- admin
    - Repository: envimate-jenkins-docker
        - Policy: - RW <- admin
    - Repository: backoffice-jenkins-docker
        - Policy: - RW <- admin
    - Repository: content-unite-jenkins-docker
        - Policy: - RW <- admin
    - Repository: tx-unite-order-service-docker
        - Policy: - RW <- admin
        - Policy: - RW <- backoffice-jenkins-role
    - Repository: frontend-unite-service-docker
        - Policy: - RW <- admin
- backoffice-jenkins
    - envimate-jenkins-infrastructure (CF)
        - Name: Parameter, backoffice-jenkins
        - ECS:
            - backoffice-jenkins-cluster
        - ROLE:
            - backoffice-jenkins-role
    - envimate-jenkins-docker
        -jenkins-with-binaries
- content-unite-jenkins
    - envimate-jenkins-infrastructure (CF)
    - envimate-jenkins-docker
        -jenkins-with-binaries

            -> Tag
Artifact    -> Version
