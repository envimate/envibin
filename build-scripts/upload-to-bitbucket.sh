#!/usr/bin/env bash

set -e
set -u
set -o pipefail

FILE="${1}"
POST_URL="https://api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads"
curl -X POST --user "${BB_AUTH_STRING}" "${POST_URL}" --form files=@"${FILE}"