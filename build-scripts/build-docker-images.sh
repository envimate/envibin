#!/usr/bin/env bash
set -e
set -u
set -o pipefail

project_version() {
    mvn --batch-mode --quiet --define exec.executable="echo" --define exec.args='${project.version}' --non-recursive exec:exec
}

VERSION=""
VERSION="$(project_version)"
mkdir -p target/docker

echo ""
echo "building envibin docker image for version ${VERSION}..."
cp -rp src/main/docker/envibin target/docker
cp -p target/envibin-${VERSION}.jar target/docker/envibin/app.jar
docker build -t "envimate/envibin:${VERSION}" target/docker/envibin/

echo ""
echo "building envibin-lb docker image for version ${VERSION}..."
cp -rp src/main/docker/lb target/docker
docker build -t "envimate/envibin-lb:${VERSION}" target/docker/lb/

docker login -u ${DOCKERHUB_USERNAME} -p ${DOCKERHUB_PASSWORD}
echo "pushing envibin docker image..."
docker push "envimate/envibin:${VERSION}"
echo "pushing envibin-lb docker image..."
docker push "envimate/envibin-lb:${VERSION}"
