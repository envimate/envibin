/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.tags;

import com.envimate.envibin.domain.version.TagNames;
import com.envimate.envibin.domain.version.VersionKey;
import com.envimate.envibin.service.shared.VersionNotFoundException;

public interface TagService {
    void setTags(VersionKey versionKey, TagNames tags) throws VersionNotFoundException;

    void removeTags(VersionKey versionKey, TagNames tags) throws VersionNotFoundException;
}
