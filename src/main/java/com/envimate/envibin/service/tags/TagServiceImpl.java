/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.tags;

import com.envimate.envibin.domain.validators.NotNullValidator;
import com.envimate.envibin.domain.version.TagNames;
import com.envimate.envibin.domain.version.Version;
import com.envimate.envibin.domain.version.VersionKey;
import com.envimate.envibin.domain.version.VersionRepository;
import com.envimate.envibin.infrastructure.neo4j.transactions.Neo4jTransactionTemplate;
import com.envimate.envibin.service.shared.VersionNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TagServiceImpl implements TagService {
    private final Neo4jTransactionTemplate neo4jTransactionTemplate;
    private final VersionRepository versionRepository;

    @Autowired
    TagServiceImpl(final Neo4jTransactionTemplate neo4jTransactionTemplate,
                   final VersionRepository versionRepository) {
        NotNullValidator.validateNotNull(neo4jTransactionTemplate, "serviceTransactionTemplate");
        NotNullValidator.validateNotNull(versionRepository, "versionRepository");
        this.neo4jTransactionTemplate = neo4jTransactionTemplate;
        this.versionRepository = versionRepository;
    }

    @Override
    public void setTags(final VersionKey versionKey, final TagNames tags) throws VersionNotFoundException {
        final Version version = this.neo4jTransactionTemplate.withinTransaction(
                neo4j -> this.versionRepository.byId(neo4j, versionKey)
        ).orElseThrow(VersionNotFoundException::versionNotFoundException);
        version.addTags(tags);
        this.neo4jTransactionTemplate.withinTransaction(neo4j -> this.versionRepository.update(neo4j, version));
    }

    @Override
    public void removeTags(final VersionKey versionKey, final TagNames tags) throws VersionNotFoundException {
        final Version version = this.neo4jTransactionTemplate.withinTransaction(
                neo4j -> this.versionRepository.byId(neo4j, versionKey)
        ).orElseThrow(VersionNotFoundException::versionNotFoundException);
        version.removeTags(tags);
        this.neo4jTransactionTemplate.withinTransaction(neo4j -> this.versionRepository.update(neo4j, version));
    }
}
