/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.health;

import com.envimate.envibin.domain.binary.BinaryRepository;
import com.envimate.envibin.domain.health.HealthStatus;
import com.envimate.envibin.domain.version.VersionRepository;
import com.envimate.envibin.infrastructure.neo4j.transactions.Neo4jTransactionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.stereotype.Component;

import static com.envimate.envibin.service.health.ComponentStatus.fromHealthStatus;

@Component
public final class SystemHealthService {
    private final BinaryRepository binaryRepository;
    private final Neo4jTransactionTemplate neo4jTransactionTemplate;
    private final VersionRepository versionRepository;
    private final BuildProperties buildProperties;

    @Autowired
    SystemHealthService(final BinaryRepository binaryRepository,
                        final Neo4jTransactionTemplate neo4jTransactionTemplate,
                        final VersionRepository versionRepository,
                        final BuildProperties buildProperties) {
        this.binaryRepository = binaryRepository;
        this.neo4jTransactionTemplate = neo4jTransactionTemplate;
        this.versionRepository = versionRepository;
        this.buildProperties = buildProperties;
    }

    public SystemHealth healthStatus() {
        final HealthStatus binaryRepositoryStatus = this.binaryRepository.status();
        final HealthStatus neo4jConnectionStatus = this.neo4jTransactionTemplate.status();
        final HealthStatus versionRepositoryStatus;
        if (!neo4jConnectionStatus.isSick()) {
            versionRepositoryStatus = this.neo4jTransactionTemplate.withinTransaction(
                    this.versionRepository::status
            );
        } else {
            versionRepositoryStatus = this.versionRepository.statusUnknown();
        }
        final SystemHealth systemHealth = SystemHealth.createSystemHealth(
                this.buildProperties.getVersion(),
                fromHealthStatus(binaryRepositoryStatus),
                fromHealthStatus(neo4jConnectionStatus),
                fromHealthStatus(versionRepositoryStatus));
        return systemHealth;
    }
}
