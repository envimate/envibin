/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.health;

import com.envimate.envibin.domain.health.HealthStatus;

public final class ComponentStatus {
    private boolean sick;
    private String name;
    private String message;

    @SuppressWarnings("unused")
    private ComponentStatus() {
    }

    private ComponentStatus(final boolean sick, final String name, final String message) {
        this.sick = sick;
        this.name = name;
        this.message = message;
    }

    static ComponentStatus fromHealthStatus(final HealthStatus healthStatus) {
        return new ComponentStatus(healthStatus.isSick(), healthStatus.componentName(), healthStatus.asString());
    }

    public String getName() {
        return this.name;
    }

    public String getMessage() {
        return this.message;
    }

    public boolean isSick() {
        return this.sick;
    }

    @Override
    public String toString() {
        return "SystemHealth{" +
                "name='" + this.name + '\'' +
                ", message='" + this.message + '\'' +
                '}';
    }
}
