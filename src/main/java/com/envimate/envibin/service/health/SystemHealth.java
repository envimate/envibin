/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.health;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class SystemHealth {
    private String version;
    private boolean healthy;
    private List<ComponentStatus> componentStatuses;

    @SuppressWarnings("unused")
    private SystemHealth() {
    }

    private SystemHealth(final String version, final boolean healthy, final List<ComponentStatus> componentStatuses) {
        this.version = version;
        this.healthy = healthy;
        this.componentStatuses = componentStatuses;
    }

    static SystemHealth createSystemHealth(final String version, final ComponentStatus... componentStatuses) {
        boolean healthy = true;
        for (final ComponentStatus componentStatus : componentStatuses) {
            if (componentStatus.isSick()) {
                healthy = false;
                break;
            }
        }
        return new SystemHealth(version, healthy, Arrays.asList(componentStatuses));
    }

    public boolean isHealthy() {
        return this.healthy;
    }

    public List<ComponentStatus> getComponentStatuses() {
        return Collections.unmodifiableList(this.componentStatuses);
    }

    public String getVersion() {
        return this.version;
    }

    @Override
    public String toString() {
        return "SystemHealth{" +
                "healthy=" + this.healthy +
                ", componentStatuses=" + this.componentStatuses +
                '}';
    }
}
