/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.labels;

import com.envimate.envibin.domain.validators.NotNullValidator;
import com.envimate.envibin.domain.version.LabelNames;
import com.envimate.envibin.domain.version.Version;
import com.envimate.envibin.domain.version.VersionKey;
import com.envimate.envibin.domain.version.VersionRepository;
import com.envimate.envibin.infrastructure.neo4j.transactions.Neo4jTransactionTemplate;
import com.envimate.envibin.service.shared.VersionNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LabelServiceImpl implements LabelService {
    private final Neo4jTransactionTemplate neo4jTransactionTemplate;
    private final VersionRepository versionRepository;

    @Autowired
    LabelServiceImpl(final Neo4jTransactionTemplate neo4jTransactionTemplate,
                     final VersionRepository versionRepository) {
        NotNullValidator.validateNotNull(neo4jTransactionTemplate, "serviceTransactionTemplate");
        NotNullValidator.validateNotNull(versionRepository, "versionRepository");
        this.neo4jTransactionTemplate = neo4jTransactionTemplate;
        this.versionRepository = versionRepository;
    }

    @Override
    public void setLabels(final VersionKey versionKey, final LabelNames labels) throws VersionNotFoundException {
        final Version version = this.neo4jTransactionTemplate.withinTransaction(
                neo4j -> this.versionRepository.byId(neo4j, versionKey)
        ).orElseThrow(VersionNotFoundException::versionNotFoundException);
        version.addLabels(labels);
        this.neo4jTransactionTemplate.withinTransaction(neo4j -> this.versionRepository.update(neo4j, version));
    }

    @Override
    public void removeLabels(final VersionKey versionKey, final LabelNames labels) throws VersionNotFoundException {
        final Version version = this.neo4jTransactionTemplate.withinTransaction(
                neo4j -> this.versionRepository.byId(neo4j, versionKey)
        ).orElseThrow(VersionNotFoundException::versionNotFoundException);
        version.removeLabels(labels);
        this.neo4jTransactionTemplate.withinTransaction(neo4j -> this.versionRepository.update(neo4j, version));
    }
}
