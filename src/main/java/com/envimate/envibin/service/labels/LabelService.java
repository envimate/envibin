/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.labels;

import com.envimate.envibin.domain.version.LabelNames;
import com.envimate.envibin.domain.version.VersionKey;
import com.envimate.envibin.service.shared.VersionNotFoundException;

public interface LabelService {
    void setLabels(VersionKey versionKey, LabelNames labels) throws VersionNotFoundException;

    void removeLabels(VersionKey versionKey, LabelNames labels) throws VersionNotFoundException;
}
