/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.vagrant;

import java.util.List;

@SuppressWarnings("AssignmentOrReturnOfFieldWithMutableType")
public final class VagrantMetaData {
    private String name;
    private String description;
    private List<VagrantVersion> versions;

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public List<VagrantVersion> getVersions() {
        return this.versions;
    }

    public void setVersions(final List<VagrantVersion> versions) {
        this.versions = versions;
    }
}
