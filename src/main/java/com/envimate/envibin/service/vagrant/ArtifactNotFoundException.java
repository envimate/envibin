/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.vagrant;

import com.envimate.envibin.domain.version.ArtifactName;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public final class ArtifactNotFoundException extends Exception {
    private static final long serialVersionUID = 717078200048369249L;

    private ArtifactNotFoundException(final ArtifactName artifactName) {
        super(artifactName.internalStringValueForMapping() + " not found");
    }

    static ArtifactNotFoundException artifactNotFoundException(final ArtifactName artifactName) {
        return new ArtifactNotFoundException(artifactName);
    }
}
