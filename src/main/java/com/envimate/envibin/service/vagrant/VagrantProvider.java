/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.vagrant;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("WeakerAccess")
@JsonInclude(Include.NON_NULL)
public final class VagrantProvider {
    private String name;
    private String url;
    @JsonProperty("checksum_type")
    private String checksumType;
    private String checksum;

    public VagrantProvider() {
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public String getChecksumType() {
        return this.checksumType;
    }

    public void setChecksumType(final String checksumType) {
        this.checksumType = checksumType;
    }

    public String getChecksum() {
        return this.checksum;
    }

    public void setChecksum(final String checksum) {
        this.checksum = checksum;
    }
}
