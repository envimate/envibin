/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.vagrant;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"AssignmentOrReturnOfFieldWithMutableType", "WeakerAccess", "unused"})
public final class VagrantVersion {
    private String version;
    private List<VagrantProvider> providers;

    public VagrantVersion() {
        this.providers = new ArrayList<>(0);
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

    public List<VagrantProvider> getProviders() {
        return this.providers;
    }

    public void setProviders(final List<VagrantProvider> providers) {
        this.providers = providers;
    }
}
