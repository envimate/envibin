/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.vagrant;

import com.envimate.envibin.domain.version.ArtifactName;

public interface VagrantService {
    VagrantMetaData getVagrantMetaData(ArtifactName artifactName) throws ArtifactNotFoundException;
}
