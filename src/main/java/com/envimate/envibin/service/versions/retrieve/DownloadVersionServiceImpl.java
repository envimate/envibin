/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.versions.retrieve;

import com.envimate.envibin.domain.binary.BinaryCarrier;
import com.envimate.envibin.domain.binary.BinaryKey;
import com.envimate.envibin.domain.binary.BinaryRepository;
import com.envimate.envibin.domain.validators.NotNullValidator;
import com.envimate.envibin.domain.version.Version;
import com.envimate.envibin.domain.version.VersionIdentifier;
import com.envimate.envibin.domain.version.VersionInternals;
import com.envimate.envibin.domain.version.VersionRepository;
import com.envimate.envibin.infrastructure.neo4j.transactions.Neo4jTransactionTemplate;
import com.envimate.envibin.service.shared.VersionNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Optional;
import java.util.function.Consumer;

@Component
final class DownloadVersionServiceImpl implements DownloadVersionService {
    private final BinaryRepository binaryRepository;
    private final Neo4jTransactionTemplate neo4jTransactionTemplate;
    private final VersionRepository versionRepository;

    @Autowired
    DownloadVersionServiceImpl(final Neo4jTransactionTemplate neo4jTransactionTemplate,
                               final BinaryRepository binaryRepository,
                               final VersionRepository versionRepository) {
        this.neo4jTransactionTemplate = neo4jTransactionTemplate;
        this.versionRepository = versionRepository;
        NotNullValidator.validateNotNull(neo4jTransactionTemplate, "serviceTransactionTemplate");
        NotNullValidator.validateNotNull(binaryRepository, "binaryRepository");
        NotNullValidator.validateNotNull(versionRepository, "versionRepository");
        this.binaryRepository = binaryRepository;
    }

    @Override
    public void streamContentTo(final VersionIdentifier versionIdentifier,
                                final Consumer<VersionInternals> onVersionResolution,
                                final OutputStream outputStream)
            throws BinaryContentStreamingException, VersionNotFoundException {
        final Optional<Version> versionOptional = this.neo4jTransactionTemplate.withinTransaction(neo4j ->
                this.versionRepository.byVersionIdentifier(neo4j, versionIdentifier)
        );
        final Version version = versionOptional.orElseThrow(VersionNotFoundException::versionNotFoundException);
        onVersionResolution.accept(version.internals());
        final BinaryKey binaryKey = version.binaryKey();
        try {
            BinaryCarrier.transfer(this.binaryRepository.inputStream(binaryKey), outputStream);
        } catch (final IOException e) {
            final VersionInternals internals = version.internals();
            final String message = String.format("Unexpected error streaming binary of %s", internals.versionKey());
            throw new BinaryContentStreamingException(message, e);
        }
    }
}
