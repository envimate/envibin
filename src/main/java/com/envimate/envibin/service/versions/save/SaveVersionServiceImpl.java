/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.versions.save;

import com.envimate.envibin.domain.binary.BinaryInformation;
import com.envimate.envibin.domain.binary.BinaryKey;
import com.envimate.envibin.domain.binary.BinaryRepository;
import com.envimate.envibin.domain.validators.NotNullValidator;
import com.envimate.envibin.domain.version.LabelNames;
import com.envimate.envibin.domain.version.TagNames;
import com.envimate.envibin.domain.version.Version;
import com.envimate.envibin.domain.version.VersionKey;
import com.envimate.envibin.domain.version.VersionRepository;
import com.envimate.envibin.infrastructure.binary.filesystem.BinaryContentProvider;
import com.envimate.envibin.infrastructure.neo4j.transactions.Neo4jTransactionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
final class SaveVersionServiceImpl implements SaveVersionService {
    private final Neo4jTransactionTemplate neo4jTransactionTemplate;
    private final VersionRepository versionRepository;
    private final BinaryRepository binaryRepository;

    @Autowired
    SaveVersionServiceImpl(final Neo4jTransactionTemplate neo4jTransactionTemplate,
                           final VersionRepository versionRepository,
                           final BinaryRepository binaryRepository) {
        NotNullValidator.validateNotNull(neo4jTransactionTemplate, "serviceTransactionTemplate");
        NotNullValidator.validateNotNull(versionRepository, "versionRepository");
        NotNullValidator.validateNotNull(binaryRepository, "binaryRepository");
        this.neo4jTransactionTemplate = neo4jTransactionTemplate;
        this.versionRepository = versionRepository;
        this.binaryRepository = binaryRepository;
    }

    @Override
    public void saveVersion(final VersionKey versionKey,
                            final BinaryContentProvider binaryContentProvider,
                            final LabelNames labelNames,
                            final TagNames tagNames) {
        NotNullValidator.validateNotNull(versionKey, "versionKey");
        NotNullValidator.validateNotNull(binaryContentProvider, "binaryContentProvider");
        NotNullValidator.validateNotNull(labelNames, "labelNames");
        NotNullValidator.validateNotNull(tagNames, "tagNames");
        final Optional<Version> versionOptional = this.neo4jTransactionTemplate.withinTransaction(
                neo4j -> this.versionRepository.byId(neo4j, versionKey)
        );
        final BinaryInformation binaryInformation = this.binaryRepository.save(versionKey, binaryContentProvider);
        if (versionOptional.isPresent()) {
            final Version version = versionOptional.get();
            updateVersion(version, binaryInformation, labelNames, tagNames);
        } else {
            createVersion(versionKey, binaryInformation, labelNames, tagNames);
        }
    }

    private void updateVersion(final Version version, final BinaryInformation binaryInformation,
                               final LabelNames labelNames, final TagNames tagNames) {
        final BinaryKey oldBinaryKey = version.binaryKey();
        version.update(binaryInformation, tagNames, labelNames);
        //Even if there's been an issue with consistency, the issue was fixed by updating the version
        //Hence it wouldn't make sense to fail here from a user perspective
        this.binaryRepository.deleteIfExists(oldBinaryKey);
        this.neo4jTransactionTemplate.withinTransaction(
                neo4j -> this.versionRepository.update(neo4j, version),
                onException -> this.binaryRepository.delete(binaryInformation.binaryKey())
        );
    }

    private void createVersion(final VersionKey versionKey, final BinaryInformation binaryInformation,
                               final LabelNames labelNames, final TagNames tagNames) {
        final Version version = Version.create(versionKey, binaryInformation, tagNames, labelNames);
        this.neo4jTransactionTemplate.withinTransaction(
                neo4j -> this.versionRepository.create(neo4j, version),
                onException -> this.binaryRepository.delete(binaryInformation.binaryKey())
        );
    }
}
