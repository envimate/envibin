/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.versions.list;

import com.envimate.envibin.domain.version.Version;
import com.envimate.envibin.domain.version.VersionInternals;

import java.util.List;

@SuppressWarnings("AssignmentOrReturnOfFieldWithMutableType")
public final class VersionInformation {
    private String artifactName;
    private String versionNumber;
    private List<String> labels;
    private List<String> tags;
    private Long versionTimestampEpochMilliSeconds;

    @SuppressWarnings("unused")
    private VersionInformation() {
    }

    private VersionInformation(final String artifactName, final String versionNumber,
                               final List<String> labels, final List<String> tags,
                               final Long versionTimestampEpochMilliSeconds) {
        this.artifactName = artifactName;
        this.versionNumber = versionNumber;
        this.labels = labels;
        this.tags = tags;
        this.versionTimestampEpochMilliSeconds = versionTimestampEpochMilliSeconds;
    }

    static VersionInformation fromVersion(final Version version) {
        final VersionInternals versionInternals = version.internals();
        return new VersionInformation(
                versionInternals.artifactNameString(),
                versionInternals.versionNumberString(),
                versionInternals.labelNamesAsListOfStrings(),
                versionInternals.tagNamesAsListOfStrings(),
                versionInternals.versionTimestampEpochMilliSeconds()
        );
    }

    public String getArtifactName() {
        return this.artifactName;
    }

    public String getVersionNumber() {
        return this.versionNumber;
    }

    public List<String> getLabels() {
        return this.labels;
    }

    public List<String> getTags() {
        return this.tags;
    }

    public Long getVersionTimestampEpochMilliSeconds() {
        return this.versionTimestampEpochMilliSeconds;
    }

    @Override
    public String toString() {
        return "VersionInformation{" +
                "artifactName='" + this.artifactName + '\'' +
                ", versionNumber='" + this.versionNumber + '\'' +
                ", labels=" + this.labels +
                ", tags=" + this.tags +
                ", versionTimestampEpochMilliSeconds=" + this.versionTimestampEpochMilliSeconds +
                '}';
    }
}
