/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.versions.presigned;

public final class ArtifactNotFoundException extends Exception {
    private static final long serialVersionUID = -7205502486746317959L;

    private ArtifactNotFoundException() {
        super();
    }

    static ArtifactNotFoundException artifactNotFoundException() {
        return new ArtifactNotFoundException();
    }
}
