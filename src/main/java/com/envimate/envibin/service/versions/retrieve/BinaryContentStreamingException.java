/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.versions.retrieve;

import java.io.IOException;

public final class BinaryContentStreamingException extends Exception {
    private static final long serialVersionUID = 612757179546949507L;

    BinaryContentStreamingException(final String message, final IOException cause) {
        super(message, cause);
    }
}
