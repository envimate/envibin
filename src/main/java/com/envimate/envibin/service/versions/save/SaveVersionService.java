/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.versions.save;

import com.envimate.envibin.domain.version.LabelNames;
import com.envimate.envibin.domain.version.TagNames;
import com.envimate.envibin.domain.version.VersionKey;
import com.envimate.envibin.infrastructure.binary.filesystem.BinaryContentProvider;

public interface SaveVersionService {
    void saveVersion(VersionKey versionKey,
                     BinaryContentProvider binaryContentProvider,
                     LabelNames labelNames,
                     TagNames tagNames);
}
