/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.versions.presigned;

import com.envimate.envibin.domain.url.DownloadUrl;
import com.envimate.envibin.domain.url.VersionDownloadUrlBuilder;
import com.envimate.envibin.domain.validators.NotNullValidator;
import com.envimate.envibin.domain.version.Version;
import com.envimate.envibin.domain.version.VersionIdentifier;
import com.envimate.envibin.domain.version.VersionRepository;
import com.envimate.envibin.infrastructure.neo4j.transactions.Neo4jTransactionTemplate;
import com.envimate.envibin.service.shared.VersionNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PresignedArtifactUrlServiceImpl implements PresignedArtifactUrlService {
    private final Neo4jTransactionTemplate neo4jTransactionTemplate;
    private final VersionRepository versionRepository;
    private final VersionDownloadUrlBuilder versionDownloadUrlBuilder;

    public PresignedArtifactUrlServiceImpl(final VersionDownloadUrlBuilder versionDownloadUrlBuilder,
                                           final Neo4jTransactionTemplate neo4jTransactionTemplate,
                                           final VersionRepository versionRepository) {
        this.versionDownloadUrlBuilder = versionDownloadUrlBuilder;
        this.neo4jTransactionTemplate = neo4jTransactionTemplate;
        this.versionRepository = versionRepository;
    }

    @Override
    public DownloadUrl getPresignedArtifactUrl(final VersionIdentifier versionIdentifier)
            throws VersionNotFoundException {
        NotNullValidator.validateNotNull(versionIdentifier, "versionIdentifier");
        final Optional<Version> versionOptional = this.neo4jTransactionTemplate.withinTransaction(neo4j ->
                this.versionRepository.byVersionIdentifier(neo4j, versionIdentifier)
        );
        final Version version = versionOptional.orElseThrow(VersionNotFoundException::versionNotFoundException);
        final DownloadUrl downloadUrl = this.versionDownloadUrlBuilder.downloadUrlFor(version);
        return downloadUrl;
    }
}
