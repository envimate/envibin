/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.versions.retrieve;

import com.envimate.envibin.domain.version.VersionIdentifier;
import com.envimate.envibin.domain.version.VersionInternals;
import com.envimate.envibin.service.shared.VersionNotFoundException;

import java.io.OutputStream;
import java.util.function.Consumer;

public interface DownloadVersionService {
    void streamContentTo(VersionIdentifier versionIdentifier,
                         Consumer<VersionInternals> onVersionResolution,
                         OutputStream outputStream)
            throws BinaryContentStreamingException, VersionNotFoundException;
}
