/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.versions.presigned;

import com.envimate.envibin.domain.url.DownloadUrl;
import com.envimate.envibin.domain.version.VersionIdentifier;
import com.envimate.envibin.service.shared.VersionNotFoundException;

public interface PresignedArtifactUrlService {
    DownloadUrl getPresignedArtifactUrl(VersionIdentifier versionIdentifier) throws VersionNotFoundException;
}
