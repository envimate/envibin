/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.versions.remove;

import com.envimate.envibin.domain.validators.NotNullValidator;
import com.envimate.envibin.domain.version.ArtifactName;
import com.envimate.envibin.domain.version.VersionKey;
import com.envimate.envibin.domain.version.VersionRepository;
import com.envimate.envibin.infrastructure.neo4j.transactions.Neo4jTransactionTemplate;
import org.neo4j.driver.v1.StatementResult;
import org.springframework.stereotype.Component;

@Component
public class RemoveVersionServiceImpl implements RemoveVersionService {
    private final Neo4jTransactionTemplate neo4jTransactionTemplate;
    private final VersionRepository versionRepository;

    RemoveVersionServiceImpl(final Neo4jTransactionTemplate neo4jTransactionTemplate,
                             final VersionRepository versionRepository) {
        NotNullValidator.validateNotNull(neo4jTransactionTemplate, "serviceTransactionTemplate");
        NotNullValidator.validateNotNull(versionRepository, "versionRepository");
        this.neo4jTransactionTemplate = neo4jTransactionTemplate;
        this.versionRepository = versionRepository;
    }

    @Override
    public void removeByArtifactName(final ArtifactName artifactName) {
        NotNullValidator.validateNotNull(artifactName, "artifactName");
        final StatementResult versionOptional = this.neo4jTransactionTemplate
                .withinTransaction(neo4j -> this.versionRepository.deleteByArtifactName(neo4j, artifactName));
    }

    @Override
    public void removeByVersionKey(final VersionKey versionKey) {
        NotNullValidator.validateNotNull(versionKey, "versionKey");
        final StatementResult versionOptional = this.neo4jTransactionTemplate
                .withinTransaction(neo4j -> this.versionRepository.deleteById(neo4j, versionKey));
    }
}
