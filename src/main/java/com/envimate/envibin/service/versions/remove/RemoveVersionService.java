/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.versions.remove;

import com.envimate.envibin.domain.version.ArtifactName;
import com.envimate.envibin.domain.version.VersionKey;

public interface RemoveVersionService {
    void removeByArtifactName(ArtifactName artifactName);

    void removeByVersionKey(VersionKey versionKey);
}
