/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.versions.list;

import com.envimate.envibin.domain.version.ArtifactName;
import com.envimate.envibin.domain.version.LabelNames;

import java.util.List;

public interface ListVersionService {
    List<VersionInformation> findVersions(ArtifactName artifactName, LabelNames labelNames);

    List<VersionInformation> listLatestVersionPerArtifact();
}
