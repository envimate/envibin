/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.versions.list;

import com.envimate.envibin.domain.validators.NotNullValidator;
import com.envimate.envibin.domain.version.ArtifactName;
import com.envimate.envibin.domain.version.LabelNames;
import com.envimate.envibin.domain.version.Version;
import com.envimate.envibin.domain.version.VersionRepository;
import com.envimate.envibin.infrastructure.neo4j.transactions.Neo4jTransactionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
class ListVersionServiceImpl implements ListVersionService {
    private final Neo4jTransactionTemplate neo4jTransactionTemplate;
    private final VersionRepository versionRepository;

    @Autowired
    ListVersionServiceImpl(final Neo4jTransactionTemplate neo4jTransactionTemplate,
                           final VersionRepository versionRepository) {
        NotNullValidator.validateNotNull(neo4jTransactionTemplate, "serviceTransactionTemplate");
        NotNullValidator.validateNotNull(versionRepository, "versionRepository");
        this.neo4jTransactionTemplate = neo4jTransactionTemplate;
        this.versionRepository = versionRepository;
    }

    @Override
    public List<VersionInformation> findVersions(final ArtifactName artifactName,
                                                 final LabelNames labelNames) {
        NotNullValidator.validateNotNull(artifactName, "artifactName");
        NotNullValidator.validateNotNull(labelNames, "labelNames");
        List<Version> versions = this.neo4jTransactionTemplate.withinTransaction(
                neo4j -> this.versionRepository.byArtifactName(neo4j, artifactName)
        );
        if (labelNames.containsLabels()) {
            versions = versions.stream()
                    .filter(version -> version.isLabelledWithAllOf(labelNames))
                    .collect(Collectors.toList());
        }
        final List<VersionInformation> versionInformation = versions.stream()
                .map(VersionInformation::fromVersion)
                .collect(Collectors.toList());
        return versionInformation;
    }

    @Override
    public List<VersionInformation> listLatestVersionPerArtifact() {
        final List<Version> versions = this.neo4jTransactionTemplate.withinTransaction(
                this.versionRepository::lastAddedAndDistinctArtifact
        );
        final List<VersionInformation> versionInformation = versions.stream()
                .map(VersionInformation::fromVersion)
                .collect(Collectors.toList());
        return versionInformation;
    }
}
