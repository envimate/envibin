/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.service.shared;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public final class VersionNotFoundException extends Exception {
    private static final long serialVersionUID = 7939218381864634827L;

    private VersionNotFoundException() {
        super("version not found");
    }

    public static VersionNotFoundException versionNotFoundException() {
        return new VersionNotFoundException();
    }
}
