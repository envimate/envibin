/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.version;

import com.envimate.envibin.domain.binary.BinaryInformation;
import com.envimate.envibin.domain.validators.NotNullValidator;

import java.util.List;
import java.util.stream.Collectors;

public final class VersionInternals {
    private final VersionKey versionKey;
    private final MvccId mvccId;
    private final VersionTimestamp versionTimestamp;
    private final BinaryInformation binaryInformation;
    private final List<Tag> tags;
    private final LabelNames labelNames;
    private final VersionFileName versionFilename;

    private VersionInternals(final VersionKey versionKey,
                             final MvccId mvccId,
                             final VersionTimestamp versionTimestamp,
                             final BinaryInformation binaryInformation,
                             final List<Tag> tags,
                             final LabelNames labelNames,
                             final VersionFileName versionFilename) {
        this.versionKey = versionKey;
        this.mvccId = mvccId;
        this.versionTimestamp = versionTimestamp;
        this.binaryInformation = binaryInformation;
        this.tags = tags;
        this.labelNames = labelNames;
        this.versionFilename = versionFilename;
    }

    static VersionInternals versionInternals(final VersionKey versionKey,
                                             final MvccId mvccId,
                                             final VersionTimestamp versionTimestamp,
                                             final BinaryInformation binaryInformation,
                                             final List<Tag> tags,
                                             final LabelNames labelNames,
                                             final VersionFileName versionFilename) {
        NotNullValidator.validateNotNull(versionKey, "versionKey");
        NotNullValidator.validateNotNull(mvccId, "binaryInformationMvccId");
        NotNullValidator.validateNotNull(versionTimestamp, "versionTimestamp");
        NotNullValidator.validateNotNull(binaryInformation, "binaryInformation");
        NotNullValidator.validateNotNull(tags, "tags");
        NotNullValidator.validateNotNull(labelNames, "labelNames");
        NotNullValidator.validateNotNull(versionFilename, "versionFilename");
        return new VersionInternals(versionKey, mvccId, versionTimestamp, binaryInformation, tags, labelNames, versionFilename);
    }

    public VersionKey versionKey() {
        return this.versionKey;
    }

    String versionKeyString() {
        return this.versionKey.internalStringValueForMapping();
    }

    Long mvccIdLong() {
        return this.mvccId.internalLongValueForMapping();
    }

    Long nextMvccIdLong() {
        return this.mvccId.next().internalLongValueForMapping();
    }

    public Long versionTimestampEpochMilliSeconds() {
        return this.versionTimestamp.epochMilliSeconds();
    }

    String binaryKeyString() {
        return this.binaryInformation.binaryKey().internalStringValueForMapping();
    }

    public String md5ChecksumString() {
        return this.binaryInformation.md5Checksum().internalStringValueForMapping();
    }

    Long binarySizeInBytesLong() {
        return this.binaryInformation.binarySize().internalSizeInBytesValueForMapping();
    }

    public List<String> labelNamesAsListOfStrings() {
        return this.labelNames.asListOfStrings();
    }

    public List<String> tagNamesAsListOfStrings() {
        return this.tags.stream()
                .map(tag -> tag.tagName().internalStringValueForMapping())
                .sorted()
                .collect(Collectors.toList());
    }

    List<TagInternals> tagInternals() {
        return this.tags.stream()
                .map(TagInternals::fromTag)
                .collect(Collectors.toList());
    }

    public String artifactNameString() {
        return this.versionKey.artifactName().internalStringValueForMapping();
    }

    public String versionNumberString() {
        return this.versionKey.versionNumber().internalStringValueForMapping();
    }

    public String versionFilenameString() {
        return this.versionFilename.internalStringValueForMapping();
    }

    @Override
    public String toString() {
        return "VersionInternals{" +
                "versionKey=" + this.versionKey +
                ", mvccId=" + this.mvccId +
                ", versionTimestamp=" + this.versionTimestamp +
                ", binaryInformation=" + this.binaryInformation +
                ", tags=" + this.tags +
                ", labelNames=" + this.labelNames +
                ", versionFilename=" + this.versionFilename +
                '}';
    }
}
