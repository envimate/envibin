/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.version;

import com.envimate.envibin.domain.validators.NotNullValidator;

public final class VersionIdentifier {
    private final ArtifactName artifactName;
    private final VersionNumber versionNumber;
    private final TagName tagName;
    private final VersionNumberOrTagName versionNumberOrTagName;

    private VersionIdentifier(final ArtifactName artifactName,
                              final VersionNumber versionNumber,
                              final TagName tagName,
                              final VersionNumberOrTagName versionNumberOrTagName) {
        this.artifactName = artifactName;
        this.versionNumber = versionNumber;
        this.tagName = tagName;
        this.versionNumberOrTagName = versionNumberOrTagName;
    }

    public static VersionIdentifier versionIdentifier(final ArtifactName artifactName,
                                                      final VersionNumber versionNumber) {
        NotNullValidator.validateNotNull(artifactName, "artifactName");
        NotNullValidator.validateNotNull(versionNumber, "versionNumber");
        return new VersionIdentifier(artifactName, versionNumber, null, null);
    }

    public static VersionIdentifier versionIdentifier(final ArtifactName artifactName,
                                                      final TagName tagName) {
        NotNullValidator.validateNotNull(artifactName, "artifactName");
        NotNullValidator.validateNotNull(tagName, "tagName");
        return new VersionIdentifier(artifactName, null, tagName, null);
    }

    public static VersionIdentifier versionIdentifier(final ArtifactName artifactName,
                                                      final VersionNumberOrTagName versionNumberOrTagName) {
        NotNullValidator.validateNotNull(artifactName, "artifactName");
        NotNullValidator.validateNotNull(versionNumberOrTagName, "versionNumberOrTagName");
        return new VersionIdentifier(artifactName, null, null, versionNumberOrTagName);
    }

    public boolean isVersionNumber() {
        return this.versionNumber != null;
    }

    public boolean isTagName() {
        return this.tagName != null;
    }

    public boolean isVersionNumberOrTagName() {
        return this.versionNumberOrTagName != null;
    }

    public ArtifactName getArtifactName() {
        return this.artifactName;
    }

    public VersionNumber getVersionNumber() {
        return this.versionNumber;
    }

    public TagName getTagName() {
        return this.tagName;
    }

    public VersionNumberOrTagName getVersionNumberOrTagName() {
        return this.versionNumberOrTagName;
    }
}
