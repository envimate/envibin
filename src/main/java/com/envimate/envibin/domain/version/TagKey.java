/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.version;

import com.envimate.envibin.domain.validators.NotNullValidator;

import java.io.Serializable;
import java.util.Objects;

final class TagKey implements Serializable {
    private static final long serialVersionUID = -72317346275621908L;
    private static final String SEPARATOR = "@";
    private final ArtifactName artifactName;
    private final TagName tagName;

    private TagKey(final ArtifactName artifactName, final TagName tagName) {
        this.artifactName = artifactName;
        this.tagName = tagName;
    }

    static TagKey tagKey(final ArtifactName artifactName, final TagName tagName) {
        NotNullValidator.validateNotNull(artifactName, "artifactName");
        NotNullValidator.validateNotNull(tagName, "tagName");
        return new TagKey(artifactName, tagName);
    }

    String internalStringValueForMapping() {
        return String.format("%s%s%s",
                this.artifactName.internalStringValueForMapping(),
                SEPARATOR,
                this.tagName.internalStringValueForMapping());
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final TagKey tagKey = (TagKey) obj;
        return Objects.equals(this.artifactName, tagKey.artifactName) &&
                Objects.equals(this.tagName, tagKey.tagName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.artifactName, this.tagName);
    }

    @Override
    public String toString() {
        return "TagKey{" +
                "artifactName=" + this.artifactName +
                ", tagName=" + this.tagName +
                '}';
    }
}
