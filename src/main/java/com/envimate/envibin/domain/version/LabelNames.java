/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.version;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public final class LabelNames implements Serializable {
    private static final long serialVersionUID = -2111653222104550011L;
    private static final int MAXIMUM_NUMBER_OF_LABEL_NAMES = 20;
    private final List<LabelName> value;

    private LabelNames(final List<LabelName> value) {
        this.value = value;
    }

    public static LabelNames labelNames(final List<LabelName> list) {
        if (list != null) {
            if (list.size() > MAXIMUM_NUMBER_OF_LABEL_NAMES) {
                throw TooManyLabelNamesException.tooManyLabelNamesException(MAXIMUM_NUMBER_OF_LABEL_NAMES);
            }
            return new LabelNames(Collections.unmodifiableList(list));
        } else {
            return new LabelNames(Collections.emptyList());
        }
    }

    public static LabelNames labelNames(final LabelName[] array) {
        if (array != null) {
            if (array.length > MAXIMUM_NUMBER_OF_LABEL_NAMES) {
                throw TooManyLabelNamesException.tooManyLabelNamesException(MAXIMUM_NUMBER_OF_LABEL_NAMES);
            }
            return new LabelNames(Collections.unmodifiableList(Arrays.asList(array)));
        } else {
            return new LabelNames(Collections.emptyList());
        }
    }

    static LabelNames empty() {
        return new LabelNames(Collections.emptyList());
    }

    List<String> asListOfStrings() {
        return this.value.stream()
                .map(LabelName::internalStringValueForMapping)
                .sorted()
                .collect(Collectors.toList());
    }

    public boolean containsLabels() {
        return !this.value.isEmpty();
    }

    boolean containsAllLabels(final LabelNames labelNames) {
        return this.value.containsAll(labelNames.value);
    }

    LabelNames copy() {
        return new LabelNames(new LinkedList<>(this.value));
    }

    @Override
    public int hashCode() {
        return this.value.hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final LabelNames labelNames = (LabelNames) obj;
        return this.value.equals(labelNames.value);
    }

    @Override
    public String toString() {
        return "LabelNames{" +
                "value=" + this.value +
                '}';
    }
}
