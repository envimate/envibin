/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.version;

import com.envimate.envibin.domain.validators.CustomTypeValidationException;

import java.io.Serializable;
import java.util.Objects;
import java.util.function.Supplier;

import static com.envimate.envibin.domain.validators.CustomTypeValidationException.customTypeValidationException;
import static com.envimate.envibin.domain.validators.NotNullValidator.validateNotNull;

public final class VersionNumberOrTagName implements Serializable {
    private static final long serialVersionUID = -2113094893346783464L;
    private final VersionNumber versionNumber;
    private final TagName tagName;

    private VersionNumberOrTagName(final VersionNumber versionNumber, final TagName tagName) {
        this.versionNumber = versionNumber;
        this.tagName = tagName;
    }

    public static VersionNumberOrTagName versionNumberOrTagName(final String versionNumberOrTagName) {
        validateNotNull(versionNumberOrTagName, "versionNumberOrTagName");
        final VersionNumber versionNumber = validCustomTypeOrNull(() -> VersionNumber.versionNumber(versionNumberOrTagName));
        final TagName tagName = validCustomTypeOrNull(() -> TagName.tagName(versionNumberOrTagName));
        if (versionNumber == null && tagName == null) {
            final String message = String.format(
                    "'%s' is not a valid version number nor a valid tag name",
                    versionNumberOrTagName
            );
            throw customTypeValidationException(message);
        }
        return new VersionNumberOrTagName(versionNumber, tagName);
    }

    public boolean isVersionNumber() {
        return this.versionNumber != null;
    }

    public boolean isTagName() {
        return this.tagName != null;
    }

    public VersionNumber versionNumber() {
        return this.versionNumber;
    }

    public TagName tagName() {
        return this.tagName;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final VersionNumberOrTagName that = (VersionNumberOrTagName) obj;
        return Objects.equals(this.versionNumber, that.versionNumber) &&
                Objects.equals(this.tagName, that.tagName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.versionNumber, this.tagName);
    }

    @Override
    public String toString() {
        return "VersionNumberOrTagName{" +
                "versionNumber=" + this.versionNumber +
                ", tagName=" + this.tagName +
                '}';
    }

    private static <T> T validCustomTypeOrNull(final Supplier<T> customTypeFactoryCall) {
        try {
            return customTypeFactoryCall.get();
        } catch (final CustomTypeValidationException ignored) {
            return null;
        }
    }
}
