/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.version;

final class TagInternals {
    private final TagKey tagKey;
    private final TagName tagName;

    private TagInternals(final TagKey tagKey, final TagName tagName) {
        this.tagKey = tagKey;
        this.tagName = tagName;
    }

    static TagInternals fromTag(final Tag tag) {
        return new TagInternals(tag.tagKey(), tag.tagName());
    }

    String tagKeyString() {
        return this.tagKey.internalStringValueForMapping();
    }

    String tagNameString() {
        return this.tagName.internalStringValueForMapping();
    }
}
