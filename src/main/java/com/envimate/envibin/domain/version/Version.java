/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.version;

import com.envimate.envibin.domain.binary.BinaryInformation;
import com.envimate.envibin.domain.binary.BinaryKey;
import com.envimate.envibin.domain.validators.NotNullValidator;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public final class Version {
    private final VersionKey versionKey;
    private final MvccId mvccId;
    private VersionTimestamp versionTimestamp;
    private BinaryInformation binaryInformation;
    private List<Tag> tags;
    private LabelNames labelNames;

    private Version(final VersionKey versionKey,
                    final MvccId mvccId,
                    final VersionTimestamp versionTimestamp,
                    final BinaryInformation binaryInformation,
                    final List<Tag> tags,
                    final LabelNames labelNames) {
        this.versionKey = versionKey;
        this.mvccId = mvccId;
        this.versionTimestamp = versionTimestamp;
        this.binaryInformation = binaryInformation;
        this.tags = tags;
        this.labelNames = labelNames;
    }

    public static Version version(final VersionKey versionKey,
                                  final MvccId mvccId,
                                  final VersionTimestamp versionTimestamp,
                                  final BinaryInformation binaryInformation,
                                  final List<Tag> tags,
                                  final LabelNames labelNames) {
        NotNullValidator.validateNotNull(versionKey, "versionKey");
        NotNullValidator.validateNotNull(mvccId, "binaryInformationMvccId");
        NotNullValidator.validateNotNull(versionTimestamp, "versionTimestamp");
        NotNullValidator.validateNotNull(binaryInformation, "binaryInformation");
        NotNullValidator.validateNotNull(tags, "tags");
        NotNullValidator.validateNotNull(labelNames, "labelNames");
        return new Version(versionKey, mvccId, versionTimestamp, binaryInformation, tags, labelNames);
    }

    public static Version create(final VersionKey versionKey,
                                 final BinaryInformation binaryInformation,
                                 final TagNames tagNames,
                                 final LabelNames labelNames) {
        NotNullValidator.validateNotNull(tagNames, "tagNames");
        final List<Tag> tags = tagNames.mapToTagsOfVersion(versionKey);
        return version(versionKey, MvccId.create(), VersionTimestamp.now(), binaryInformation, tags, labelNames);
    }

    public void addTags(final TagNames tagNames) {
        final List<Tag> tags = tagNames.mapToTagsOfVersion(this.versionKey);
        this.tags.addAll(tags);
    }

    public void removeTags(final TagNames tagNames) {
        final List<Tag> tags = tagNames.mapToTagsOfVersion(this.versionKey);
        this.tags.removeAll(tags);
    }

    public void addLabels(final LabelNames labels) {
        final List<String> current = this.labelNames.asListOfStrings();
        final List<String> toAdd = labels.asListOfStrings();
        current.addAll(toAdd);
        final List<LabelName> labelNames = current.stream().map(LabelName::labelName).collect(Collectors.toList());
        this.labelNames = LabelNames.labelNames(labelNames);
    }

    public void removeLabels(final LabelNames labels) {
        final List<String> current = this.labelNames.asListOfStrings();
        final List<String> toRemove = labels.asListOfStrings();
        current.removeAll(toRemove);
        final List<LabelName> labelNames = current.stream().map(LabelName::labelName).collect(Collectors.toList());
        this.labelNames = LabelNames.labelNames(labelNames);
    }

    public BinaryKey binaryKey() {
        return this.binaryInformation.binaryKey();
    }

    public void update(final BinaryInformation binaryInformation,
                       final TagNames tagNames,
                       final LabelNames labelNames) {
        this.binaryInformation = binaryInformation;
        this.versionTimestamp = VersionTimestamp.now();
        this.tags = tagNames.mapToTagsOfVersion(this.versionKey);
        this.labelNames = labelNames;
    }

    public boolean isLabelledWithAllOf(final LabelNames labelNames) {
        return this.labelNames.containsAllLabels(labelNames);
    }

    public boolean isTaggedWith(final TagName tagName) {
        final Optional<Tag> tagOptional = this.tags.stream()
                .filter(tag -> tag.hasName(tagName))
                .findFirst();
        return tagOptional.isPresent();
    }

    public boolean hasVersionNumber(final VersionNumber versionNumber) {
        return this.versionKey.hasVersionNumber(versionNumber);
    }

    private VersionFileName defaultFileName() {
        return VersionFileName.versionFileName(String.format("%s-%s.%s",
                this.versionKey.artifactName().internalStringValueForMapping(),
                this.versionKey.versionNumber().internalStringValueForMapping(),
                "ova"
        ));
    }

    public VersionInternals internals() {
        return VersionInternals.versionInternals(
                this.versionKey,
                this.mvccId,
                this.versionTimestamp,
                this.binaryInformation,
                new LinkedList<>(this.tags),
                this.labelNames.copy(),
                this.defaultFileName());
    }

    @Override
    public String toString() {
        return "Version{" +
                "versionKey=" + this.versionKey +
                ", mvccId=" + this.mvccId +
                ", versionTimestamp=" + this.versionTimestamp +
                ", binaryInformation=" + this.binaryInformation +
                ", tags=" + this.tags +
                ", labelNames=" + this.labelNames +
                '}';
    }
}
