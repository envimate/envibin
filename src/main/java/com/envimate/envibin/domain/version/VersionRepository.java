/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.version;

import com.envimate.envibin.domain.binary.BinaryInformation;
import com.envimate.envibin.domain.binary.BinaryKey;
import com.envimate.envibin.domain.binary.BinarySize;
import com.envimate.envibin.domain.binary.Md5Checksum;
import com.envimate.envibin.domain.health.HealthStatus;
import com.envimate.envibin.infrastructure.neo4j.transactions.Neo4j;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Statement;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.Value;
import org.neo4j.driver.v1.exceptions.Neo4jException;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.envimate.envibin.domain.version.ConflictEntityDeletedException.conflictEntityDeletedException;
import static com.envimate.envibin.domain.version.ConflictEntityModifiedException.conflictEntityModifiedException;
import static com.envimate.envibin.domain.version.LabelName.labelName;
import static com.envimate.envibin.domain.version.TagName.tagName;
import static org.neo4j.driver.v1.Values.parameters;

@Component
public final class VersionRepository {
    private static final String COMPONENT_NAME = "VersionRepository";

    public Optional<Version> byVersionIdentifier(final Neo4j neo4j, final VersionIdentifier versionIdentifier) {
        final ArtifactName artifactName = versionIdentifier.getArtifactName();
        if (versionIdentifier.isVersionNumber()) {
            final VersionNumber versionNumber = versionIdentifier.getVersionNumber();
            return this.byId(neo4j, VersionKey.versionKey(artifactName, versionNumber));
        } else if (versionIdentifier.isTagName()) {
            final TagName tagName = versionIdentifier.getTagName();
            return byArtifactNameFilteredBy(neo4j, artifactName, version -> version.isTaggedWith(tagName));
        } else {
            final VersionNumberOrTagName versionNumberOrTagName = versionIdentifier.getVersionNumberOrTagName();
            return byArtifactNameFilteredBy(neo4j, artifactName, version ->
                    (
                            versionNumberOrTagName.isVersionNumber() &&
                                    version.hasVersionNumber(versionNumberOrTagName.versionNumber())
                    ) || (
                            versionNumberOrTagName.isTagName() && version.isTaggedWith(versionNumberOrTagName.tagName())
                    ));
        }
    }

    private Optional<Version> byArtifactNameFilteredBy(final Neo4j neo4j,
                                                       final ArtifactName artifactName,
                                                       final Predicate<Version> predicate) {
        final List<Version> versions = byArtifactName(neo4j, artifactName);
        return versions.stream()
                .filter(predicate)
                .findFirst();
    }

    public List<Version> byArtifactName(final Neo4j neo4j, final ArtifactName artifactName) {
        final Statement statement = new Statement("" +
                "MATCH (version:Version) " +
                "WHERE version.versionKey STARTS WITH $artifactName " +
                "OPTIONAL MATCH (version)-[:LABELLED_WITH]->(label:Label) " +
                "OPTIONAL MATCH (version)-[:TAGGED_WITH]->(tags:Tag) " +
                "RETURN version,collect(DISTINCT label) as labels,collect(DISTINCT tags) as tags",
                parameters("artifactName", artifactName.internalStringValueForMapping() + "@"));
        final StatementResult result = neo4j.run(statement);
        return result.list(this::mapResultRecord);
    }

    public List<Version> byArtifactNameStartsWith(final Neo4j neo4j, final ArtifactName artifactName) {
        final Statement statement = new Statement("" +
                "MATCH (version:Version) " +
                "WHERE version.versionKey STARTS WITH $artifactName " +
                "OPTIONAL MATCH (version)-[:LABELLED_WITH]->(label:Label) " +
                "OPTIONAL MATCH (version)-[:TAGGED_WITH]->(tags:Tag) " +
                "RETURN version,collect(DISTINCT label) as labels,collect(DISTINCT tags) as tags",
                parameters("artifactName", artifactName.internalStringValueForMapping()));
        final StatementResult result = neo4j.run(statement);
        return result.list(this::mapResultRecord);
    }

    public List<Version> lastAddedAndDistinctArtifact(final Neo4j neo4j) {
        final Statement statement = new Statement("" +
                "MATCH (version:Version) " +
                "WITH distinct split(version.versionKey, '@')[0] as key, MAX(version.versionTimestampEpochMilliSeconds) " +
                "AS latest " +
                "WITH key, latest " +
                "MATCH (version:Version) " +
                "WHERE version.versionTimestampEpochMilliSeconds = latest " +
                "AND split(version.versionKey, '@')[0] = key " +
                "OPTIONAL MATCH (version)-[:LABELLED_WITH]->(label:Label) " +
                "OPTIONAL MATCH (version)-[:TAGGED_WITH]->(tags:Tag) " +
                "RETURN version, collect(distinct label) AS labels, collect(distinct tags) AS tags");
        final StatementResult result = neo4j.run(statement);
        return result.list(this::mapResultRecord);
    }

    public Optional<Version> byId(final Neo4j neo4j, final VersionKey versionKey) {
        final Map<String, Object> parameters = new HashMap<>(1);
        parameters.put("versionKey", versionKey.internalStringValueForMapping());
        final Statement statement = new Statement("" +
                "MATCH (version:Version {versionKey: $versionKey}) " +
                "OPTIONAL MATCH (version)-[:LABELLED_WITH]->(label:Label) " +
                "OPTIONAL MATCH (version)-[:TAGGED_WITH]->(tags:Tag) " +
                "RETURN version, collect(DISTINCT label) as labels, collect(DISTINCT tags) as tags", parameters);
        final StatementResult result = neo4j.run(statement);
        return mapSingleResult(result);
    }

    public Version create(final Neo4j neo4j, final Version version) {
        final VersionInternals internals = version.internals();
        final Map<String, Object> parameters = mapToParameters(internals);
        neo4j.run("" +
                "CREATE " +
                "(version:Version" +
                "   {" +
                "       versionKey: $versionKey," +
                "       mvccId: $mvccId," +
                "       versionTimestampEpochMilliSeconds: $versionTimestampEpochMilliSeconds," +
                "       binaryKey: $binaryKey," +
                "       md5Checksum: $md5Checksum," +
                "       binarySizeInBytes: $binarySizeInBytes" +
                "   }" +
                ") " +
                "RETURN version", parameters);
        applyLabels(neo4j, internals);
        applyTags(neo4j, internals);
        return byId(neo4j, internals.versionKey()).orElseThrow(() -> {
            final String message = String.format("Unexpected error creating version %s", version);
            return new UnsupportedOperationException(message);
        });
    }

    public Version update(final Neo4j neo4j, final Version version) {
        final VersionInternals internals = version.internals();
        final Map<String, Object> parameters = mapToParameters(internals);
        final Statement statement = new Statement("" +
                "MATCH " +
                "(version:Version {versionKey: $versionKey, mvccId: $mvccId}) " +
                "SET " +
                "   version.mvccId = $newMvccId, " +
                "   version.versionTimestampEpochMilliSeconds = $versionTimestampEpochMilliSeconds, " +
                "   version.binaryKey = $binaryKey, " +
                "   version.md5Checksum = $md5Checksum, " +
                "   version.binarySizeInBytes = $binarySizeInBytes " +
                "RETURN version", parameters);
        final StatementResult result = neo4j.run(statement);
        final Optional<Version> versionOptional = mapSingleResult(result);
        if (!versionOptional.isPresent()) {
            final Optional<Version> conflictingVersion = byId(neo4j, internals.versionKey());
            if (conflictingVersion.isPresent()) {
                throw conflictEntityModifiedException(version, conflictingVersion.get());
            } else {
                throw conflictEntityDeletedException(version);
            }
        }
        applyLabels(neo4j, internals);
        applyTags(neo4j, internals);
        return byId(neo4j, internals.versionKey()).orElseThrow(() -> {
            final String message = String.format("Unexpected error creating version %s", version);
            return new UnsupportedOperationException(message);
        });
    }

    private void applyLabels(final Neo4j neo4j, final VersionInternals internals) {
        final Value parameters = parameters("versionKey", internals.versionKeyString(),
                "labelNames", internals.labelNamesAsListOfStrings());
        neo4j.run("" +
                "MATCH (version:Version {versionKey: $versionKey})-[labelRelation:LABELLED_WITH]->(:Label) " +
                "DELETE labelRelation ", parameters);
        neo4j.run("" +
                "MATCH (version:Version {versionKey: $versionKey}) " +
                "UNWIND {labelNames} as labelName " +
                "MERGE (label:Label {labelName: labelName}) " +
                "MERGE (version)-[:LABELLED_WITH]->(label)", parameters);
        neo4j.run("MATCH (label:Label) " +
                "WHERE NOT (label)<-[:LABELLED_WITH]-(:Version)" +
                "DELETE label");
    }

    private void applyTags(final Neo4j neo4j, final VersionInternals internals) {
        final List<TagInternals> tagInternals = internals.tagInternals();
        final List<Value> tagValueList = tagInternals.stream()
                .map(tagInternal -> parameters("tagKey", tagInternal.tagKeyString(),
                        "tagName", tagInternal.tagNameString()))
                .collect(Collectors.toList());
        final Value parameters = parameters("versionKey", internals.versionKeyString(),
                "artifactName", internals.artifactNameString(),
                "newTagNames", internals.tagNamesAsListOfStrings(),
                "newTags", tagValueList);
        neo4j.run("" +
                "MATCH (version:Version {versionKey: $versionKey})-[currentTagRelation:TAGGED_WITH]->(currentTag:Tag) " +
                "WHERE NOT currentTag.tagName IN {newTagNames} " +
                "DETACH DELETE currentTag", parameters);
        neo4j.run("" +
                "MATCH (otherVersion:Version)-[otherTagRelation:TAGGED_WITH]->(otherTag:Tag) " +
                "WHERE " +
                "otherVersion.versionKey STARTS WITH $artifactName AND " +
                "otherTag.tagName IN {newTagNames} " +
                "DELETE otherTagRelation", parameters);
        neo4j.run("" +
                "MATCH (version:Version {versionKey: $versionKey}) " +
                "UNWIND {newTags} as newTag " +
                "MERGE (tags:Tag {tagKey: newTag.tagKey, tagName: newTag.tagName}) " +
                "MERGE (version)-[:TAGGED_WITH]->(tags)", parameters);
    }

    private Map<String, Object> mapToParameters(final VersionInternals versionInternals) {
        final Map<String, Object> parameters = new HashMap<>(7);
        parameters.put("versionKey", versionInternals.versionKeyString());
        parameters.put("mvccId", versionInternals.mvccIdLong());
        parameters.put("newMvccId", versionInternals.nextMvccIdLong());
        parameters.put("versionTimestampEpochMilliSeconds", versionInternals.versionTimestampEpochMilliSeconds());
        parameters.put("binaryKey", versionInternals.binaryKeyString());
        parameters.put("md5Checksum", versionInternals.md5ChecksumString());
        parameters.put("binarySizeInBytes", versionInternals.binarySizeInBytesLong());
        return parameters;
    }

    private Optional<Version> mapSingleResult(final StatementResult result) {
        if (result.hasNext()) {
            final Record single = result.single();
            return Optional.of(mapResultRecord(single));
        } else {
            return Optional.empty();
        }
    }

    private Version mapResultRecord(final Record single) {
        final Value versionValue = single.get("version");
        final VersionKey versionKey = VersionKey.parse(versionValue.get("versionKey").asString());
        final MvccId mvccId = MvccId.mvccId(versionValue.get("mvccId").asLong());
        final long versionTimestampEpochMilliSeconds = versionValue.get("versionTimestampEpochMilliSeconds").asLong();
        final VersionTimestamp versionTimestamp = VersionTimestamp.timestamp(versionTimestampEpochMilliSeconds);
        final BinaryInformation binaryInformation = BinaryInformation.binaryInformation(
                BinaryKey.parse(versionValue.get("binaryKey").asString()),
                Md5Checksum.parse(versionValue.get("md5Checksum").asString()),
                BinarySize.binarySize(versionValue.get("binarySizeInBytes").asLong())
        );
        final TagNames tagNames;
        if (single.containsKey("tags")) {
            final Value tagsValue = single.get("tags");
            final List<TagName> tagNameList = tagsValue.asList(value -> tagName(value.get("tagName").asString()));
            tagNames = TagNames.tagNames(tagNameList);
        } else {
            tagNames = TagNames.empty();
        }
        final LabelNames labelNames;
        if (single.containsKey("labels")) {
            final Value labelsValue = single.get("labels");
            final List<LabelName> labelNameList = labelsValue.asList(value -> labelName(value.get("labelName").asString()));
            labelNames = LabelNames.labelNames(labelNameList);
        } else {
            labelNames = LabelNames.empty();
        }
        return Version.version(
                versionKey,
                mvccId,
                versionTimestamp,
                binaryInformation,
                tagNames.mapToTagsOfVersion(versionKey),
                labelNames
        );
    }

    public void deleteByArtifactNameStartsWith(final Neo4j neo4j, final ArtifactName artifactName) {
        final Statement statement = new Statement("" +
                "MATCH (version:Version) " +
                "WHERE version.versionKey STARTS WITH $artifactName " +
                "DETACH DELETE version",
                parameters("artifactName", artifactName.internalStringValueForMapping()));
        neo4j.run(statement);
    }

    public StatementResult deleteById(final Neo4j neo4j, final VersionKey versionKey) {
        final Map<String, Object> parameters = new HashMap<>(1);
        parameters.put("versionKey", versionKey.internalStringValueForMapping());
        final Statement statement = new Statement("" +
                "MATCH (version:Version {versionKey: $versionKey}) " +
                "DETACH DELETE version", parameters);
        return neo4j.run(statement);
    }

    public StatementResult deleteByArtifactName(final Neo4j neo4j, final ArtifactName artifactName) {
        final Statement statement = new Statement("" +
                "MATCH (version:Version) " +
                "WHERE version.versionKey STARTS WITH $artifactName " +
                "DETACH DELETE version",
                parameters("artifactName", artifactName.internalStringValueForMapping() + "@"));
        return neo4j.run(statement);
    }

    public HealthStatus status(final Neo4j neo4j) {
        try {
            this.byArtifactName(neo4j, ArtifactName.artifactName("com.envimate.testdata.envibin"));
            return HealthStatus.okay(COMPONENT_NAME);
        } catch (final Neo4jException e) {
            return HealthStatus.sick(COMPONENT_NAME, e);
        }
    }

    public HealthStatus statusUnknown() {
        return HealthStatus.sick(COMPONENT_NAME, new IllegalStateException("Neo4j connection is down"));
    }
}
