/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.version;

final class TooManyLabelNamesException extends RuntimeException {
    private static final long serialVersionUID = 1305727995945695718L;

    private TooManyLabelNamesException(final String message) {
        super(message);
    }

    static TooManyLabelNamesException tooManyLabelNamesException(final int maximumNumberOfLabelNames) {
        final String message = String.format("Too many label names, please provide not more than %s", maximumNumberOfLabelNames);
        return new TooManyLabelNamesException(message);
    }
}
