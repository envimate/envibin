/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.version;

final class ConflictEntityDeletedException extends RuntimeException {
    private static final long serialVersionUID = 6549856280403535802L;

    private ConflictEntityDeletedException(final String message) {
        super(message);
    }

    static ConflictEntityDeletedException conflictEntityDeletedException(final Version version) {
        final String message = String.format("Could not update version %s because the version has been deleted " +
                "in the meantime.", version);
        return new ConflictEntityDeletedException(message);
    }
}
