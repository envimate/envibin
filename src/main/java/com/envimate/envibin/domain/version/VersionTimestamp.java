/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.version;

import java.io.Serializable;
import java.time.Instant;

import static com.envimate.envibin.domain.validators.NotNullValidator.validateNotNull;
import static com.envimate.envibin.domain.validators.PositiveLongValidator.validateIsPositiveLong;

public final class VersionTimestamp implements Serializable {
    private static final long serialVersionUID = -515643829759875609L;
    private final Long value;

    private VersionTimestamp(final Long value) {
        this.value = value;
    }

    static VersionTimestamp now() {
        final Instant now = Instant.now();
        return new VersionTimestamp(now.toEpochMilli());
    }

    public static VersionTimestamp timestamp(final Long epochMilliSecond) {
        validateNotNull(epochMilliSecond, "epochMilliSecond");
        validateIsPositiveLong(epochMilliSecond, "epochMilliSecond");
        return new VersionTimestamp(epochMilliSecond);
    }

    Long epochMilliSeconds() {
        return this.value;
    }

    @Override
    public int hashCode() {
        return this.value.hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final VersionTimestamp that = (VersionTimestamp) obj;
        return this.value.equals(that.value);
    }

    @Override
    public String toString() {
        return "VersionTimestamp{" +
                "value=" + this.value +
                '}';
    }
}
