/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.version;

import java.io.Serializable;
import java.util.regex.Pattern;

import static com.envimate.envibin.domain.validators.NotNullValidator.validateNotNull;
import static com.envimate.envibin.domain.validators.RegexValidator.validateMatchesPattern;

public final class LabelName implements Serializable {
    private static final long serialVersionUID = 2839878477711429029L;
    private static final Pattern PATTERN = Pattern.compile("^[a-zA-Z0-9_\\-.]{1,128}$");
    private final String value;

    private LabelName(final String value) {
        this.value = value;
    }

    public static LabelName labelName(final String labelName) {
        validateNotNull(labelName, "labelName");
        validateMatchesPattern(PATTERN, labelName, "labelName");
        return new LabelName(labelName);
    }

    public String internalStringValueForMapping() {
        return this.value;
    }

    @Override
    public int hashCode() {
        return this.value.hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final LabelName labelName = (LabelName) obj;
        return this.value.equals(labelName.value);
    }

    @Override
    public String toString() {
        return "LabelName{" +
                "value='" + this.value + '\'' +
                '}';
    }
}
