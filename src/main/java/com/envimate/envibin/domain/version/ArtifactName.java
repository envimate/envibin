/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.version;

import java.io.Serializable;
import java.util.regex.Pattern;

import static com.envimate.envibin.domain.validators.NotNullValidator.validateNotNull;
import static com.envimate.envibin.domain.validators.RegexValidator.validateMatchesPattern;

public final class ArtifactName implements Serializable {
    private static final long serialVersionUID = -5433624040264570288L;
    private static final Pattern PATTERN = Pattern.compile("^[a-z0-9_\\-.]{1,128}$");
    private final String value;

    private ArtifactName(final String value) {
        this.value = value;
    }

    public static ArtifactName artifactName(final String artifactId) {
        validateNotNull(artifactId, "artifactId");
        validateMatchesPattern(PATTERN, artifactId, "artifactId");
        return new ArtifactName(artifactId);
    }

    public String internalStringValueForMapping() {
        return this.value;
    }

    @Override
    public int hashCode() {
        return this.value.hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final ArtifactName tagName = (ArtifactName) obj;
        return this.value.equals(tagName.value);
    }

    @Override
    public String toString() {
        return "ArtifactName{" +
                "value='" + this.value + '\'' +
                '}';
    }
}
