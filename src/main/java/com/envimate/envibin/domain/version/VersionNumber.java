/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.version;

import java.io.Serializable;
import java.util.regex.Pattern;

import static com.envimate.envibin.domain.validators.NotNullValidator.validateNotNull;
import static com.envimate.envibin.domain.validators.RegexValidator.validateMatchesPattern;

public final class VersionNumber implements Serializable {
    private static final long serialVersionUID = 640016100849424186L;
    private static final Pattern PATTERN = Pattern.compile("^[a-zA-Z0-9_\\-.]{1,128}$");
    private final String value;

    private VersionNumber(final String value) {
        this.value = value;
    }

    public static VersionNumber versionNumber(final String versionNumber) {
        validateNotNull(versionNumber, "versionNumber");
        validateMatchesPattern(PATTERN, versionNumber, "versionNumber");
        return new VersionNumber(versionNumber);
    }

    public String internalStringValueForMapping() {
        return this.value;
    }

    @Override
    public int hashCode() {
        return this.value.hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final VersionNumber tagName = (VersionNumber) obj;
        return this.value.equals(tagName.value);
    }

    @Override
    public String toString() {
        return "VersionNumber{" +
                "value='" + this.value + '\'' +
                '}';
    }
}
