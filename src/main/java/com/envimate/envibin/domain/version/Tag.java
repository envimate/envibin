/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.version;

import com.envimate.envibin.domain.validators.NotNullValidator;

import java.util.Objects;

final class Tag {
    private final TagKey tagKey;
    private final TagName tagName;

    private Tag(final TagKey tagKey, final TagName tagName) {
        this.tagKey = tagKey;
        this.tagName = tagName;
    }

    static Tag tag(final TagKey tagKey, final TagName tagName) {
        NotNullValidator.validateNotNull(tagKey, "versionKey");
        NotNullValidator.validateNotNull(tagName, "tagName");
        return new Tag(tagKey, tagName);
    }

    TagKey tagKey() {
        return this.tagKey;
    }

    TagName tagName() {
        return this.tagName;
    }

    public boolean hasName(final TagName tagName) {
        return this.tagName.equals(tagName);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Tag tag = (Tag) obj;
        return Objects.equals(this.tagKey, tag.tagKey) &&
                Objects.equals(this.tagName, tag.tagName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.tagKey, this.tagName);
    }
}
