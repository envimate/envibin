/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.version;

final class ConflictEntityModifiedException extends RuntimeException {
    private static final long serialVersionUID = -6008325435981073748L;

    private ConflictEntityModifiedException(final String message) {
        super(message);
    }

    static ConflictEntityModifiedException conflictEntityModifiedException(final Version version,
                                                                           final Version conflictingVersion) {
        final String message = String.format("Could not update version %s because the version has been updated " +
                        "in the meantime. Did not save changes, current state in the database: %s",
                version, conflictingVersion);
        return new ConflictEntityModifiedException(message);
    }
}
