/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.version;

import com.envimate.envibin.domain.validators.NotNullValidator;

import java.io.Serializable;

public final class VersionKey implements Serializable {
    private static final long serialVersionUID = 5809534371732692908L;
    private static final String SEPARATOR = "@";
    private final ArtifactName artifactName;
    private final VersionNumber versionNumber;

    private VersionKey(final ArtifactName artifactName, final VersionNumber versionNumber) {
        this.artifactName = artifactName;
        this.versionNumber = versionNumber;
    }

    public static VersionKey versionKey(final ArtifactName artifactName,
                                        final VersionNumber versionNumber) {
        NotNullValidator.validateNotNull(artifactName, "artifactName");
        NotNullValidator.validateNotNull(versionNumber, "versionNumber");
        return new VersionKey(artifactName, versionNumber);
    }

    public static VersionKey parse(final String versionKeyAsString) {
        NotNullValidator.validateNotNull(versionKeyAsString, "versionKeyAsString");
        final String[] split = versionKeyAsString.split(SEPARATOR);
        if (split.length != 2) {
            throw InvalidVersionKeyException.invalidVersionKeyException(versionKeyAsString);
        }
        final ArtifactName artifactName = ArtifactName.artifactName(split[0]);
        final VersionNumber versionNumber = VersionNumber.versionNumber(split[1]);
        return new VersionKey(artifactName, versionNumber);
    }

    public ArtifactName artifactName() {
        return this.artifactName;
    }

    public VersionNumber versionNumber() {
        return this.versionNumber;
    }

    public String internalStringValueForMapping() {
        final String artifactNameStringValueForMapping = this.artifactName.internalStringValueForMapping();
        final String versionNumberStringValueForMapping = this.versionNumber.internalStringValueForMapping();
        final String value = artifactNameStringValueForMapping + SEPARATOR + versionNumberStringValueForMapping;
        return value;
    }

    @Override
    public String toString() {
        return "VersionKey{" +
                "artifactName=" + this.artifactName +
                ", versionNumber=" + this.versionNumber +
                '}';
    }

    public boolean hasVersionNumber(final VersionNumber versionNumber) {
        return this.versionNumber.equals(versionNumber);
    }

    private static final class InvalidVersionKeyException extends IllegalStateException {
        private static final long serialVersionUID = 7385854870495547225L;

        private InvalidVersionKeyException(final String message) {
            super(message);
        }

        private static InvalidVersionKeyException invalidVersionKeyException(final String invalidValue) {
            final String message = String.format("Invalid binary key value: '%s'. " +
                    "The value must contain the artifact name " +
                    "followed by the separator '@' " +
                    "the version number " +
                    "followed by the separator '@' " +
                    "and the binary checksum.", invalidValue);
            return new InvalidVersionKeyException(message);
        }
    }
}
