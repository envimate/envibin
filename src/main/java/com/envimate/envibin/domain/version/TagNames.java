/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.version;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public final class TagNames implements Serializable {
    private static final long serialVersionUID = -2111653222104550011L;
    private static final int MAXIMUM_NUMBER_OF_TAG_NAMES = 20;
    private final List<TagName> value;

    private TagNames(final List<TagName> value) {
        this.value = value;
    }

    static TagNames tagNames(final List<TagName> list) {
        if (list != null) {
            if (list.size() > MAXIMUM_NUMBER_OF_TAG_NAMES) {
                throw TooManyTagNamesException.tooManyTagNamesException(MAXIMUM_NUMBER_OF_TAG_NAMES);
            }
            return new TagNames(Collections.unmodifiableList(list));
        } else {
            return new TagNames(Collections.emptyList());
        }
    }

    public static TagNames tagNames(final TagName[] array) {
        if (array != null) {
            if (array.length > MAXIMUM_NUMBER_OF_TAG_NAMES) {
                throw TooManyTagNamesException.tooManyTagNamesException(MAXIMUM_NUMBER_OF_TAG_NAMES);
            }
            return new TagNames(Collections.unmodifiableList(Arrays.asList(array)));
        } else {
            return new TagNames(Collections.emptyList());
        }
    }

    static TagNames empty() {
        return new TagNames(Collections.emptyList());
    }

    List<Tag> mapToTagsOfVersion(final VersionKey versionKey) {
        return this.value.stream()
                .map(tagName -> Tag.tag(TagKey.tagKey(versionKey.artifactName(), tagName), tagName))
                .collect(Collectors.toList());
    }

    @Override
    public int hashCode() {
        return this.value.hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final TagNames tagNames = (TagNames) obj;
        return this.value.equals(tagNames.value);
    }

    @Override
    public String toString() {
        return "TagNames{" +
                "value=" + this.value +
                '}';
    }
}
