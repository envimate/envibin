/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.version;

import java.io.Serializable;

import static com.envimate.envibin.domain.validators.NotNullValidator.validateNotNull;

public final class MvccId implements Serializable {
    private static final long serialVersionUID = -5433624040264570288L;
    private final Long value;

    private MvccId(final Long value) {
        this.value = value;
    }

    public static MvccId mvccId(final Long mvccId) {
        validateNotNull(mvccId, "mvccId");
        return new MvccId(mvccId);
    }

    public static MvccId create() {
        return mvccId(0L);
    }

    public Long internalLongValueForMapping() {
        return this.value;
    }

    public MvccId next() {
        return mvccId(this.value + 1L);
    }

    @Override
    public int hashCode() {
        return this.value.hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final MvccId tagName = (MvccId) obj;
        return this.value.equals(tagName.value);
    }

    @Override
    public String toString() {
        return "MvccId{" +
                "value=" + this.value +
                '}';
    }
}
