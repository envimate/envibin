/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.binary;

import com.envimate.envibin.domain.validators.CustomTypeValidationException;
import com.envimate.envibin.domain.validators.NotNullValidator;

import java.io.Serializable;
import java.util.regex.Pattern;

public final class Md5Checksum implements Serializable {
    private static final long serialVersionUID = -5492854516195650163L;
    private static final Pattern MD5_REGEX = Pattern.compile("[a-fA-F0-9]{32}");
    private final String value;

    private Md5Checksum(final String value) {
        this.value = value;
    }

    public static Md5Checksum parse(final String checksum) {
        NotNullValidator.validateNotNull(checksum, "checksum");
        if (MD5_REGEX.matcher(checksum).matches()) {
            return new Md5Checksum(checksum);
        } else {
            throw CustomTypeValidationException.customTypeValidationException(
                    String.format("%s is not a valid md5 checksum", checksum));
        }
    }

    public String internalStringValueForMapping() {
        return this.value;
    }

    @Override
    public int hashCode() {
        return this.value.hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Md5Checksum that = (Md5Checksum) obj;
        return this.value.equals(that.value);
    }

    @Override
    public String toString() {
        return "Md5Checksum{" +
                "value='" + this.value + '\'' +
                '}';
    }
}
