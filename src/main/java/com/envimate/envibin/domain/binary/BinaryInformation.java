/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.binary;

import com.envimate.envibin.domain.validators.NotNullValidator;

public final class BinaryInformation {
    private final BinaryKey binaryKey;
    private final Md5Checksum md5Checksum;
    private final BinarySize binarySize;

    private BinaryInformation(final BinaryKey binaryKey, final Md5Checksum md5Checksum, final BinarySize binarySize) {
        this.binaryKey = binaryKey;
        this.md5Checksum = md5Checksum;
        this.binarySize = binarySize;
    }

    public static BinaryInformation binaryInformation(final BinaryKey binaryKey,
                                                      final Md5Checksum md5Checksum,
                                                      final BinarySize binarySize) {
        NotNullValidator.validateNotNull(binaryKey, "binaryKey");
        NotNullValidator.validateNotNull(md5Checksum, "md5Checksum");
        NotNullValidator.validateNotNull(binarySize, "binarySize");
        return new BinaryInformation(binaryKey, md5Checksum, binarySize);
    }

    public BinaryKey binaryKey() {
        return this.binaryKey;
    }

    public Md5Checksum md5Checksum() {
        return this.md5Checksum;
    }

    public BinarySize binarySize() {
        return this.binarySize;
    }
}
