/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.binary;

import com.envimate.envibin.domain.health.HealthStatus;
import com.envimate.envibin.domain.version.VersionKey;
import com.envimate.envibin.infrastructure.binary.filesystem.BinaryContentProvider;

import java.io.InputStream;

public interface BinaryRepository {
    BinaryInformation save(VersionKey versionKey, BinaryContentProvider binaryContentProvider);

    InputStream inputStream(BinaryKey binaryKey);

    void deleteIfExists(BinaryKey binaryKey);

    void delete(BinaryKey binaryKey);

    HealthStatus status();
}
