/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.binary;

import com.envimate.envibin.domain.validators.PositiveLongValidator;

import java.io.Serializable;

import static com.envimate.envibin.domain.validators.NotNullValidator.validateNotNull;

public final class BinarySize implements Serializable {
    private static final long serialVersionUID = -8798998487786615401L;
    private final Long value;

    private BinarySize(final Long value) {
        this.value = value;
    }

    public static BinarySize binarySize(final Long fileSizeInBytes) {
        validateNotNull(fileSizeInBytes, "fileSizeInBytes");
        PositiveLongValidator.validateIsPositiveLong(fileSizeInBytes, "fileSizeInBytes");
        return new BinarySize(fileSizeInBytes);
    }

    public Long internalSizeInBytesValueForMapping() {
        return this.value;
    }

    @Override
    public int hashCode() {
        return this.value.hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final BinarySize tagName = (BinarySize) obj;
        return this.value.equals(tagName.value);
    }

    @Override
    public String toString() {
        return "FileSize{" +
                "value='" + this.value + '\'' +
                '}';
    }
}
