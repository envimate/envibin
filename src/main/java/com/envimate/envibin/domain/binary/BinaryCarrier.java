/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.binary;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

public final class BinaryCarrier {
    private static final int BUFFER_SIZE = 1024 * 1024;

    private BinaryCarrier() {
    }

    public static void transfer(final InputStream inputStream, final OutputStream outputStream) throws IOException {
        try (
                ReadableByteChannel inputChannel = Channels.newChannel(inputStream);
                WritableByteChannel outputChannel = Channels.newChannel(outputStream)
        ) {
            final ByteBuffer buffer = ByteBuffer.allocateDirect(BUFFER_SIZE);
            while (inputChannel.read(buffer) != -1) {
                buffer.flip();
                outputChannel.write(buffer);
                buffer.compact();
            }
            buffer.flip();
            while (buffer.hasRemaining()) {
                outputChannel.write(buffer);
            }
        }
    }
}
