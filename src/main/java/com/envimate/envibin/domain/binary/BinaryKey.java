/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.binary;

import com.envimate.envibin.domain.validators.NotNullValidator;
import com.envimate.envibin.domain.version.VersionKey;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public final class BinaryKey implements Serializable {
    private static final long serialVersionUID = 5809534371732692908L;
    private static final String SEPARATOR = "@";
    private final VersionKey versionKey;
    private final UUID uuid;

    private BinaryKey(final VersionKey versionKey, final UUID uuid) {
        this.versionKey = versionKey;
        this.uuid = uuid;
    }

    public static BinaryKey createNew(final VersionKey versionKey) {
        NotNullValidator.validateNotNull(versionKey, "versionKey");
        return new BinaryKey(versionKey, UUID.randomUUID());
    }

    public static BinaryKey parse(final String binaryKeyAsString) {
        NotNullValidator.validateNotNull(binaryKeyAsString, "binaryKeyAsString");
        final String[] split = binaryKeyAsString.split(SEPARATOR);
        final int md5ChecksumStart = binaryKeyAsString.lastIndexOf(SEPARATOR);
        final String versionKeyString = binaryKeyAsString.substring(0, md5ChecksumStart);
        final VersionKey versionKey = VersionKey.parse(versionKeyString);
        final UUID uuid = UUID.fromString(split[split.length - 1]);
        return new BinaryKey(versionKey, uuid);
    }

    public String internalStringValueForMapping() {
        final String value = String.format("%s%s%s",
                this.versionKey.internalStringValueForMapping(),
                SEPARATOR,
                this.uuid.toString());
        return value;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final BinaryKey binaryKey = (BinaryKey) obj;
        return Objects.equals(this.versionKey, binaryKey.versionKey) &&
                Objects.equals(this.uuid, binaryKey.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.versionKey, this.uuid);
    }

    @Override
    public String toString() {
        return "BinaryKey{" +
                "versionKey=" + this.versionKey +
                ", uuid=" + this.uuid +
                '}';
    }
}
