/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.url;

import java.io.Serializable;

import static com.envimate.envibin.domain.validators.RequiredStringValidator.validateNotNullNorEmpty;

public final class DownloadUrl implements Serializable {
    private static final long serialVersionUID = 8395779165503622383L;
    private final String value;

    private DownloadUrl(final String value) {
        this.value = value;
    }

    static DownloadUrl downloadUrl(final String value) {
        validateNotNullNorEmpty(value, "value");
        return new DownloadUrl(value);
    }

    public String asString() {
        return this.value;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final DownloadUrl that = (DownloadUrl) obj;
        return this.value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return this.value.hashCode();
    }

    @Override
    public String toString() {
        return "DownloadUrl{" +
                "value='" + this.value + '\'' +
                '}';
    }
}
