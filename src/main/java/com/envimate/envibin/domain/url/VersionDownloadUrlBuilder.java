/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.url;

import com.envimate.envibin.domain.validators.NotNullValidator;
import com.envimate.envibin.domain.version.Version;
import com.envimate.envibin.domain.version.VersionInternals;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;

public final class VersionDownloadUrlBuilder implements Serializable {
    private static final long serialVersionUID = 8395779165503622383L;
    private final ExternalProtocol externalProtocol;
    private final ExternalHostAddress externalHostAddress;
    private final ExternalPort externalPort;

    private VersionDownloadUrlBuilder(final ExternalProtocol externalProtocol,
                                      final ExternalHostAddress externalHostAddress,
                                      final ExternalPort externalPort) {
        this.externalProtocol = externalProtocol;
        this.externalHostAddress = externalHostAddress;
        this.externalPort = externalPort;
    }

    public static VersionDownloadUrlBuilder externalServiceAddress(final ExternalProtocol externalProtocol,
                                                                   final ExternalHostAddress externalHostAddress,
                                                                   final ExternalPort externalPort) {
        NotNullValidator.validateNotNull(externalProtocol, "externalProtocol");
        NotNullValidator.validateNotNull(externalHostAddress, "externalHostAddress");
        NotNullValidator.validateNotNull(externalPort, "externalPort");
        return new VersionDownloadUrlBuilder(externalProtocol, externalHostAddress, externalPort);
    }

    public DownloadUrl downloadUrlFor(final Version version) {
        final String fragment = null;
        final String query = null;
        final String userInfo = null;
        final VersionInternals internals = version.internals();
        final String path = String.format("/%s/versions/%s", internals.artifactNameString(), internals.versionNumberString());
        try {
            final URI uri = new URI(
                    this.externalProtocol.lookupAsString(),
                    userInfo,
                    this.externalHostAddress.lookupAsString(),
                    this.externalPort.lookupAsInteger(),
                    path,
                    query, fragment);
            return DownloadUrl.downloadUrl(uri.toASCIIString());
        } catch (final URISyntaxException e) {
            final String message = String.format("Could not build a valid download uri using " +
                            "external.protocol: '%s', external.address: '%s', external.externalPort: '%s' " +
                            " for version %s",
                    this.externalHostAddress.lookupAsString(),
                    this.externalPort.lookupAsInteger(),
                    this.externalProtocol.lookupAsString(),
                    version);
            throw new IllegalStateException(message, e);
        }
    }
}
