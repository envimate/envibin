/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.health;

public final class HealthStatus {
    private final String componentName;
    private final Exception exception;

    private HealthStatus(final String componentName, final Exception exception) {
        this.componentName = componentName;
        this.exception = exception;
    }

    public String asString() {
        if (this.exception == null) {
            return String.format("%s: OK", this.componentName);
        } else {
            final String exceptionStackTrace = exceptionStackTrace(this.exception);
            return String.format("%s: SICK: %s", this.componentName, exceptionStackTrace);
        }
    }

    public boolean isSick() {
        return this.exception != null;
    }

    public String componentName() {
        return this.componentName;
    }

    public static HealthStatus sick(final String componentName, final Exception exception) {
        return new HealthStatus(componentName, exception);
    }

    public static HealthStatus okay(final String componentName) {
        return new HealthStatus(componentName, null);
    }

    private static String exceptionStackTrace(final Exception exception) {
        Throwable currentlyRendering = exception;
        final StringBuilder traceBuilder = new StringBuilder(currentlyRendering.toString() + "\n");
        for (final StackTraceElement e1 : currentlyRendering.getStackTrace()) {
            traceBuilder.append("\t at ").append(e1.toString()).append("\n");
        }
        while (currentlyRendering.getCause() != null) {
            currentlyRendering = currentlyRendering.getCause();
            traceBuilder.append("Cause by: ").append(currentlyRendering.toString()).append("\n");
            for (final StackTraceElement e1 : currentlyRendering.getStackTrace()) {
                traceBuilder.append("\t at ").append(e1.toString()).append("\n");
            }
        }
        return traceBuilder.toString();
    }
}
