/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.validators;

import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.envimate.envibin.domain.validators.CustomTypeValidationException.customTypeValidationException;

public final class RegexValidator {
    private RegexValidator() {
    }

    public static void validateMatchesPattern(final Pattern pattern, final String value, final String name) {
        final Matcher matcher = pattern.matcher(value);
        if (!matcher.matches()) {
            throw customTypeValidationException(name + " must match pattern " + pattern);
        }
    }

    public static <T> T validateMatchesPattern(final Pattern pattern, final String value, final String name,
                                               final Function<Matcher, T> matcherProcessor) {
        final Matcher matcher = pattern.matcher(value);
        if (!matcher.matches()) {
            throw customTypeValidationException(name + " must match pattern " + pattern);
        }
        return matcherProcessor.apply(matcher);
    }
}
