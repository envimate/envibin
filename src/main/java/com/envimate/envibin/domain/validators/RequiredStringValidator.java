/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.validators;

import static com.envimate.envibin.domain.validators.CustomTypeValidationException.customTypeValidationException;

public final class RequiredStringValidator {
    private RequiredStringValidator() {
    }

    public static void validateNotNullNorEmpty(final String value, final String name) {
        if (value == null || value.trim().isEmpty()) {
            throw customTypeValidationException(name + " must not be empty");
        }
    }
}
