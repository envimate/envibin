/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.validators;

public final class CustomTypeValidationException extends IllegalArgumentException {
    private static final long serialVersionUID = -5137712128096384694L;

    private CustomTypeValidationException(final String s) {
        super(s);
    }

    public static CustomTypeValidationException customTypeValidationException(final String message) {
        return new CustomTypeValidationException(message);
    }
}
