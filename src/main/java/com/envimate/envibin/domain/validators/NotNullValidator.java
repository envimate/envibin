/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.validators;

import static com.envimate.envibin.domain.validators.CustomTypeValidationException.customTypeValidationException;

public final class NotNullValidator {
    private NotNullValidator() {
    }

    public static void validateNotNull(final Object value, final String name) {
        if (value == null) {
            throw customTypeValidationException(name + " must not be null");
        }
    }
}
