/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.validators;

import static com.envimate.envibin.domain.validators.CustomTypeValidationException.customTypeValidationException;

public final class PositiveLongValidator {
    private PositiveLongValidator() {
    }

    public static void validateIsPositiveLong(final Long value, final String name) {
        if (!(value > 0L)) {
            throw customTypeValidationException(name + "(" + value + ") must be a positive, non overflowing long");
        }
    }

    public static Long validateIsPositiveLong(final String value, final String name) {
        final Long longValue = Long.parseLong(value);
        validateIsPositiveLong(longValue, name);
        return longValue;
    }
}
