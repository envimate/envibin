/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.validators;

import java.util.function.Supplier;

public final class ValidCustomTypeOrNull {
    private ValidCustomTypeOrNull() {
    }

    public static <T> T validCustomTypeOrNull(final Supplier<T> customTypeFactoryCall) {
        try {
            return customTypeFactoryCall.get();
        } catch (final CustomTypeValidationException ignored) {
            return null;
        }
    }
}
