/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.filesystem;

import java.util.Objects;

import static com.envimate.envibin.domain.validators.NotNullValidator.validateNotNull;

public final class DirectoryName {
    private final String value;

    private DirectoryName(final String value) {
        this.value = value;
    }

    public static DirectoryName directoryName(final String name) {
        validateNotNull(name, "name");
        return new DirectoryName(name);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final DirectoryName that = (DirectoryName) obj;
        return Objects.equals(this.value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.value);
    }

    @Override
    public String toString() {
        return "DirectoryName{" +
                "value='" + this.value + '\'' +
                '}';
    }
}
