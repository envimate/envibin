/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.filesystem;

import java.io.IOException;

public final class FileCouldNotBeDeletedException extends RuntimeException {
    private static final long serialVersionUID = -7621975465712550022L;

    FileCouldNotBeDeletedException(final String message, final IOException cause) {
        super(message, cause);
    }
}
