/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.filesystem;

import java.nio.file.Path;

public final class FileSystemRootDirectoryReachedException extends RuntimeException {
    private static final long serialVersionUID = -4456108410982488812L;

    private FileSystemRootDirectoryReachedException(final String message) {
        super(message);
    }

    static FileSystemRootDirectoryReachedException fileSystemRootDirectoryReachedException(final Path lastPathElement) {
        return new FileSystemRootDirectoryReachedException(String.format("'%s' has no parent directory", lastPathElement));
    }
}
