/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.filesystem;

import com.envimate.envibin.domain.binary.BinarySize;
import com.envimate.envibin.domain.binary.Md5Checksum;
import com.envimate.envibin.infrastructure.binary.filesystem.UnexpectedBinaryRepositoryException;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

public final class ExistingFile {
    private final Path path;

    private ExistingFile(final Path path) {
        this.path = path;
    }

    public static ExistingFile forAbsolutePathString(final String absolutePathString) {
        final Path path = Paths.get(absolutePathString);
        return forPath(path);
    }

    public static ExistingFile forPath(final Path path) {
        if (!Files.exists(path)) {
            throw new IllegalArgumentException(path + " does not exist");
        }
        if (!Files.isRegularFile(path)) {
            throw new IllegalArgumentException(path + " is not a regular file");
        }
        final Path absolutePath = path.toAbsolutePath();
        return new ExistingFile(absolutePath);
    }

    public ExistingDirectory directory() {
        final Path parent = this.path.getParent();
        return ExistingDirectory.forPath(parent);
    }

    public String asAbsolutePathString() {
        return this.path.toString();
    }

    public boolean isExecutable() {
        return Files.isExecutable(this.path);
    }

    public BinarySize determineFileSize() throws UnexpectedBinaryRepositoryException {
        try {
            final long size = Files.size(this.path);
            return BinarySize.binarySize(size);
        } catch (final IOException e) {
            throw new UnsupportedOperationException(String.format("Could not read file size of '%s'", this.path), e);
        }
    }

    public Md5Checksum determineFileChecksum() throws UnexpectedBinaryRepositoryException {
        try {
            final String md5Sum = Md5SumFromFile.md5SumFromFile(this.path);
            return Md5Checksum.parse(md5Sum);
        } catch (final NoSuchAlgorithmException | IOException e) {
            throw new UnsupportedOperationException(String.format("Could not create md5 sum of '%s'", this.path), e);
        }
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final ExistingFile that = (ExistingFile) obj;
        return Objects.equals(this.path, that.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.path);
    }

    @Override
    public String toString() {
        return "ExistingFile{" +
                "path=" + this.path +
                '}';
    }

    public void rename(final String newName)
            throws UnexpectedBinaryRepositoryException {
        try {
            Files.move(
                    this.path,
                    this.path.getParent().resolve(newName),
                    StandardCopyOption.ATOMIC_MOVE,
                    StandardCopyOption.REPLACE_EXISTING);
        } catch (final IOException e) {
            throw new UnsupportedOperationException(String.format("Could not mv tmp file to its final name: %s", this.path), e);
        }
    }

    public InputStream openInputStream() {
        try {
            return Files.newInputStream(this.path);
        } catch (final IOException e) {
            final String message = String.format("Unexpected error opening input stream of %s", this.path);
            throw new UnsupportedOperationException(message, e);
        }
    }

    public void delete() {
        try {
            Files.delete(this.path);
        } catch (final IOException e) {
            final String message = String.format("Unexpected error deleting file %s", this.path);
            throw new FileCouldNotBeDeletedException(message, e);
        }
    }
}
