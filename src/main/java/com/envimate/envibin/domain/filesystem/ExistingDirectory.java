/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.filesystem;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.envimate.envibin.domain.filesystem.FileSystemRootDirectoryReachedException.fileSystemRootDirectoryReachedException;

public final class ExistingDirectory {
    private final Path path;

    private ExistingDirectory(final Path path) {
        this.path = path;
    }

    public static ExistingDirectory forPath(final Path path) {
        if (!Files.exists(path)) {
            throw new IllegalArgumentException(path + " does not exist");
        }
        if (!Files.isDirectory(path)) {
            throw new IllegalArgumentException(path + " is not a directory");
        }
        final Path absolutePath = path.toAbsolutePath();
        return new ExistingDirectory(absolutePath);
    }

    public ExistingDirectory existingSubdirectory(final String name) {
        final Path subDirectoryPath = this.path.resolve(name);
        return forPath(subDirectoryPath);
    }

    public Directory subdirectory(final String name) {
        final Path subdirectoryPath = this.path.resolve(name);
        return Directory.forPath(subdirectoryPath);
    }

    public ExistingDirectory parentDirectory() {
        final Path parent = this.path.getParent();
        if (parent == null) {
            throw fileSystemRootDirectoryReachedException(this.path);
        }
        return forPath(parent);
    }

    public ExistingFile existingFile(final String glob) {
        final PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:" + glob);
        try {
            final List<Path> matchedFiles = Files.walk(this.path, 1)
                    .filter(filePath -> matcher.matches(filePath.getFileName()))
                    .collect(Collectors.toList());
            if (matchedFiles.size() != 1) {
                final String errorMessage;
                if (matchedFiles.size() == 0) {
                    errorMessage = String.format("Glob %s did not match any files in the directory %s",
                            glob,
                            this.path);
                } else {
                    final String matchedFileNames = matchedFiles.stream()
                            .map(path -> path.toAbsolutePath().toString())
                            .collect(Collectors.joining(System.lineSeparator()));
                    errorMessage = String.format("Glob %s matched multiple files in directory %s. Found: %s",
                            glob,
                            this.path,
                            matchedFileNames);
                }
                throw new IllegalArgumentException(errorMessage);
            }
            return ExistingFile.forPath(matchedFiles.get(0).toAbsolutePath());
        } catch (final IOException e) {
            final String errorMessage = String.format("Could not find file in the directory %s, matching file glob %s",
                    this.path,
                    glob);
            throw new UnsupportedOperationException(errorMessage, e);
        }
    }

    public File asFile() {
        return this.path.toFile();
    }

    public String asAbsolutePathString() {
        return this.path.toString();
    }

    public DirectoryName name() {
        final Path fileName = this.path.getFileName();
        if (fileName == null) {
            throw new UnsupportedOperationException(String.format("Path %s has zero elements", this.path));
        }
        final String name = fileName.toString();
        return DirectoryName.directoryName(name);
    }

    public NonExistingFile tempFile() {
        final String uuid = UUID.randomUUID().toString();
        final Path tempFilePath = this.asFile().toPath().resolve(uuid);
        return NonExistingFile.nonExistingFile(tempFilePath);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final ExistingDirectory that = (ExistingDirectory) obj;
        return Objects.equals(this.path, that.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.path);
    }

    @Override
    public String toString() {
        return "ExistingDirectory{" +
                "path=" + this.path +
                '}';
    }

    public void deleteChildIfExists(final String filename) {
        try {
            Files.deleteIfExists(this.path.resolve(filename));
        } catch (final IOException e) {
            final String message = String.format("Unexpected error deleting file %s of binary key %s", this.path, filename);
            throw new UnsupportedOperationException(message, e);
        }
    }
}
