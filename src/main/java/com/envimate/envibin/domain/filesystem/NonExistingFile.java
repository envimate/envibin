/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.filesystem;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public final class NonExistingFile {
    private static final int MEGABYTE = 1048576;
    private final Path path;

    private NonExistingFile(final Path path) {
        this.path = path;
    }

    static NonExistingFile nonExistingFile(final Path path) {
        if (Files.exists(path)) {
            final String errorMessage = String.format("The file '%s' exists even though it shouldn't exist", path);
            throw new IllegalStateException(errorMessage);
        }
        return new NonExistingFile(path);
    }

    public ExistingFile createWithContent(final InputStream inputStream) {
        try (FileOutputStream fos = new FileOutputStream(this.path.toFile())) {
            final byte[] buffer = new byte[MEGABYTE];
            int bytesRead;
            //read from is to buffer
            //noinspection NestedAssignment
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                fos.write(buffer, 0, bytesRead);
            }
            inputStream.close();
            //flush OutputStream to write any buffered data to file
            fos.flush();
        } catch (final IOException e) {
            throw new FileCouldNotBeCreatedException(String.format("Failed to create file from stream: %s", this.path), e);
        }
        return ExistingFile.forPath(this.path);
    }

    public ExistingFile createWithContent(final String content) {
        return createWithContent(new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8)));
    }
}
