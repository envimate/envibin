/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.filesystem;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

final class Md5SumFromFile {
    private static final int MD5_CALCULATION_BUFFER_SIZE = 20 * 1024 * 1024;
    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

    private Md5SumFromFile() {
    }

    public static String md5SumFromFile(final Path path) throws NoSuchAlgorithmException, IOException {
        final MessageDigest md5Digest = MessageDigest.getInstance("MD5");
        try (FileInputStream fileInputStream = new FileInputStream(path.toFile());
             ReadableByteChannel inputChannel = Channels.newChannel(fileInputStream)) {
            final ByteBuffer buffer = ByteBuffer.allocateDirect(MD5_CALCULATION_BUFFER_SIZE);
            while (inputChannel.read(buffer) != -1) {
                buffer.flip();
                md5Digest.update(buffer);
                buffer.compact();
            }
            buffer.flip();
            while (buffer.hasRemaining()) {
                md5Digest.update(buffer);
            }
        }
        final byte[] digestBytes = md5Digest.digest();
        return bytesToHex(digestBytes);
    }

    @SuppressWarnings("MagicNumber")
    private static String bytesToHex(final byte[] bytes) {
        final char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            final int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }
}
