/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.domain.filesystem;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Objects;

public final class Directory {
    private final Path path;

    private Directory(final Path path) {
        this.path = path;
    }

    static Directory forPath(final Path path) {
        if (Files.exists(path) && !Files.isDirectory(path)) {
            throw new IllegalArgumentException(path + " is not a directory");
        }
        final Path absolutePath = path.toAbsolutePath();
        return new Directory(absolutePath);
    }

    public Directory subdirectory(final String name) {
        return forPath(this.path.resolve(name));
    }

    public void deleteRecursiveIfExists() {
        final File file = this.path.toFile();
        if (file.exists()) {
            try {
                Files.walk(this.path)
                        .sorted(Comparator.reverseOrder())
                        .map(Path::toFile)
                        .forEach(fileToDelete -> {
                            final boolean success = fileToDelete.delete();
                            if (!success) {
                                final String absolutePath = fileToDelete.getAbsolutePath();
                                final String message = "Could not delete file '" + absolutePath + "'";
                                throw new UnsupportedOperationException(message);
                            }
                        });
            } catch (final IOException e) {
                throw new UnsupportedOperationException("Could not recursively delete directory " + this.path, e);
            }
        }
    }

    public ExistingDirectory createDirectoryTree() {
        try {
            Files.createDirectories(this.path);
            return ExistingDirectory.forPath(this.path);
        } catch (final IOException e) {
            throw new UnsupportedOperationException("Could not create directory " + this.path, e);
        }
    }

    public String asString() {
        return this.path.toString();
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Directory that = (Directory) obj;
        return Objects.equals(this.path, that.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.path);
    }

    @Override
    public String toString() {
        return "Directory{" +
                "path=" + this.path +
                '}';
    }
}
