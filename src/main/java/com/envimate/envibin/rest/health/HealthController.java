/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.rest.health;

import com.envimate.envibin.service.health.SystemHealth;
import com.envimate.envibin.service.health.SystemHealthService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthController {
    private final SystemHealthService systemHealthService;

    public HealthController(final SystemHealthService systemHealthService) {
        this.systemHealthService = systemHealthService;
    }

    @RequestMapping(
            path = "/health",
            method = RequestMethod.GET
    )
    public ResponseEntity<SystemHealth> health() {
        final SystemHealth systemHealth = this.systemHealthService.healthStatus();
        final HttpStatus httpStatus;
        if (systemHealth.isHealthy()) {
            httpStatus = HttpStatus.OK;
        } else {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        final ResponseEntity<SystemHealth> systemHealthResponseEntity = new ResponseEntity<>(systemHealth, httpStatus);
        return systemHealthResponseEntity;
    }
}
