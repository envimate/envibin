/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.rest.tags;

import com.envimate.envibin.domain.version.ArtifactName;
import com.envimate.envibin.domain.version.TagName;
import com.envimate.envibin.domain.version.TagNames;
import com.envimate.envibin.domain.version.VersionKey;
import com.envimate.envibin.domain.version.VersionNumber;
import com.envimate.envibin.service.shared.VersionNotFoundException;
import com.envimate.envibin.service.tags.TagService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TagController {
    private final TagService tagService;

    public TagController(final TagService tagService) {
        this.tagService = tagService;
    }

    @RequestMapping(
            path = "/{artifact_name}/versions/{version_number}/tags",
            method = RequestMethod.PUT
    )
    public void setTags(@PathVariable("artifact_name") final ArtifactName artifactName,
                        @PathVariable("version_number") final VersionNumber versionNumber,
                        @RequestParam("tag") final TagName[] tags) throws VersionNotFoundException {
        final VersionKey versionKey = VersionKey.versionKey(artifactName, versionNumber);
        final TagNames tagNames = TagNames.tagNames(tags);
        this.tagService.setTags(versionKey, tagNames);
    }

    @RequestMapping(
            path = "/{artifact_name}/versions/{version_number}/tags",
            method = RequestMethod.DELETE
    )
    public void removeTags(@PathVariable("artifact_name") final ArtifactName artifactName,
                           @PathVariable("version_number") final VersionNumber versionNumber,
                           @RequestParam("tag") final TagName[] tags) throws VersionNotFoundException {
        final VersionKey versionKey = VersionKey.versionKey(artifactName, versionNumber);
        final TagNames tagNames = TagNames.tagNames(tags);
        this.tagService.removeTags(versionKey, tagNames);
    }
}
