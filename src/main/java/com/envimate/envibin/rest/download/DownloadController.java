/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.rest.download;

import com.envimate.envibin.domain.version.ArtifactName;
import com.envimate.envibin.domain.version.TagName;
import com.envimate.envibin.domain.version.VersionIdentifier;
import com.envimate.envibin.domain.version.VersionNumber;
import com.envimate.envibin.domain.version.VersionNumberOrTagName;
import com.envimate.envibin.service.shared.VersionNotFoundException;
import com.envimate.envibin.service.versions.retrieve.BinaryContentStreamingException;
import com.envimate.envibin.service.versions.retrieve.DownloadVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

@RestController
public class DownloadController {
    private static final int NOT_FOUND = 404;
    private final DownloadVersionService downloadVersionService;

    @Autowired
    public DownloadController(final DownloadVersionService downloadVersionService) {
        this.downloadVersionService = downloadVersionService;
    }

    @RequestMapping(path = "/{artifact_name}/versions/{version_number}", method = RequestMethod.GET)
    public void downloadByArtifactNameAndVersionNumber(
            @PathVariable("artifact_name") final ArtifactName artifactName,
            @PathVariable("version_number") final VersionNumber versionNumber,
            final HttpServletResponse response)
            throws BinaryContentStreamingException {
        final VersionIdentifier versionIdentifier = VersionIdentifier.versionIdentifier(artifactName, versionNumber);
        serverDownload(response, versionIdentifier);
    }

    @RequestMapping(path = "/{artifact_name}/tags/{tag_name}", method = RequestMethod.GET)
    public void downloadByArtifactNameAndVersionNumber(
            @PathVariable("artifact_name") final ArtifactName artifactName,
            @PathVariable("tag_name") final TagName tagName,
            final HttpServletResponse response)
            throws BinaryContentStreamingException {
        final VersionIdentifier versionIdentifier = VersionIdentifier.versionIdentifier(artifactName, tagName);
        serverDownload(response, versionIdentifier);
    }

    @RequestMapping(path = "/{artifact_name}/{version_number_or_tag}", method = RequestMethod.GET)
    public void downloadByArtifactNameAndVersionNumber(
            @PathVariable("artifact_name") final ArtifactName artifactName,
            @PathVariable("version_number_or_tag") final VersionNumberOrTagName versionNumberOrTagName,
            final HttpServletResponse response)
            throws BinaryContentStreamingException {
        final VersionIdentifier versionIdentifier = VersionIdentifier.versionIdentifier(artifactName, versionNumberOrTagName);
        serverDownload(response, versionIdentifier);
    }

    private void serverDownload(final HttpServletResponse httpServletResponse, final VersionIdentifier versionIdentifier)
            throws BinaryContentStreamingException {
        final ServletOutputStream outputStream = outputStream(httpServletResponse);
        try {
            this.downloadVersionService.streamContentTo(versionIdentifier,
                    versionInternals -> {
                        final String fileName = toUrlDecodedFileName(versionInternals.versionFilenameString());
                        httpServletResponse.setContentType("application/x-msdownload");
                        httpServletResponse.setHeader("Content-disposition", "attachment; filename=" + fileName);
                    },
                    outputStream);
        } catch (final VersionNotFoundException e) {
            sendError(httpServletResponse, NOT_FOUND, "Version not found");
        }
    }

    private String toUrlDecodedFileName(final String fileName) {
        try {
            final String urlEncodedFilename = URLEncoder.encode(fileName, "UTF-8");
            final String urlDecodedFileName = URLDecoder.decode(urlEncodedFilename, "ISO8859_1");
            return urlDecodedFileName;
        } catch (final UnsupportedEncodingException e) {
            throw new UnsupportedOperationException("Could not find a default encoding, that should not happen", e);
        }
    }

    private void sendError(final HttpServletResponse response, final int code, final String message) {
        try {
            response.sendError(code, message);
        } catch (final IOException e) {
            throw new UnsupportedOperationException("Unexpected error dealing with http response io", e);
        }
    }

    private ServletOutputStream outputStream(final HttpServletResponse response) {
        try {
            return response.getOutputStream();
        } catch (final IOException e) {
            throw new UnsupportedOperationException("Unexpected error accessing servlet response output stream", e);
        }
    }
}
