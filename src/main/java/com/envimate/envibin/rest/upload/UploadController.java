/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.rest.upload;

import com.envimate.envibin.domain.version.ArtifactName;
import com.envimate.envibin.domain.version.LabelName;
import com.envimate.envibin.domain.version.LabelNames;
import com.envimate.envibin.domain.version.TagName;
import com.envimate.envibin.domain.version.TagNames;
import com.envimate.envibin.domain.version.VersionKey;
import com.envimate.envibin.domain.version.VersionNumber;
import com.envimate.envibin.infrastructure.binary.filesystem.BinaryContentProvider;
import com.envimate.envibin.service.versions.save.SaveVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartFile;

import static com.envimate.envibin.infrastructure.binary.filesystem.BinaryContentProvider.binaryContentProvider;

@RestController
public class UploadController {
    private final SaveVersionService saveVersionService;

    @Autowired
    public UploadController(final SaveVersionService saveVersionService) {
        this.saveVersionService = saveVersionService;
    }

    @RequestMapping(
            path = "/{artifact_name}/versions/{version_number}",
            method = RequestMethod.PUT
    )
    public void upload(
            @PathVariable("artifact_name") final ArtifactName artifactName,
            @PathVariable("version_number") final VersionNumber versionNumber,
            @RequestParam("file") final MultipartFile uploadedFile,
            @RequestParam(value = "tag", required = false) final TagName[] tags,
            @RequestParam(value = "label", required = false) final LabelName[] labels) {
        final VersionKey versionKey = VersionKey.versionKey(artifactName, versionNumber);
        final TagNames tagNames = TagNames.tagNames(tags);
        final LabelNames labelNames = LabelNames.labelNames(labels);
        final BinaryContentProvider binaryBinaryContentProvider = binaryContentProvider(uploadedFile::getInputStream);
        this.saveVersionService.saveVersion(versionKey, binaryBinaryContentProvider, labelNames, tagNames);
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Current request is not a multipart request")
    @ExceptionHandler(MultipartException.class)
    public void conflict() {
        // Nothing to do
    }
}
