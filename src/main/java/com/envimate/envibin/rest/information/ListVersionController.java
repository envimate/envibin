/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.rest.information;

import com.envimate.envibin.domain.version.ArtifactName;
import com.envimate.envibin.domain.version.LabelName;
import com.envimate.envibin.domain.version.LabelNames;
import com.envimate.envibin.domain.version.TagName;
import com.envimate.envibin.service.versions.list.ListVersionService;
import com.envimate.envibin.service.versions.list.VersionInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public final class ListVersionController {
    private final ListVersionService listVersionService;

    @Autowired
    public ListVersionController(final ListVersionService listVersionService) {
        this.listVersionService = listVersionService;
    }

    @RequestMapping(
            path = "/",
            method = RequestMethod.GET
    )
    public List<VersionInformation> listLatestVersionPerArtifact() {
        return this.listVersionService.listLatestVersionPerArtifact();
    }

    @RequestMapping(
            path = {"/{artifact_name}/versions", "/{artifact_name}/versions/"},
            method = RequestMethod.GET
    )
    public List<VersionInformation> versions(@PathVariable("artifact_name") final ArtifactName artifactName,
                                             @RequestParam(value = "tag", required = false) final TagName[] tags,
                                             @RequestParam(value = "label", required = false) final LabelName[] labels) {
        return this.listVersionService.findVersions(artifactName, LabelNames.labelNames(labels));
    }
}
