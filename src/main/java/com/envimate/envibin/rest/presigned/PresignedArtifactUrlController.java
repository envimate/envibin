/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.rest.presigned;

import com.envimate.envibin.domain.url.DownloadUrl;
import com.envimate.envibin.domain.version.ArtifactName;
import com.envimate.envibin.domain.version.TagName;
import com.envimate.envibin.domain.version.VersionIdentifier;
import com.envimate.envibin.domain.version.VersionNumber;
import com.envimate.envibin.domain.version.VersionNumberOrTagName;
import com.envimate.envibin.service.shared.VersionNotFoundException;
import com.envimate.envibin.service.versions.presigned.PresignedArtifactUrlService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PresignedArtifactUrlController {
    private final PresignedArtifactUrlService presignedArtifactUrlService;

    public PresignedArtifactUrlController(final PresignedArtifactUrlService presignedArtifactUrlService) {
        this.presignedArtifactUrlService = presignedArtifactUrlService;
    }

    @RequestMapping(path = "/{artifact_name}/presigned/{version_number_or_tag}", method = RequestMethod.GET)
    public String getPresignedArtifactUrl(@PathVariable("artifact_name") final ArtifactName artifactName,
                                          @PathVariable("version_number_or_tag") final VersionNumberOrTagName versionOrTag)
            throws VersionNotFoundException {
        final VersionIdentifier versionIdentifier = VersionIdentifier.versionIdentifier(artifactName, versionOrTag);
        return downloadUrlFor(versionIdentifier);
    }

    @RequestMapping(path = "/{artifact_name}/presigned/versions/{version_number}", method = RequestMethod.GET)
    public String getPresignedArtifactUrl(@PathVariable("artifact_name") final ArtifactName artifactName,
                                          @PathVariable("version_number") final VersionNumber versionNumber)
            throws VersionNotFoundException {
        final VersionIdentifier versionIdentifier = VersionIdentifier.versionIdentifier(artifactName, versionNumber);
        return downloadUrlFor(versionIdentifier);
    }

    @RequestMapping(path = "/{artifact_name}/presigned/tags/{tagName}", method = RequestMethod.GET)
    public String getPresignedArtifactUrl(@PathVariable("artifact_name") final ArtifactName artifactName,
                                          @PathVariable("tagName") final TagName tagName)
            throws VersionNotFoundException {
        final VersionIdentifier versionIdentifier = VersionIdentifier.versionIdentifier(artifactName, tagName);
        return downloadUrlFor(versionIdentifier);
    }

    private String downloadUrlFor(final VersionIdentifier versionIdentifier) throws VersionNotFoundException {
        final DownloadUrl artifactUrl = this.presignedArtifactUrlService.getPresignedArtifactUrl(versionIdentifier);
        return artifactUrl.asString();
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No version found for version number or tag name")
    @ExceptionHandler(VersionNotFoundException.class)
    public void notFound() {
        // Nothing to do
    }
}
