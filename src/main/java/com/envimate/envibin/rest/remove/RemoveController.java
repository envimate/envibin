/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.rest.remove;

import com.envimate.envibin.domain.version.ArtifactName;
import com.envimate.envibin.domain.version.VersionKey;
import com.envimate.envibin.domain.version.VersionNumber;
import com.envimate.envibin.service.versions.remove.RemoveVersionService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RemoveController {
    private final RemoveVersionService removeVersionService;

    public RemoveController(final RemoveVersionService removeVersionService) {
        this.removeVersionService = removeVersionService;
    }

    @RequestMapping(
            path = "/{artifact_name}",
            method = RequestMethod.DELETE
    )
    public void removeArtifact(@PathVariable("artifact_name") final ArtifactName artifactName) {
        this.removeVersionService.removeByArtifactName(artifactName);
    }

    @RequestMapping(
            path = "/{artifact_name}/versions/{version_number}",
            method = RequestMethod.DELETE
    )
    public void removeVersion(@PathVariable("artifact_name") final ArtifactName artifactName,
                              @PathVariable("version_number") final VersionNumber versionNumber) {
        this.removeVersionService.removeByVersionKey(VersionKey.versionKey(artifactName, versionNumber));
    }
}
