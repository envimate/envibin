/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.rest.vagrant;

import com.envimate.envibin.domain.version.ArtifactName;
import com.envimate.envibin.service.vagrant.ArtifactNotFoundException;
import com.envimate.envibin.service.vagrant.VagrantMetaData;
import com.envimate.envibin.service.vagrant.VagrantService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MetaDataController {
    private final VagrantService vagrantService;

    public MetaDataController(final VagrantService vagrantService) {
        this.vagrantService = vagrantService;
    }

    @RequestMapping(
            path = "/{artifact_name}/vagrant/metadata",
            method = RequestMethod.GET
    )
    public VagrantMetaData getMetadata(@PathVariable("artifact_name") final ArtifactName artifactName)
            throws ArtifactNotFoundException {
        return this.vagrantService.getVagrantMetaData(artifactName);
    }
}
