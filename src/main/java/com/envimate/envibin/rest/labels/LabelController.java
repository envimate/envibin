/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.rest.labels;

import com.envimate.envibin.domain.version.ArtifactName;
import com.envimate.envibin.domain.version.LabelName;
import com.envimate.envibin.domain.version.LabelNames;
import com.envimate.envibin.domain.version.VersionKey;
import com.envimate.envibin.domain.version.VersionNumber;
import com.envimate.envibin.service.labels.LabelService;
import com.envimate.envibin.service.shared.VersionNotFoundException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LabelController {
    private final LabelService labelService;

    public LabelController(final LabelService labelService) {
        this.labelService = labelService;
    }

    @RequestMapping(
            path = "/{artifact_name}/versions/{version_number}/labels",
            method = RequestMethod.PUT
    )
    public void setLabels(@PathVariable("artifact_name") final ArtifactName artifactName,
                          @PathVariable("version_number") final VersionNumber versionNumber,
                          @RequestParam("label") final LabelName[] labels) throws VersionNotFoundException {
        final VersionKey versionKey = VersionKey.versionKey(artifactName, versionNumber);
        final LabelNames labelNames = LabelNames.labelNames(labels);
        this.labelService.setLabels(versionKey, labelNames);
    }

    @RequestMapping(
            path = "/{artifact_name}/versions/{version_number}/labels",
            method = RequestMethod.DELETE
    )
    public void removeLabels(@PathVariable("artifact_name") final ArtifactName artifactName,
                             @PathVariable("version_number") final VersionNumber versionNumber,
                             @RequestParam("label") final LabelName[] labels) throws VersionNotFoundException {
        final VersionKey versionKey = VersionKey.versionKey(artifactName, versionNumber);
        final LabelNames labelNames = LabelNames.labelNames(labels);
        this.labelService.removeLabels(versionKey, labelNames);
    }
}
