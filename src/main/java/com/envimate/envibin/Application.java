/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin;

import com.envimate.envibin.infrastructure.spring.Neo4jConfig;
import com.envimate.envibin.infrastructure.spring.UserConfig;
import com.envimate.envibin.infrastructure.spring.WebMvcConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SuppressWarnings({"UtilityClassWithoutPrivateConstructor", "NonFinalUtilityClass"})
@SpringBootApplication
@Import({Neo4jConfig.class, UserConfig.class, WebMvcConfiguration.class})
public class Application {
    @SuppressWarnings("resource")
    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
