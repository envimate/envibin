/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.infrastructure.spring;

import com.envimate.envibin.domain.filesystem.ExistingDirectory;
import com.envimate.envibin.domain.url.ExternalHostAddress;
import com.envimate.envibin.domain.url.ExternalPort;
import com.envimate.envibin.domain.url.ExternalProtocol;
import com.envimate.envibin.domain.url.VersionDownloadUrlBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.nio.file.Paths;

@Configuration
public class UserConfig {
    @Bean
    @Qualifier("binaryRepositoryFileSystemDirectory")
    public ExistingDirectory binaryRepositoryFileSystemDirectory(@Value("${binaries.path}") final String binariesPath) {
        return ExistingDirectory.forPath(Paths.get(binariesPath));
    }

    @Bean
    public VersionDownloadUrlBuilder externalAddress(final ExternalProtocol protocol,
                                                     final ExternalHostAddress externalHostAddress,
                                                     final ExternalPort externalPort) {
        return VersionDownloadUrlBuilder.externalServiceAddress(protocol, externalHostAddress, externalPort);
    }

    @Bean
    public ExternalProtocol externalProtocol(final ApplicationContext applicationContext) {
        return () -> applicationContext.getEnvironment().getProperty("external.protocol");
    }

    @Bean
    public ExternalHostAddress externalHostAddress(final ApplicationContext applicationContext) {
        return () -> applicationContext.getEnvironment().getProperty("external.address");
    }

    @Bean
    public ExternalPort externalPort(final ApplicationContext applicationContext) {
        return () -> Integer.parseInt(applicationContext.getEnvironment().getProperty("external.port"));
    }
}
