/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.infrastructure.spring;

import com.envimate.envibin.domain.version.ArtifactName;
import com.envimate.envibin.domain.version.LabelName;
import com.envimate.envibin.domain.version.TagName;
import com.envimate.envibin.domain.version.VersionNumber;
import com.envimate.envibin.domain.version.VersionNumberOrTagName;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@SuppressWarnings("CanBeFinal")
@Configuration
public class WebMvcConfiguration extends WebMvcConfigurationSupport {
    @Override
    protected void configurePathMatch(final PathMatchConfigurer configurer) {
        super.configurePathMatch(configurer);
        configurer.setUseSuffixPatternMatch(false);
    }

    @Override
    protected void addFormatters(final FormatterRegistry registry) {
        registry.addConverter(String.class, ArtifactName.class, ArtifactName::artifactName);
        registry.addConverter(String.class, VersionNumber.class, VersionNumber::versionNumber);
        registry.addConverter(String.class, TagName.class, TagName::tagName);
        registry.addConverter(String.class, LabelName.class, LabelName::labelName);
        registry.addConverter(String.class, VersionNumberOrTagName.class, VersionNumberOrTagName::versionNumberOrTagName);
        super.addFormatters(registry);
    }
}
