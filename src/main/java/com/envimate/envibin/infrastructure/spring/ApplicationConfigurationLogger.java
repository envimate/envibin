/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.infrastructure.spring;

import com.envimate.envibin.domain.url.ExternalHostAddress;
import com.envimate.envibin.domain.url.ExternalPort;
import com.envimate.envibin.domain.url.ExternalProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
final class ApplicationConfigurationLogger implements ApplicationListener<ApplicationReadyEvent> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationConfigurationLogger.class);
    private final ExternalProtocol externalProtocol;
    private final ExternalHostAddress externalHostAddress;
    private final ExternalPort externalPort;
    private final String neo4jUrl;
    private final String binariesPath;

    @Autowired
    ApplicationConfigurationLogger(final ExternalProtocol externalProtocol,
                                   final ExternalHostAddress externalHostAddress,
                                   final ExternalPort externalPort,
                                   @Value("${neo4j.url}") final String neo4jUrl,
                                   @Value("${binaries.path}") final String binariesPath
    ) {
        this.externalProtocol = externalProtocol;
        this.externalHostAddress = externalHostAddress;
        this.externalPort = externalPort;
        this.neo4jUrl = neo4jUrl;
        this.binariesPath = binariesPath;
    }

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        LOGGER.info("Envibin configuration: " +
                        "binaries.path:{}, " +
                        "neo4j.url:{}, " +
                        "external.protocol:{}, " +
                        "external.address:{}," +
                        "server.externalPort:{}",
                this.binariesPath,
                this.neo4jUrl,
                this.externalProtocol.lookupAsString(),
                this.externalHostAddress.lookupAsString(),
                this.externalPort.lookupAsInteger()
        );
    }
}
