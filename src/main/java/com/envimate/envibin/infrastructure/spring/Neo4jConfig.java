/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.infrastructure.spring;

import org.neo4j.driver.v1.Config;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Neo4jConfig {
    private static final int MAX_IDLE_SESSIONS = 10;

    @Bean(destroyMethod = "close")
    public Driver ne4jDriver(@Value("${neo4j.url}") final String neo4jUrl) {
        final Config config = Config.build()
                .withLeakedSessionsLogging()
                .withMaxIdleSessions(MAX_IDLE_SESSIONS)
                .toConfig();
        final Driver driver = GraphDatabase.driver(neo4jUrl, config);
        return driver;
    }
}
