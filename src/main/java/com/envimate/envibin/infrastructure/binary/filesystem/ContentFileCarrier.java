/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.infrastructure.binary.filesystem;

import java.nio.file.Path;

public interface ContentFileCarrier {
    void transferTo(Path path);
}
