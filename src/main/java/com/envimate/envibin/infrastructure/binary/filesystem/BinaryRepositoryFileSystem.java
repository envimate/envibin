/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.infrastructure.binary.filesystem;

import com.envimate.envibin.domain.binary.BinaryInformation;
import com.envimate.envibin.domain.binary.BinaryKey;
import com.envimate.envibin.domain.binary.BinaryRepository;
import com.envimate.envibin.domain.binary.BinarySize;
import com.envimate.envibin.domain.binary.Md5Checksum;
import com.envimate.envibin.domain.filesystem.ExistingDirectory;
import com.envimate.envibin.domain.filesystem.ExistingFile;
import com.envimate.envibin.domain.filesystem.FileCouldNotBeCreatedException;
import com.envimate.envibin.domain.filesystem.FileCouldNotBeDeletedException;
import com.envimate.envibin.domain.filesystem.NonExistingFile;
import com.envimate.envibin.domain.health.HealthStatus;
import com.envimate.envibin.domain.version.VersionKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.InputStream;

@Component
final class BinaryRepositoryFileSystem implements BinaryRepository {
    private static final String COMPONENT_NAME = "BinaryRepository";
    private final ExistingDirectory existingDirectory;

    @Autowired
    BinaryRepositoryFileSystem(@Qualifier("binaryRepositoryFileSystemDirectory") final ExistingDirectory existingDirectory) {
        this.existingDirectory = existingDirectory;
    }

    @Override
    public BinaryInformation save(final VersionKey versionKey,
                                  final BinaryContentProvider binaryContentProvider)
            throws UnexpectedBinaryRepositoryException {
        final NonExistingFile tempFile = this.existingDirectory.tempFile();
        final ExistingFile existingFile = binaryContentProvider.transferContentTo(tempFile);
        final BinarySize binarySize = existingFile.determineFileSize();
        final Md5Checksum md5Checksum = existingFile.determineFileChecksum();
        final BinaryKey binaryKey = BinaryKey.createNew(versionKey);
        existingFile.rename(binaryKey.internalStringValueForMapping());
        return BinaryInformation.binaryInformation(binaryKey, md5Checksum, binarySize);
    }

    @Override
    public InputStream inputStream(final BinaryKey binaryKey) {
        final ExistingFile existingFile = this.existingDirectory.existingFile(binaryKey.internalStringValueForMapping());
        return existingFile.openInputStream();
    }

    @Override
    public void deleteIfExists(final BinaryKey binaryKey) {
        this.existingDirectory.deleteChildIfExists(binaryKey.internalStringValueForMapping());
    }

    @Override
    public void delete(final BinaryKey binaryKey) {
        final ExistingFile existingFile = this.existingDirectory.existingFile(binaryKey.internalStringValueForMapping());
        existingFile.delete();
    }

    @Override
    public HealthStatus status() {
        final NonExistingFile nonExistingFile = this.existingDirectory.tempFile();
        final ExistingFile healthState;
        try {
            healthState = nonExistingFile.createWithContent("OK");
            healthState.delete();
        } catch (final FileCouldNotBeCreatedException | FileCouldNotBeDeletedException e) {
            return HealthStatus.sick(COMPONENT_NAME, e);
        }
        return HealthStatus.okay(COMPONENT_NAME);
    }
}
