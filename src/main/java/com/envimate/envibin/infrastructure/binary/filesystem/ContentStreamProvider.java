/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.infrastructure.binary.filesystem;

import java.io.IOException;
import java.io.InputStream;

public interface ContentStreamProvider {
    @SuppressWarnings("RedundantThrows")
    InputStream stream() throws IOException;
}
