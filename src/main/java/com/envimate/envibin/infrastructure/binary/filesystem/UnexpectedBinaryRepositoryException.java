/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.infrastructure.binary.filesystem;

public final class UnexpectedBinaryRepositoryException extends RuntimeException {
    private static final long serialVersionUID = 1705082228863516561L;

    private UnexpectedBinaryRepositoryException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public static UnexpectedBinaryRepositoryException binaryRepositoryException(final String message, final Throwable cause) {
        return new UnexpectedBinaryRepositoryException(message, cause);
    }
}
