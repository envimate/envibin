/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.infrastructure.binary.filesystem;

import com.envimate.envibin.domain.binary.BinaryCarrier;
import com.envimate.envibin.domain.filesystem.ExistingFile;
import com.envimate.envibin.domain.filesystem.NonExistingFile;
import com.envimate.envibin.domain.validators.NotNullValidator;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class BinaryContentProvider {
    private final ContentStreamProvider contentStreamProvider;

    private BinaryContentProvider(final ContentStreamProvider contentStreamProvider) {
        this.contentStreamProvider = contentStreamProvider;
    }

    public static BinaryContentProvider binaryContentProvider(final ContentStreamProvider contentStreamProvider) {
        NotNullValidator.validateNotNull(contentStreamProvider, "contentStreamProvider");
        return new BinaryContentProvider(contentStreamProvider);
    }

    @SuppressWarnings("unused")
    public void streamTo(final OutputStream outputStream) throws IOException {
        BinaryCarrier.transfer(this.contentStreamProvider.stream(), outputStream);
    }

    public ExistingFile transferContentTo(final NonExistingFile target) {
        try (InputStream stream = this.contentStreamProvider.stream()) {
            return target.createWithContent(stream);
        } catch (final IOException e) {
            throw new UnsupportedOperationException(
                    String.format("Could not obtain input stream from content stream provider to create file: %s", target), e);
        }
    }
}
