/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.infrastructure.binary.filesystem;

import com.envimate.envibin.domain.binary.BinaryKey;
import com.envimate.envibin.domain.validators.NotNullValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Component
final class BinaryRepositoryFileSystemDirectory {
    private final Path path;

    @Autowired
    BinaryRepositoryFileSystemDirectory(@Qualifier("binaryRepositoryStoragePath") final String path) {
        NotNullValidator.validateNotNull(path, "path");
        final Path pathInstance = Paths.get(path);
        if (!Files.exists(pathInstance)) {
            final String errorMessage = String.format("'%s' does not exist and thus cannot be used " +
                    "as storage for the binary repository", path);
            throw new IllegalArgumentException(errorMessage);
        }
        if (!Files.isDirectory(pathInstance)) {
            final String errorMessage = String.format("'%s' is not a directory and thus cannot be used " +
                    "as storage for the binary repository", path);
            throw new IllegalArgumentException(errorMessage);
        }
        this.path = Paths.get(path);
    }

    Path pathOfBinaryFile(final BinaryKey binaryKey) {
        final Path binaryFilePath = this.path.resolve(binaryKey.internalStringValueForMapping());
        if (Files.exists(binaryFilePath) && !Files.isRegularFile(binaryFilePath)) {
            final String errorMessage = String.format("The binary file path '%s' exists, but is not a regular file. " +
                    "This implementation will not mess with the file system in an unexpected state." +
                    "Backup the contents of the path in questions, delete it and try again.", binaryFilePath);
            throw new IllegalStateException(errorMessage);
        }
        return binaryFilePath;
    }

    Path tempFile() {
        final String uuid = UUID.randomUUID().toString();
        final Path binaryFilePath = this.path.resolve(uuid);
        if (Files.exists(binaryFilePath)) {
            final String errorMessage = String.format("The temporary file path '%s' exists even though it was built using a " +
                    "random uuid. ", binaryFilePath);
            throw new IllegalStateException(errorMessage);
        }
        return binaryFilePath;
    }
}
