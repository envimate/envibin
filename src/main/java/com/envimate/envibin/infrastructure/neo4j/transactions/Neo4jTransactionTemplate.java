/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.infrastructure.neo4j.transactions;

import com.envimate.envibin.domain.health.HealthStatus;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;
import java.util.function.Function;

@Component
public final class Neo4jTransactionTemplate {
    private static final String COMPONENT_NAME = "Neo4jConnection";
    private final Driver ne4jDriver;

    @Autowired
    public Neo4jTransactionTemplate(final Driver ne4jDriver) {
        this.ne4jDriver = ne4jDriver;
    }

    @SuppressWarnings("UnusedReturnValue")
    public <ReturnType> ReturnType withinTransaction(final TransactionalServiceCall<ReturnType> call,
                                                     final Consumer<Exception> onFailure) {
        try {
            return withinTransaction(call);
        } catch (final Exception e) {
            try {
                onFailure.accept(e);
            } catch (final Exception failureHandlingException) {
                e.addSuppressed(failureHandlingException);
            }
            throw e;
        }
    }

    public <ReturnType> ReturnType withinTransaction(final TransactionalServiceCall<ReturnType> call) {
        return usingManagedTransactionResources(transaction -> {
            try {
                final ReturnType returnValue = call.withinTransaction(new Neo4j(transaction));
                commit(transaction);
                return returnValue;
            } catch (final Throwable throwable) {
                transaction.failure();
                throw throwable;
            }
        });
    }

    public HealthStatus status() {
        try (Session session = this.ne4jDriver.session();
             Transaction transaction = session.beginTransaction()) {
            final String statement = "CALL dbms.functions();";
            transaction.run(statement);
            return HealthStatus.okay(COMPONENT_NAME);
        } catch (final Exception e) {
            return HealthStatus.sick(COMPONENT_NAME, e);
        }
    }

    private <ReturnType> ReturnType usingManagedTransactionResources(
            final Function<Transaction, ReturnType> transactionConsumer) {
        try (Session session = this.ne4jDriver.session();
             Transaction transaction = session.beginTransaction()) {
            return transactionConsumer.apply(transaction);
        }
    }

    private void commit(final Transaction transaction) {
        transaction.success();
        transaction.close();
    }
}

