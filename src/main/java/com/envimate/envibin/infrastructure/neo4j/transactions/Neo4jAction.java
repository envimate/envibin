/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.infrastructure.neo4j.transactions;

import org.neo4j.driver.v1.StatementRunner;

interface Neo4jAction<ReturnType> {
    ReturnType runStatement(StatementRunner statementRunner);
}
