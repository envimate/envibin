/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.infrastructure.neo4j.transactions;

public interface TransactionalServiceCall<ReturnType> {
    ReturnType withinTransaction(Neo4j neo4j);
}
