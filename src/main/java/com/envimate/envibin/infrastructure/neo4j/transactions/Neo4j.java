/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.infrastructure.neo4j.transactions;

import org.neo4j.driver.v1.Statement;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.Transaction;
import org.neo4j.driver.v1.Value;

import java.util.Map;

@SuppressWarnings("UnusedReturnValue")
public final class Neo4j {
    private final Transaction transaction;

    Neo4j(final Transaction transaction) {
        this.transaction = transaction;
    }

    public StatementResult run(final String statementTemplate, final Value parameters) {
        return this.transaction.run(statementTemplate, parameters);
    }

    public StatementResult run(final String statementTemplate, final Map<String, Object> statementParameters) {
        return this.transaction.run(statementTemplate, statementParameters);
    }

    public StatementResult run(final String statementTemplate) {
        return doStuff(runner -> runner.run(statementTemplate));
    }

    public StatementResult run(final Statement statement) {
        return doStuff(runner -> runner.run(statement));
    }

    private <ReturnType> ReturnType doStuff(final Neo4jAction<ReturnType> neo4jAction) {
        return neo4jAction.runStatement(this.transaction);
    }
}
