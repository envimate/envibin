/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.tags;

import com.envimate.envibin.usecases.support.AbstractTest;
import com.envimate.envibin.usecases.support.ListVersionsUseCaseBuilder;
import com.envimate.envibin.usecases.support.TagVersionUseCaseBuilder;
import com.envimate.envibin.usecases.support.UploadUseCaseBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

class TagsTest extends AbstractTest {
    private static final int NOT_FOUND = 404;
    @Autowired
    private UploadUseCaseBuilder uploadUseCaseBuilder;
    @Autowired
    private ListVersionsUseCaseBuilder listVersionsUseCaseBuilder;
    @Autowired
    private TagVersionUseCaseBuilder tagVersionUseCaseBuilder;

    @Test
    void tagArtifact() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .execute();
        this.tagVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingTags("latest")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .execute()
                .assertContainsVersion("1.0.0")
                .taggedWith("latest");
    }

    @Test
    void tagArtifactWithMultipleTags() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .execute();
        this.tagVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingTags("latest", "stable")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .execute()
                .assertContainsVersion("1.0.0")
                .taggedWith("latest", "stable");
    }

    @Test
    void tagMultipleArtifactsWithSameTag() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial-docker")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello Docker")
                .execute();
        this.tagVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingTags("latest", "stable")
                .execute();
        this.tagVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial-docker")
                .usingVersionNumber("1.0.0")
                .usingTags("latest", "stable")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .execute()
                .assertContainsVersion("1.0.0")
                .taggedWith("latest", "stable");
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial-docker")
                .execute()
                .assertContainsVersion("1.0.0")
                .taggedWith("latest", "stable");
    }

    @Test
    void tagNonExistentArtifact() {
        this.tagVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingTags("latest")
                .expectingResponseCodeNotFound()
                .expectingErrorMessage("version not found")
                .execute();
    }

    @Test
    void tagNonExistentVersion() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .execute();
        this.tagVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.1")
                .usingTags("latest")
                .expectingResponseCodeNotFound()
                .expectingErrorMessage("version not found")
                .execute();
    }

    @Test
    void tagArtifactWithoutTags() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .execute();
        this.tagVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingTags()
                .expectingResponseCodeBadRequest()
                .expectingErrorMessage("Required TagName[] parameter 'tag' is not present")
                .execute();
    }

    @Test
    void removeTagFromArtifact() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingTags("latest", "stable")
                .execute();
        this.tagVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .remove()
                .usingTags("stable")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .execute()
                .assertContainsVersion("1.0.0")
                .taggedWith("latest");
    }

    @Test
    void removeMultipleTagsFromArtifact() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingTags("latest", "stable", "beta")
                .execute();
        this.tagVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .remove()
                .usingTags("stable", "beta")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .execute()
                .assertContainsVersion("1.0.0")
                .taggedWith("latest");
    }

    @Test
    void removeNonExistentTagFromArtifact() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingTags("latest", "stable", "beta")
                .execute();
        this.tagVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .remove()
                .usingTags("stable", "alpha")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .execute()
                .assertContainsVersion("1.0.0")
                .taggedWith("latest", "beta");
    }
}
