/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.labels;

import com.envimate.envibin.usecases.support.AbstractTest;
import com.envimate.envibin.usecases.support.LabelVersionUseCaseBuilder;
import com.envimate.envibin.usecases.support.ListVersionsUseCaseBuilder;
import com.envimate.envibin.usecases.support.UploadUseCaseBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

class LabelsTest extends AbstractTest {
    @Autowired
    private UploadUseCaseBuilder uploadUseCaseBuilder;
    @Autowired
    private ListVersionsUseCaseBuilder listVersionsUseCaseBuilder;
    @Autowired
    private LabelVersionUseCaseBuilder labelVersionUseCaseBuilder;

    @Test
    void labelArtifact() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .execute();
        this.labelVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .addingLabels("build-success")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .execute()
                .assertContainsVersion("1.0.0")
                .labeledWith("build-success");
    }

    @Test
    void labelArtifactWithMultipleLabels() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .execute();
        this.labelVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .addingLabels("build-success", "test-success")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .execute()
                .assertContainsVersion("1.0.0")
                .labeledWith("build-success", "test-success");
    }

    @Test
    void labelMultipleArtifactsWithSameLabels() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial-docker")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .execute();
        this.labelVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .addingLabels("build-success", "test-success")
                .execute();
        this.labelVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial-docker")
                .usingVersionNumber("1.0.0")
                .addingLabels("build-success", "test-success")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .execute()
                .assertContainsVersion("1.0.0")
                .labeledWith("build-success", "test-success");
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial-docker")
                .execute()
                .assertContainsVersion("1.0.0")
                .labeledWith("build-success", "test-success");
    }

    @Test
    void labelNonExistentArtifact() {
        this.labelVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .addingLabels("build-success")
                .expectingResponseCodeNotFound()
                .expectingErrorMessage("version not found")
                .execute();
    }

    @Test
    void labelArtifactWithoutTags() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .execute();
        this.labelVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .addingLabels()
                .expectingResponseCodeBadRequest()
                .expectingErrorMessage("Required LabelName[] parameter 'label' is not present")
                .execute();
    }

    @Test
    void removeLabelFromArtifact() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingLabels("build-success", "test-success")
                .execute();
        this.labelVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .removingLabels("test-success")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .execute()
                .assertContainsVersion("1.0.0")
                .labeledWith("build-success");
    }

    @Test
    void removeMultipleLabelsFromArtifact() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingLabels("build-success", "test-success", "pre-deploy")
                .execute();
        this.labelVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .removingLabels("test-success", "pre-deploy")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .execute()
                .assertContainsVersion("1.0.0")
                .labeledWith("build-success");
    }

    @Test
    void removeLabelsFromArtifactGivenMultipleArtifactsWithSameLabelsExist() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial-docker")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .execute();
        this.labelVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .addingLabels("build-success", "test-success")
                .execute();
        this.labelVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial-docker")
                .usingVersionNumber("1.0.0")
                .addingLabels("build-success", "test-success")
                .execute();
        this.labelVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .removingLabels("build-success", "test-success")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .execute()
                .assertContainsVersion("1.0.0")
                .labeledWith();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial-docker")
                .execute()
                .assertContainsVersion("1.0.0")
                .labeledWith("build-success", "test-success");
    }

    @Test
    void removeNonExistentLabelFromArtifact() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingLabels("build-success", "test-success", "pre-deploy")
                .execute();
        this.labelVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .removingLabels("test-success", "alpha")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .execute()
                .assertContainsVersion("1.0.0")
                .labeledWith("build-success", "pre-deploy");
    }
}
