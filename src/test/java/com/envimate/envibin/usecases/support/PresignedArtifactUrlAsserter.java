/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.support;

import org.junit.jupiter.api.Assertions;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.StandardCharsets;

@SuppressWarnings("UnusedReturnValue")
public final class PresignedArtifactUrlAsserter {
    private final String urlString;

    PresignedArtifactUrlAsserter(final String urlString) {
        this.urlString = urlString;
    }

    public PresignedArtifactUrlAsserter assertDownloadedContentIs(final String content) {
        final URL url = parseUrl();
        final String actualContent;
        try (ReadableByteChannel readableByteChannel = Channels.newChannel(url.openStream());
             ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
             WritableByteChannel writableByteChannel = Channels.newChannel(byteArrayOutputStream)) {
            fastChannelCopy(readableByteChannel, writableByteChannel);
            actualContent = new String(byteArrayOutputStream.toByteArray(), StandardCharsets.UTF_8);
        } catch (final IOException e) {
            throw new UnsupportedOperationException("Could not download content from url " + this.urlString, e);
        }
        Assertions.assertEquals(content, actualContent, "download content");
        return this;
    }

    private URL parseUrl() {
        try {
            return new URL(this.urlString);
        } catch (final MalformedURLException e) {
            throw new UnsupportedOperationException("Invalid download url: " + this.urlString, e);
        }
    }

    private static void fastChannelCopy(final ReadableByteChannel src, final WritableByteChannel dest) throws IOException {
        final ByteBuffer buffer = ByteBuffer.allocateDirect(16 * 1024);
        while (src.read(buffer) != -1) {
            // prepare the buffer to be drained
            buffer.flip();
            // write to the channel, may block
            dest.write(buffer);
            // If partial transfer, shift remainder down
            // If buffer is empty, same as doing clear()
            buffer.compact();
        }
        // EOF will leave buffer in fill state
        buffer.flip();
        // make sure the buffer is fully drained.
        while (buffer.hasRemaining()) {
            dest.write(buffer);
        }
    }
}
