/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.support;

import com.envimate.envibin.usecases.support.restclient.HttpRequester;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public final class DownloadVersionUseCaseBuilder {
    private final HttpRequester httpRequester;
    private String artifactName;
    private String versionNumber;
    private String tagName;
    private String versionNumberOrTagName;

    @Autowired
    public DownloadVersionUseCaseBuilder(final HttpRequester httpRequester) {
        this.httpRequester = httpRequester;
    }

    public DownloadVersionUseCaseBuilder usingArtifactName(final String artifactName) {
        this.artifactName = artifactName;
        return this;
    }

    public DownloadVersionUseCaseBuilder usingVersionNumber(final String versionNumber) {
        this.versionNumber = versionNumber;
        return this;
    }

    public DownloadVersionUseCaseBuilder usingTagName(final String tagName) {
        this.tagName = tagName;
        return this;
    }

    public DownloadVersionUseCaseBuilder usingVersionNumberOrTagName(final String versionNumberOrTagName) {
        this.versionNumberOrTagName = versionNumberOrTagName;
        return this;
    }

    public DownloadVersionUseCaseBuilder usingExpectedResponseCodeNotFound() {
        this.httpRequester.expectingResponseCodeNotFound();
        return this;
    }

    public DownloadVersionUseCaseBuilder usingExpectedResponseCodeMethodNotAllowed() {
        this.httpRequester.expectingResponseCodeMethodNotAllowed();
        return this;
    }

    public DownloadVersionUseCaseBuilder usingExpectedResponseCodeOk() {
        this.httpRequester.expectingResponseCodeOk();
        return this;
    }

    public DownloadedVersionAsserter execute() {
        this.httpRequester.addUrlPathSegment(this.artifactName);
        if (this.versionNumber != null) {
            ensureNull(this.tagName);
            ensureNull(this.versionNumberOrTagName);
            this.httpRequester.addUrlPathSegment("versions");
            this.httpRequester.addUrlPathSegment(this.versionNumber);
        } else if (this.tagName != null) {
            ensureNull(this.versionNumberOrTagName);
            this.httpRequester.addUrlPathSegment("tags");
            this.httpRequester.addUrlPathSegment(this.tagName);
        } else {
            this.httpRequester.addUrlPathSegment(this.versionNumberOrTagName);
        }
        final String response = this.httpRequester.requestString(HttpMethod.GET);
        reset();
        return DownloadedVersionAsserter.createDownloadedVersionAsserter(response);
    }

    private void reset() {
        this.artifactName = null;
        this.tagName = null;
        this.versionNumber = null;
        this.versionNumberOrTagName = null;
    }

    private void ensureNull(final String value) {
        if (value != null) {
            throw new UnsupportedOperationException("Can only operate on one of tagName, versionNumber or " +
                    "versionNumberOrTagName - ensure you are only setting one of them, in this case it seems " + value +
                    " should not be set.");
        }
    }
}
