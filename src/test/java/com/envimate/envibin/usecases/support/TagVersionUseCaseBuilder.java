/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.support;

import com.envimate.envibin.usecases.support.restclient.HttpRequester;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public final class TagVersionUseCaseBuilder {
    private String artifactName;
    private String versionNumber;
    private boolean removeFlag;
    private final HttpRequester httpRequester;

    public TagVersionUseCaseBuilder(final HttpRequester httpRequester) {
        this.httpRequester = httpRequester;
    }

    public TagVersionUseCaseBuilder usingArtifactName(final String artifactName) {
        this.artifactName = artifactName;
        return this;
    }

    public TagVersionUseCaseBuilder usingVersionNumber(final String versionNumber) {
        this.versionNumber = versionNumber;
        return this;
    }

    public TagVersionUseCaseBuilder usingTags(final String... tags) {
        this.httpRequester.addUrlParameters("tag", tags);
        return this;
    }

    public TagVersionUseCaseBuilder remove() {
        this.removeFlag = true;
        return this;
    }

    public TagVersionUseCaseBuilder expectingResponseCodeBadRequest() {
        this.httpRequester.expectingResponseCodeBadRequest();
        return this;
    }

    public TagVersionUseCaseBuilder expectingResponseCodeNotFound() {
        this.httpRequester.expectingResponseCodeNotFound();
        return this;
    }

    public TagVersionUseCaseBuilder expectingErrorMessage(final String errorMessage) {
        this.httpRequester.expectingErrorMessage(errorMessage);
        return this;
    }

    public void execute() {
        HttpMethod method = HttpMethod.PUT;
        if (this.removeFlag) {
            method = HttpMethod.DELETE;
        }
        this.httpRequester.addUrlPathSegment(this.artifactName);
        this.httpRequester.addUrlPathSegment("versions");
        this.httpRequester.addUrlPathSegment(this.versionNumber);
        this.httpRequester.addUrlPathSegment("tags");
        this.httpRequester.requestString(method);
    }
}
