/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.support;

import com.envimate.envibin.domain.filesystem.ExistingDirectory;
import com.envimate.envibin.domain.filesystem.ExistingFile;

import static com.envimate.envibin.MavenProjectDirectory.mavenProjectDirectory;

public final class TestBinaries {
    private static final ExistingDirectory BINARIES_DIRECTORY = mavenProjectDirectory()
            .subDirectory("src", "test", "resources", "binaries");

    private TestBinaries() {
    }

    public static ExistingFile ubuntuXenialVirtualbox150MbBox() {
        return BINARIES_DIRECTORY.existingFile("ubuntu-xenial_virtualbox_150Mb.box");
    }

    public static ExistingFile ubuntuXenialVirtualboxBox() {
        return BINARIES_DIRECTORY.existingFile("ubuntu-xenial_virtualbox.box");
    }

    public static ExistingFile ubuntuXenialVmwareDesktopBox() {
        return BINARIES_DIRECTORY.existingFile("ubuntu-xenial_vmware-desktop.box");
    }
}
