/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.support.restclient;

import com.envimate.envibin.domain.url.ExternalHostAddress;
import com.envimate.envibin.domain.url.ExternalPort;
import com.envimate.envibin.domain.url.ExternalProtocol;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Component
@Scope("prototype")
final class HttpRequestUrlBuilder {
    private final ExternalProtocol externalProtocol;
    private final ExternalHostAddress externalHostAddress;
    private final ExternalPort externalPort;
    private UriComponentsBuilder uriComponentsBuilder;

    @Autowired
    HttpRequestUrlBuilder(final ExternalProtocol externalProtocol,
                          final ExternalHostAddress externalHostAddress,
                          final ExternalPort externalPort) {
        this.externalProtocol = externalProtocol;
        this.externalHostAddress = externalHostAddress;
        this.externalPort = externalPort;
        reset();
    }

    void addUrlParameters(final String name, final String... values) {
        for (final String value : values) {
            this.uriComponentsBuilder.queryParam(name, value);
        }
    }

    void addUrlPathSegment(final String segment) {
        this.uriComponentsBuilder.pathSegment(segment);
    }

    URI buildUri() {
        final URI uri = this.uriComponentsBuilder.build(true).toUri();
        reset();
        return uri;
    }

    private void reset() {
        this.uriComponentsBuilder = UriComponentsBuilder.newInstance()
                .scheme(this.externalProtocol.lookupAsString())
                .host(this.externalHostAddress.lookupAsString())
                .port(this.externalPort.lookupAsInteger());
    }
}
