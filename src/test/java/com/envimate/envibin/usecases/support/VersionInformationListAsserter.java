/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.support;

import com.envimate.envibin.service.versions.list.VersionInformation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings("UnusedReturnValue")
public final class VersionInformationListAsserter {
    private final List<VersionInformation> versionInformationList;

    VersionInformationListAsserter(final List<VersionInformation> versionInformationList) {
        this.versionInformationList = new ArrayList<>(versionInformationList);
    }

    public VersionInformationListAsserter assertArtifactNameIs(final String expected) {
        assertThat(this.versionInformationList)
                .extracting("artifactName")
                .containsOnly(expected);
        return this;
    }

    public VersionInformationListAsserter ignoringEveryVersionThatDoesNotBelongToTheArtifacts(final String... artifactNames) {
        final List<String> artifactNamesWhitelist = Arrays.asList(artifactNames);
        final List<VersionInformation> filtered = this.versionInformationList.stream()
                .filter(versionInformationItem -> artifactNamesWhitelist.contains(versionInformationItem.getArtifactName()))
                .collect(Collectors.toList());
        return new VersionInformationListAsserter(filtered);
    }

    public VersionInformationAsserter assertContainsOneVersionOfArtifact(final String artifactName) {
        assertThat(this.versionInformationList)
                .extracting("artifactName")
                .containsOnlyOnce(artifactName);
        final VersionInformation versionInformation = this.versionInformationList.stream()
                .filter(versionInformationItem -> versionInformationItem.getArtifactName().equals(artifactName))
                .findFirst().orElseThrow(UnsupportedOperationException::new);
        return new VersionInformationAsserter(this, versionInformation);
    }

    public VersionInformationAsserter assertContainsVersion(final String versionNumber) {
        assertThat(this.versionInformationList)
                .extracting("versionNumber")
                .contains(versionNumber);
        final VersionInformation versionInformation = this.versionInformationList.stream()
                .filter(versionInformationItem -> versionInformationItem.getVersionNumber().equals(versionNumber))
                .findFirst().orElseThrow(UnsupportedOperationException::new);
        return new VersionInformationAsserter(this, versionInformation);
    }

    public VersionInformationListAsserter assertDoesNotContainVersion(final String versionNumber) {
        assertThat(this.versionInformationList)
                .extracting("versionNumber")
                .doesNotContain(versionNumber);
        return this;
    }

    public void assertLengthIs(final int expectedLength) {
        assertThat(this.versionInformationList)
                .hasSize(expectedLength);
    }
}
