/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.support;

import com.envimate.envibin.usecases.support.restclient.HttpRequester;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public final class VagrantUseCaseBuilder {
    private String artifactName;
    private final HttpRequester httpRequester;

    public VagrantUseCaseBuilder(final HttpRequester httpRequester) {
        this.httpRequester = httpRequester;
    }

    public VagrantUseCaseBuilder usingArtifactName(final String artifactName) {
        this.artifactName = artifactName;
        return this;
    }

    public VagrantUseCaseBuilder expectingResponseCodeNotFound() {
        this.httpRequester.expectingResponseCodeNotFound();
        return this;
    }

    public VagrantUseCaseBuilder expectingErrorMessage(final String errorMessage) {
        this.httpRequester.expectingErrorMessage(errorMessage);
        return this;
    }

    public VagrantAsserter execute() {
        this.httpRequester.addUrlPathSegment(this.artifactName);
        this.httpRequester.addUrlPathSegment("vagrant");
        this.httpRequester.addUrlPathSegment("metadata");
        final String response = this.httpRequester.requestString(HttpMethod.GET);
        return new VagrantAsserter(response);
    }
}
