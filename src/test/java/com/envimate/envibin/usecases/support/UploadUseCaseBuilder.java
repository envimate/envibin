/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.support;

import com.envimate.envibin.domain.filesystem.ExistingFile;
import com.envimate.envibin.usecases.support.restclient.HttpRequester;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

@SuppressWarnings("UnusedReturnValue")
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public final class UploadUseCaseBuilder {
    private final HttpRequester httpRequester;
    private String artifactName;
    private String versionNumber;

    @Autowired
    UploadUseCaseBuilder(final HttpRequester httpRequester) {
        this.httpRequester = httpRequester;
    }

    public UploadUseCaseBuilder usingArtifactName(final String artifactName) {
        this.artifactName = artifactName;
        return this;
    }

    public UploadUseCaseBuilder usingVersionNumber(final String versionNumber) {
        this.versionNumber = versionNumber;
        return this;
    }

    public UploadUseCaseBuilder usingBinaryContent(final String binaryContent) {
        if (binaryContent != null) {
            this.httpRequester.setUploadContent(binaryContent);
        }
        return this;
    }

    public UploadUseCaseBuilder usingBinaryFile(final ExistingFile existingFile) {
        this.httpRequester.setUploadContent(existingFile);
        return this;
    }

    public UploadUseCaseBuilder usingLabels(final String... labels) {
        this.httpRequester.addUrlParameters("label", labels);
        return this;
    }

    public UploadUseCaseBuilder usingTags(final String... tags) {
        this.httpRequester.addUrlParameters("tag", tags);
        return this;
    }

    public UploadUseCaseBuilder usingExpectedResponseCodeMethodNotAllowed() {
        this.httpRequester.expectingResponseCodeMethodNotAllowed();
        return this;
    }

    public UploadUseCaseBuilder usingExpectedResponseCodeBadRequest() {
        this.httpRequester.expectingResponseCodeBadRequest();
        return this;
    }

    public UploadUseCaseBuilder usingExpectedErrorMessage(final String errorMessage) {
        this.httpRequester.expectingErrorMessage(errorMessage);
        return this;
    }

    public UploadUseCaseBuilder execute() {
        this.httpRequester.addUrlPathSegment(this.artifactName);
        this.httpRequester.addUrlPathSegment("versions");
        this.httpRequester.addUrlPathSegment(this.versionNumber);
        this.httpRequester.requestString(HttpMethod.PUT);
        return this;
    }
}
