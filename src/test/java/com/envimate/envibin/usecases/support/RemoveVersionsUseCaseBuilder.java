/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.support;

import com.envimate.envibin.usecases.support.restclient.HttpRequester;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public final class RemoveVersionsUseCaseBuilder {
    private final HttpRequester httpRequester;
    private String versionNumber;
    private String artifactName;

    public RemoveVersionsUseCaseBuilder(final HttpRequester httpRequester) {
        this.httpRequester = httpRequester;
    }

    public RemoveVersionsUseCaseBuilder usingArtifactName(final String artifactName) {
        this.artifactName = artifactName;
        return this;
    }

    public RemoveVersionsUseCaseBuilder usingVersionNumber(final String versionNumber) {
        this.versionNumber = versionNumber;
        return this;
    }

    public void execute() {
        this.httpRequester.addUrlPathSegment(this.artifactName);
        if (this.versionNumber != null) {
            this.httpRequester.addUrlPathSegment("versions");
            this.httpRequester.addUrlPathSegment(this.versionNumber);
        }
        this.httpRequester.requestString(HttpMethod.DELETE);
    }
}
