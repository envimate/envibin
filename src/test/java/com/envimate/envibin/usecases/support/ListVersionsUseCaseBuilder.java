/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.support;

import com.envimate.envibin.service.versions.list.VersionInformation;
import com.envimate.envibin.usecases.support.restclient.HttpRequester;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public final class ListVersionsUseCaseBuilder {
    private final HttpRequester httpRequester;
    private String artifactName;

    @Autowired
    public ListVersionsUseCaseBuilder(final HttpRequester httpRequester) {
        this.httpRequester = httpRequester;
    }

    public ListVersionsUseCaseBuilder usingArtifactName(final String artifactName) {
        this.artifactName = artifactName;
        return this;
    }

    public ListVersionsUseCaseBuilder usingTags(final String... tags) {
        this.httpRequester.addUrlParameters("tag", tags);
        return this;
    }

    public ListVersionsUseCaseBuilder usingLabels(final String... labels) {
        this.httpRequester.addUrlParameters("label", labels);
        return this;
    }

    public VersionInformationListAsserter execute() {
        this.httpRequester.addUrlPathSegment(this.artifactName);
        this.httpRequester.addUrlPathSegment("versions");
        final List<VersionInformation> list = this.httpRequester.requestTypedResult(HttpMethod.GET, new ListTypeReference());
        return new VersionInformationListAsserter(list);
    }

    private static final class ListTypeReference extends TypeReference<List<VersionInformation>> {
    }
}
