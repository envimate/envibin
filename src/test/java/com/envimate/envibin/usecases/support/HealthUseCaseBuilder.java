/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.support;

import com.envimate.envibin.service.health.SystemHealth;
import com.envimate.envibin.usecases.support.restclient.HttpRequester;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public final class HealthUseCaseBuilder {
    private final HttpRequester httpRequester;

    public HealthUseCaseBuilder(final HttpRequester httpRequester) {
        this.httpRequester = httpRequester;
    }

    public HeathAsserter execute() {
        this.httpRequester.addUrlPathSegment("health");
        final SystemHealth systemHealth = this.httpRequester.requestTypedResult(HttpMethod.GET, SystemHealth.class);
        return new HeathAsserter(systemHealth);
    }
}
