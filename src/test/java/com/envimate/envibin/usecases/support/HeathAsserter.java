/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.support;

import com.envimate.envibin.MavenProjectDirectory;
import com.envimate.envibin.service.health.SystemHealth;
import org.junit.jupiter.api.Assertions;

import static org.assertj.core.api.Assertions.assertThat;

public final class HeathAsserter {
    private final SystemHealth systemHealth;

    HeathAsserter(final SystemHealth systemHealth) {
        this.systemHealth = systemHealth;
    }

    public HeathAsserter assertContainsCurrentVersion() {
        final MavenProjectDirectory mavenProjectDirectory = MavenProjectDirectory.mavenProjectDirectory();
        final String projectVersion = mavenProjectDirectory.projectVersion();
        Assertions.assertEquals(projectVersion, this.systemHealth.getVersion(), "version");
        return this;
    }

    public HeathAsserter assertHealthy() {
        Assertions.assertEquals(true, this.systemHealth.isHealthy(), "healthy");
        return this;
    }

    public HeathAsserter assertContainsComponentState(final String componentName) {
        assertThat(this.systemHealth.getComponentStatuses())
                .extracting("name")
                .containsOnlyOnce(componentName);
        return this;
    }
}
