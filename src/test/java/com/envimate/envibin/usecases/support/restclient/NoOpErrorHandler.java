/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.support.restclient;

import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;

final class NoOpErrorHandler extends DefaultResponseErrorHandler {
    @Override
    public void handleError(final ClientHttpResponse response) {
    }
}
