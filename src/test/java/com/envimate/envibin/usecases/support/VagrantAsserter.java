/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.support;

import org.junit.jupiter.api.Assertions;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class VagrantAsserter {
    private static final Pattern LINE_BREAKS = Pattern.compile("\n", Pattern.LITERAL);
    private static final Pattern WHITESPACES = Pattern.compile(" ", Pattern.LITERAL);
    private final String actual;

    VagrantAsserter(final String actual) {
        this.actual = actual;
    }

    public void assertMetaDataContentIsEqualTo(final String expected) throws IOException {
        Assertions.assertEquals(readJson(expected), trimJson(this.actual), "download content");
    }

    private String readJson(final String path) throws IOException {
        final Resource stateFile = new ClassPathResource(path);
        final Path resPath = Paths.get(stateFile.getURI());
        final String json = new String(Files.readAllBytes(resPath), "UTF8");
        return trimJson(json);
    }

    private String trimJson(final String json) {
        final Matcher lineBreakMatcher = LINE_BREAKS.matcher(json);
        final String withoutLineBreaks = lineBreakMatcher.replaceAll(Matcher.quoteReplacement(""));
        final Matcher whitespaceMatcher = WHITESPACES.matcher(withoutLineBreaks);
        final String withoutWhiteSpaces = whitespaceMatcher.replaceAll(Matcher.quoteReplacement(""));
        return withoutWhiteSpaces;
    }
}
