/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.support.restclient;

import com.envimate.envibin.domain.filesystem.ExistingFile;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Component
@Scope("prototype")
public final class HttpRequester {
    private final HttpRequestUrlBuilder httpRequestUrlBuilder;
    private final RestTemplate restTemplate;
    private HttpStatus expectedHttpResponseStatus;
    private Resource uploadContent;
    private String expectedHttpResponseErrorMessage;

    @Autowired
    public HttpRequester(final HttpRequestUrlBuilder httpRequestUrlBuilder) {
        this.httpRequestUrlBuilder = httpRequestUrlBuilder;
        this.restTemplate = new RestTemplate();
        this.restTemplate.setErrorHandler(new NoOpErrorHandler());
        reset();
    }

    public void expectingResponseCodeNotFound() {
        this.expectedHttpResponseStatus = HttpStatus.NOT_FOUND;
    }

    public void expectingResponseCodeMethodNotAllowed() {
        this.expectedHttpResponseStatus = HttpStatus.METHOD_NOT_ALLOWED;
    }

    public void expectingResponseCodeBadRequest() {
        this.expectedHttpResponseStatus = HttpStatus.BAD_REQUEST;
    }

    public void expectingResponseCodeOk() {
        this.expectedHttpResponseStatus = HttpStatus.OK;
    }

    public void expectingErrorMessage(final String errorMessage) {
        this.expectedHttpResponseErrorMessage = errorMessage;
    }

    public void setUploadContent(final String content) {
        this.uploadContent = UploadResourceFactory.uploadResourceFrom(content);
    }

    public void setUploadContent(final ExistingFile content) {
        this.uploadContent = UploadResourceFactory.uploadResourceFrom(content);
    }

    public <R> R requestTypedResult(final HttpMethod httpMethod, final TypeReference<R> valueTypeRef) {
        return requestTypedResult(httpMethod, (objectMapper, responseBody) -> objectMapper.readValue(responseBody, valueTypeRef));
    }

    public <R> R requestTypedResult(final HttpMethod httpMethod, final Class<R> targetType) {
        return requestTypedResult(httpMethod, (objectMapper, responseBody) -> objectMapper.readValue(responseBody, targetType));
    }

    private <R> R requestTypedResult(final HttpMethod httpMethod, final ObjectMapperInvoker<R> objectMapperInvoker) {
        final String responseBody = requestString(httpMethod);
        final ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapperInvoker.invoke(objectMapper, responseBody);
        } catch (final IOException e) {
            final String message = String.format("Could not map from response body %s", responseBody);
            throw new UnsupportedOperationException(message, e);
        }
    }

    public void addUrlParameters(final String name, final String... values) {
        this.httpRequestUrlBuilder.addUrlParameters(name, values);
    }

    public void addUrlPathSegment(final String segment) {
        this.httpRequestUrlBuilder.addUrlPathSegment(segment);
    }

    public String requestString(final HttpMethod httpMethod) {
        final URI uri = this.httpRequestUrlBuilder.buildUri();
        final ResponseEntity<String> response;
        if (httpMethod.equals(HttpMethod.GET)) {
            response = this.restTemplate.getForEntity(uri, String.class);
        } else {
            final SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
            requestFactory.setBufferRequestBody(false);
            this.restTemplate.setRequestFactory(requestFactory);
            final HttpHeaders headers = new HttpHeaders();
            final LinkedMultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
            if (this.uploadContent != null) {
                parameters.add("file", this.uploadContent);
                headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            }
            final HttpEntity<LinkedMultiValueMap<String, Object>> entity = new HttpEntity<>(parameters, headers);
            response = this.restTemplate.exchange(
                    uri.toString(),
                    httpMethod,
                    entity,
                    String.class);
        }
        assertEquals(this.expectedHttpResponseStatus, response.getStatusCode(), "response code" + response);
        final String responseBody = response.getBody();
        if (this.expectedHttpResponseErrorMessage != null) {
            assertEquals(this.expectedHttpResponseErrorMessage, extractErrorMessage(responseBody), "error message: " + response);
        }
        reset();
        return responseBody;
    }

    private void reset() {
        this.uploadContent = null;
        this.expectedHttpResponseStatus = HttpStatus.OK;
        this.expectingErrorMessage(null);
    }

    private String extractErrorMessage(final String responseBody) {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            final ObjectNode node = mapper.readValue(responseBody, ObjectNode.class);
            if (node.get("message") != null) {
                return node.get("message").textValue();
            } else {
                return null;
            }
        } catch (final IOException e) {
            throw new UnsupportedOperationException("unexpected error reading root node from json: " + responseBody, e);
        }
    }

    private interface ObjectMapperInvoker<R> {
        R invoke(ObjectMapper objectMapper, String responseBody) throws IOException;
    }
}
