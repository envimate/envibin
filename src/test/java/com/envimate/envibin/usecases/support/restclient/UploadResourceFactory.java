/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.support.restclient;

import com.envimate.envibin.domain.filesystem.ExistingFile;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import java.nio.charset.StandardCharsets;

final class UploadResourceFactory {
    private UploadResourceFactory() {
    }

    static Resource uploadResourceFrom(final String content) {
        return new ByteArrayResource(content.getBytes(StandardCharsets.UTF_8)) {
            @Override
            public String getFilename() {
                return "content";
            }
        };
    }

    static Resource uploadResourceFrom(final ExistingFile content) {
        return new FileSystemResource(content.asAbsolutePathString());
    }
}
