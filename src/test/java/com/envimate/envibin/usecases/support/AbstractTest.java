/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.support;

import com.envimate.envibin.Application;
import com.envimate.envibin.domain.binary.BinaryRepository;
import com.envimate.envibin.domain.version.ArtifactName;
import com.envimate.envibin.domain.version.Version;
import com.envimate.envibin.domain.version.VersionRepository;
import com.envimate.envibin.infrastructure.neo4j.transactions.Neo4jTransactionTemplate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Application.class, webEnvironment = WebEnvironment.DEFINED_PORT)
@TestPropertySource(properties = "server.port = 9000")
public abstract class AbstractTest {
    private static final ArtifactName ARTIFACT_NAME = ArtifactName.artifactName("com.envimate.testdata.");
    @Autowired
    private Neo4jTransactionTemplate neo4jTransactionTemplate;
    @Autowired
    private VersionRepository versionRepository;
    @Autowired
    private BinaryRepository binaryRepository;

    @BeforeEach
    public void before() {
        cleanup();
    }

    private void cleanup() {
        final List<Version> versions = this.neo4jTransactionTemplate.withinTransaction(neo4j -> {
            final List<Version> result = this.versionRepository.byArtifactNameStartsWith(neo4j, ARTIFACT_NAME);
            this.versionRepository.deleteByArtifactNameStartsWith(neo4j, ARTIFACT_NAME);
            return result;
        });
        versions.forEach(version -> this.binaryRepository.deleteIfExists(version.binaryKey()));
    }
}
