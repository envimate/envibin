/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.support;

import org.junit.jupiter.api.Assertions;

@SuppressWarnings("UnusedReturnValue")
public final class DownloadedVersionAsserter {
    private final String content;

    private DownloadedVersionAsserter(final String content) {
        this.content = content;
    }

    static DownloadedVersionAsserter createDownloadedVersionAsserter(final String contentAsString) {
        return new DownloadedVersionAsserter(contentAsString);
    }

    public DownloadedVersionAsserter assertContentEquals(final String expected) {
        Assertions.assertEquals(expected, this.content, "download content");
        return this;
    }
}
