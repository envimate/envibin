/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.support;

import com.envimate.envibin.usecases.support.restclient.HttpRequester;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public final class LabelVersionUseCaseBuilder {
    private final HttpRequester httpRequester;
    private String artifactName;
    private String versionNumber;
    private boolean removeFlag;

    @Autowired
    LabelVersionUseCaseBuilder(final HttpRequester httpRequester) {
        this.httpRequester = httpRequester;
    }

    public LabelVersionUseCaseBuilder usingArtifactName(final String artifactName) {
        this.artifactName = artifactName;
        return this;
    }

    public LabelVersionUseCaseBuilder usingVersionNumber(final String versionNumber) {
        this.versionNumber = versionNumber;
        return this;
    }

    public LabelVersionUseCaseBuilder addingLabels(final String... labels) {
        this.removeFlag = false;
        this.httpRequester.addUrlParameters("label", labels);
        return this;
    }

    public LabelVersionUseCaseBuilder removingLabels(final String... labels) {
        this.removeFlag = true;
        this.httpRequester.addUrlParameters("label", labels);
        return this;
    }

    public LabelVersionUseCaseBuilder expectingResponseCodeNotFound() {
        this.httpRequester.expectingResponseCodeNotFound();
        return this;
    }

    public LabelVersionUseCaseBuilder expectingResponseCodeBadRequest() {
        this.httpRequester.expectingResponseCodeBadRequest();
        return this;
    }

    public LabelVersionUseCaseBuilder expectingErrorMessage(final String errorMessage) {
        this.httpRequester.expectingErrorMessage(errorMessage);
        return this;
    }

    public void execute() {
        HttpMethod method = HttpMethod.PUT;
        if (this.removeFlag) {
            method = HttpMethod.DELETE;
        }
        this.httpRequester.addUrlPathSegment(this.artifactName);
        this.httpRequester.addUrlPathSegment("versions");
        this.httpRequester.addUrlPathSegment(this.versionNumber);
        this.httpRequester.addUrlPathSegment("labels");
        this.httpRequester.requestString(method);
    }
}
