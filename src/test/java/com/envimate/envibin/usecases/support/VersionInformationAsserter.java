/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.support;

import com.envimate.envibin.service.versions.list.VersionInformation;

import static org.assertj.core.api.Assertions.assertThat;

public final class VersionInformationAsserter {
    private final VersionInformation versionInformation;
    private final VersionInformationListAsserter versionInformationListAsserter;

    VersionInformationAsserter(final VersionInformationListAsserter versionInformationListAsserter,
                               final VersionInformation versionInformation) {
        this.versionInformationListAsserter = versionInformationListAsserter;
        this.versionInformation = versionInformation;
    }

    public VersionInformationAsserter withVersionNumber(final String versionNumber) {
        assertThat(this.versionInformation.getVersionNumber()).isEqualTo(versionNumber);
        return this;
    }

    public VersionInformationAsserter labeledWith(final String... labels) {
        assertThat(this.versionInformation.getLabels())
                .containsExactlyInAnyOrder(labels);
        return this;
    }

    public VersionInformationAsserter taggedWith(final String... tags) {
        assertThat(this.versionInformation.getTags())
                .containsExactlyInAnyOrder(tags);
        return this;
    }

    public VersionInformationListAsserter and() {
        return this.versionInformationListAsserter;
    }
}
