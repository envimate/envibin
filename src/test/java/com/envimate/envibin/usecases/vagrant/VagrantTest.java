/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.vagrant;

import com.envimate.envibin.usecases.support.AbstractTest;
import com.envimate.envibin.usecases.support.TestBinaries;
import com.envimate.envibin.usecases.support.UploadUseCaseBuilder;
import com.envimate.envibin.usecases.support.VagrantUseCaseBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

class VagrantTest extends AbstractTest {
    @Autowired
    private UploadUseCaseBuilder uploadUseCaseBuilder;
    @Autowired
    private VagrantUseCaseBuilder vagrantUseCaseBuilder;

    @Test
    void getVagrantMetaData() throws IOException {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0_-_virtualbox")
                .usingBinaryFile(TestBinaries.ubuntuXenialVirtualboxBox())
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0_-_vmware_desktop")
                .usingBinaryFile(TestBinaries.ubuntuXenialVmwareDesktopBox())
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.1_-_virtualbox")
                .usingBinaryFile(TestBinaries.ubuntuXenialVirtualboxBox())
                .execute();
        this.vagrantUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .execute()
                .assertMetaDataContentIsEqualTo("responses/vagrant/getVagrantMetaDataExpectedResponse.json");
    }

    @Test
    void getVagrantMetaDataWhenNoArtifactsExist() {
        this.vagrantUseCaseBuilder
                .usingArtifactName("non-existent-artifact")
                .expectingResponseCodeNotFound()
                .expectingErrorMessage("non-existent-artifact not found")
                .execute();
    }

    @Test
    void getVagrantMetaDataWhenNoVersionsSuffixed() throws IOException {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-precision")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello world!")
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-precision")
                .usingVersionNumber("1.0.1")
                .usingBinaryContent("Hello world!")
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-precision")
                .usingVersionNumber("1.0.2")
                .usingBinaryContent("Hello world!")
                .execute();
        this.vagrantUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-precision")
                .execute()
                .assertMetaDataContentIsEqualTo(
                        "responses/vagrant/getVagrantMetaDataWhenNoVersionsSuffixedExpectedResponse.json");
    }
}
