/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.versions.presigned;

import com.envimate.envibin.usecases.support.AbstractTest;
import com.envimate.envibin.usecases.support.PresignedArtifactUrlUseCaseBuilder;
import com.envimate.envibin.usecases.support.UploadUseCaseBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

class PresignedArtifactUrlTest extends AbstractTest {
    @Autowired
    private PresignedArtifactUrlUseCaseBuilder presignedArtifactUrlUseCaseBuilder;
    @Autowired
    private UploadUseCaseBuilder uploadUseCaseBuilder;

    @Test
    void getPresignedArtifactUrlVersionNumber() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello world - 1.0.0")
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("2.0.0-SNAPSHOT")
                .usingBinaryContent("Hello world - 2.0.0-SNAPSHOT")
                .execute();
        this.presignedArtifactUrlUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .execute()
                .assertDownloadedContentIs("Hello world - 1.0.0");
        this.presignedArtifactUrlUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("2.0.0-SNAPSHOT")
                .execute()
                .assertDownloadedContentIs("Hello world - 2.0.0-SNAPSHOT");
    }

    @Test
    void getPresignedArtifactUrlTag() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingTags("stable")
                .usingBinaryContent("Hello world - 1.0.0")
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("2.0.0-SNAPSHOT")
                .usingTags("dev")
                .usingBinaryContent("Hello world - 2.0.0-SNAPSHOT")
                .execute();
        this.presignedArtifactUrlUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingTagName("stable")
                .execute()
                .assertDownloadedContentIs("Hello world - 1.0.0");
        this.presignedArtifactUrlUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingTagName("dev")
                .execute()
                .assertDownloadedContentIs("Hello world - 2.0.0-SNAPSHOT");
    }

    @Test
    void getPresignedArtifactUrlTagOrVersionNumberVersionNumber() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingTags("stable")
                .usingBinaryContent("Hello world - 1.0.0")
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("2.0.0-SNAPSHOT")
                .usingTags("dev")
                .usingBinaryContent("Hello world - 2.0.0-SNAPSHOT")
                .execute();
        this.presignedArtifactUrlUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumberOrTagName("1.0.0")
                .execute()
                .assertDownloadedContentIs("Hello world - 1.0.0");
        this.presignedArtifactUrlUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumberOrTagName("2.0.0-SNAPSHOT")
                .execute()
                .assertDownloadedContentIs("Hello world - 2.0.0-SNAPSHOT");
    }

    @Test
    void getPresignedArtifactUrlTagOrVersionNumberTagName() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingTags("stable")
                .usingBinaryContent("Hello world - 1.0.0")
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("2.0.0-SNAPSHOT")
                .usingTags("dev")
                .usingBinaryContent("Hello world - 2.0.0-SNAPSHOT")
                .execute();
        this.presignedArtifactUrlUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumberOrTagName("stable")
                .execute()
                .assertDownloadedContentIs("Hello world - 1.0.0");
        this.presignedArtifactUrlUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumberOrTagName("dev")
                .execute()
                .assertDownloadedContentIs("Hello world - 2.0.0-SNAPSHOT");
    }

    @Test
    void versionNumberNotFoundReturns404ByVersionNumber() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello world!")
                .execute();
        this.presignedArtifactUrlUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.1")
                .usingExpectedResponseCodeNotFound()
                .execute();
    }

    @Test
    void versionNumberNotFoundReturns404ByTagName() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingTags("stable")
                .usingBinaryContent("Hello world!")
                .execute();
        this.presignedArtifactUrlUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingTagName("development")
                .usingExpectedResponseCodeNotFound()
                .execute();
    }

    @Test
    void versionNumberNotFoundReturns404ByVersionNumberTagName() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingTags("stable")
                .usingBinaryContent("Hello world!")
                .execute();
        this.presignedArtifactUrlUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumberOrTagName("development")
                .usingExpectedResponseCodeNotFound()
                .execute();
        this.presignedArtifactUrlUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumberOrTagName("1.0.1")
                .usingExpectedResponseCodeNotFound()
                .execute();
    }

    @Test
    void artifactNameNotFoundReturns404ByVersionNumber() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello world!")
                .execute();
        this.presignedArtifactUrlUseCaseBuilder
                .usingArtifactName(UUID.randomUUID().toString())
                .usingVersionNumber("1.0.0")
                .usingExpectedResponseCodeNotFound()
                .execute();
    }

    @Test
    void artifactNameNotFoundReturns404ByTagName() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingTags("stable")
                .usingBinaryContent("Hello world!")
                .execute();
        this.presignedArtifactUrlUseCaseBuilder
                .usingArtifactName(UUID.randomUUID().toString())
                .usingTagName("stable")
                .usingExpectedResponseCodeNotFound()
                .execute();
    }

    @Test
    void artifactNameNotFoundReturns404ByVersionNumberOrTagName() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingTags("stable")
                .usingBinaryContent("Hello world!")
                .execute();
        this.presignedArtifactUrlUseCaseBuilder
                .usingArtifactName(UUID.randomUUID().toString())
                .usingVersionNumberOrTagName("1.0.0")
                .usingExpectedResponseCodeNotFound()
                .execute();
        this.presignedArtifactUrlUseCaseBuilder
                .usingArtifactName(UUID.randomUUID().toString())
                .usingVersionNumberOrTagName("stable")
                .usingExpectedResponseCodeNotFound()
                .execute();
    }

    @Test
    void missingArtifactNameByVersionNumber() {
        this.presignedArtifactUrlUseCaseBuilder
                .usingArtifactName("")
                .usingVersionNumber("1.0.0")
                .usingExpectedResponseCodeNotFound()
                .execute();
    }

    @Test
    void missingArtifactNameByTagName() {
        this.presignedArtifactUrlUseCaseBuilder
                .usingArtifactName("")
                .usingTagName("stable")
                .usingExpectedResponseCodeNotFound()
                .execute();
    }

    @Test
    void missingArtifactNameByVersionNumberOrTagName() {
        this.presignedArtifactUrlUseCaseBuilder
                .usingArtifactName("")
                .usingVersionNumberOrTagName("stable")
                .usingExpectedResponseCodeNotFound()
                .execute();
    }

    @Test
    void missingVersionNumber() {
        this.presignedArtifactUrlUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("")
                .usingExpectedResponseCodeNotFound()
                .execute();
    }

    @Test
    void missingTagName() {
        this.presignedArtifactUrlUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingTagName("")
                .usingExpectedResponseCodeNotFound()
                .execute();
    }

    @Test
    void missingVersionNumberOrTagName() {
        this.presignedArtifactUrlUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumberOrTagName("")
                .usingExpectedResponseCodeNotFound()
                .execute();
    }
}
