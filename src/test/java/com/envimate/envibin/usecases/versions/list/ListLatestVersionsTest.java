/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.versions.list;

import com.envimate.envibin.usecases.support.AbstractTest;
import com.envimate.envibin.usecases.support.ListLatestVersionsUseCaseBuilder;
import com.envimate.envibin.usecases.support.UploadUseCaseBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

class ListLatestVersionsTest extends AbstractTest {
    @Autowired
    private UploadUseCaseBuilder uploadUseCaseBuilder;
    @Autowired
    private ListLatestVersionsUseCaseBuilder listLatestVersionsUseCaseBuilder;

    @Test
    void listAllArtifactsWithLatestVersions() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0-SNAPSHOT")
                .usingBinaryContent("Hello World")
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.docker")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.docker")
                .usingVersionNumber("0.9.0")
                .usingBinaryContent("Hello World")
                .execute();
        this.listLatestVersionsUseCaseBuilder
                .execute()
                .ignoringEveryVersionThatDoesNotBelongToTheArtifacts(
                        "com.envimate.testdata.ubuntu-xenial.base",
                        "com.envimate.testdata.ubuntu-xenial.docker")
                .assertContainsOneVersionOfArtifact("com.envimate.testdata.ubuntu-xenial.base")
                .withVersionNumber("1.0.0")
                .and()
                .assertContainsOneVersionOfArtifact("com.envimate.testdata.ubuntu-xenial.docker")
                .withVersionNumber("0.9.0")
                .and()
                .assertLengthIs(2);
    }
}
