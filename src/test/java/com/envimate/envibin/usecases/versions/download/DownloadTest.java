/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.versions.download;

import com.envimate.envibin.usecases.support.AbstractTest;
import com.envimate.envibin.usecases.support.DownloadVersionUseCaseBuilder;
import com.envimate.envibin.usecases.support.UploadUseCaseBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

class DownloadTest extends AbstractTest {
    @Autowired
    private UploadUseCaseBuilder uploadUseCaseBuilder;
    @Autowired
    private DownloadVersionUseCaseBuilder downloadVersionUseCaseBuilder;

    @Test
    void downloadsDistinctVersionByVersionNumber() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello world - 1.0.0")
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("2.0.0-SNAPSHOT")
                .usingBinaryContent("Hello world - 2.0.0-SNAPSHOT")
                .execute();
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .execute()
                .assertContentEquals("Hello world - 1.0.0");
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("2.0.0-SNAPSHOT")
                .execute()
                .assertContentEquals("Hello world - 2.0.0-SNAPSHOT");
    }

    @Test
    void downloadsDistinctVersionByTag() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingTags("stable")
                .usingBinaryContent("Hello world - 1.0.0")
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("2.0.0-SNAPSHOT")
                .usingTags("dev")
                .usingBinaryContent("Hello world - 2.0.0-SNAPSHOT")
                .execute();
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingTagName("stable")
                .execute()
                .assertContentEquals("Hello world - 1.0.0");
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingTagName("dev")
                .execute()
                .assertContentEquals("Hello world - 2.0.0-SNAPSHOT");
    }

    @Test
    void downloadsDistinctVersionByTagOrVersionNumberVersionNumber() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingTags("stable")
                .usingBinaryContent("Hello world - 1.0.0")
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("2.0.0-SNAPSHOT")
                .usingTags("dev")
                .usingBinaryContent("Hello world - 2.0.0-SNAPSHOT")
                .execute();
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumberOrTagName("1.0.0")
                .execute()
                .assertContentEquals("Hello world - 1.0.0");
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumberOrTagName("2.0.0-SNAPSHOT")
                .execute()
                .assertContentEquals("Hello world - 2.0.0-SNAPSHOT");
    }

    @Test
    void downloadsDistinctVersionByTagOrVersionNumberTagName() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingTags("stable")
                .usingBinaryContent("Hello world - 1.0.0")
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("2.0.0-SNAPSHOT")
                .usingTags("dev")
                .usingBinaryContent("Hello world - 2.0.0-SNAPSHOT")
                .execute();
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumberOrTagName("stable")
                .execute()
                .assertContentEquals("Hello world - 1.0.0");
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumberOrTagName("dev")
                .execute()
                .assertContentEquals("Hello world - 2.0.0-SNAPSHOT");
    }

    @Test
    void versionNumberNotFoundReturns404ByVersionNumber() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello world!")
                .execute();
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.1")
                .usingExpectedResponseCodeNotFound()
                .execute();
    }

    @Test
    void versionNumberNotFoundReturns404ByTagName() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingTags("stable")
                .usingBinaryContent("Hello world!")
                .execute();
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingTagName("development")
                .usingExpectedResponseCodeNotFound()
                .execute();
    }

    @Test
    void versionNumberNotFoundReturns404ByVersionNumberTagName() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingTags("stable")
                .usingBinaryContent("Hello world!")
                .execute();
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumberOrTagName("development")
                .usingExpectedResponseCodeNotFound()
                .execute();
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumberOrTagName("1.0.1")
                .usingExpectedResponseCodeNotFound()
                .execute();
    }

    @Test
    void artifactNameNotFoundReturns404ByVersionNumber() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello world!")
                .execute();
        this.downloadVersionUseCaseBuilder
                .usingArtifactName(UUID.randomUUID().toString())
                .usingVersionNumber("1.0.0")
                .usingExpectedResponseCodeNotFound()
                .execute();
    }

    @Test
    void artifactNameNotFoundReturns404ByTagName() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingTags("stable")
                .usingBinaryContent("Hello world!")
                .execute();
        this.downloadVersionUseCaseBuilder
                .usingArtifactName(UUID.randomUUID().toString())
                .usingTagName("stable")
                .usingExpectedResponseCodeNotFound()
                .execute();
    }

    @Test
    void artifactNameNotFoundReturns404ByVersionNumberOrTagName() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("1.0.0")
                .usingTags("stable")
                .usingBinaryContent("Hello world!")
                .execute();
        this.downloadVersionUseCaseBuilder
                .usingArtifactName(UUID.randomUUID().toString())
                .usingVersionNumberOrTagName("1.0.0")
                .usingExpectedResponseCodeNotFound()
                .execute();
        this.downloadVersionUseCaseBuilder
                .usingArtifactName(UUID.randomUUID().toString())
                .usingVersionNumberOrTagName("stable")
                .usingExpectedResponseCodeNotFound()
                .execute();
    }

    @Test
    void missingArtifactNameByVersionNumber() {
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("")
                .usingVersionNumber("1.0.0")
                .usingExpectedResponseCodeNotFound()
                .execute();
    }

    @Test
    void missingArtifactNameByTagName() {
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("")
                .usingTagName("stable")
                .usingExpectedResponseCodeNotFound()
                .execute();
    }

    @Test
    void missingArtifactNameByVersionNumberOrTagName() {
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("")
                .usingVersionNumberOrTagName("stable")
                .usingExpectedResponseCodeMethodNotAllowed()
                .execute();
    }

    @Test
    void missingVersionNumber() {
        //this will result in a list versions call
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumber("")
                .usingExpectedResponseCodeOk()
                .execute();
    }

    @Test
    void missingTagName() {
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingTagName("")
                .usingExpectedResponseCodeNotFound()
                .execute();
    }

    @Test
    void missingVersionNumberOrTagName() {
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial")
                .usingVersionNumberOrTagName("")
                .usingExpectedResponseCodeMethodNotAllowed()
                .execute();
    }
}
