/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.versions.upload;

import com.envimate.envibin.usecases.support.AbstractTest;
import com.envimate.envibin.usecases.support.DownloadVersionUseCaseBuilder;
import com.envimate.envibin.usecases.support.ListVersionsUseCaseBuilder;
import com.envimate.envibin.usecases.support.TestBinaries;
import com.envimate.envibin.usecases.support.UploadUseCaseBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

class UploadTest extends AbstractTest {
    @Autowired
    private UploadUseCaseBuilder uploadUseCaseBuilder;
    @Autowired
    private ListVersionsUseCaseBuilder listVersionsUseCaseBuilder;
    @Autowired
    private DownloadVersionUseCaseBuilder downloadVersionUseCaseBuilder;

    @Test
    void missingArtifactName() {
        this.uploadUseCaseBuilder
                .usingArtifactName("")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingExpectedResponseCodeMethodNotAllowed()
                .execute();
    }

    @Test
    void missingVersionNumber() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("")
                .usingBinaryContent("Hello World")
                .usingExpectedResponseCodeMethodNotAllowed()
                .execute();
    }

    @Test
    void missingBinaryContent() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent(null)
                .usingExpectedResponseCodeBadRequest()
                .usingExpectedErrorMessage("Current request is not a multipart request")
                .execute();
    }

    @Test
    void uploadArtifact() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0");
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .execute()
                .assertContentEquals("Hello World");
    }

    @Test
    void uploadLargeArtifact() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.2")
                .usingBinaryFile(TestBinaries.ubuntuXenialVirtualbox150MbBox())
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.2");
    }

    @Test
    void uploadArtifactUpdatesContent() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World!")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0");
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .execute()
                .assertContentEquals("Hello World!");
    }

    @Test
    void uploadArtifactWithLabel() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingLabels("built-successfully")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0")
                .labeledWith("built-successfully");
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingLabels("built-successfully")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0")
                .labeledWith("built-successfully");
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .execute()
                .assertContentEquals("Hello World");
    }

    @Test
    void uploadArtifactWithLabels() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingLabels("tests-passed", "built-successfully")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0")
                .labeledWith("built-successfully", "tests-passed");
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingLabels("tests-passed")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0")
                .labeledWith("built-successfully", "tests-passed");
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingLabels("built-successfully")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0")
                .labeledWith("built-successfully", "tests-passed");
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .execute()
                .assertContentEquals("Hello World");
    }

    @Test
    void uploadArtifactWithLabelsUpdatesLabels() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingLabels("tests-passed", "built-successfully")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0")
                .labeledWith("built-successfully", "tests-passed");
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingLabels("built-successfully")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0")
                .labeledWith("built-successfully");
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingLabels()
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0")
                .labeledWith();
    }

    @Test
    void uploadArtifactWithUnderscoreSnapshot() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0_SNAPSHOT")
                .usingBinaryContent("Hello")
                .usingLabels()
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0_SNAPSHOT");
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0_SNAPSHOT")
                .execute()
                .assertContentEquals("Hello");
    }

    @Test
    void uploadArtifactWithLabelsActuallyFilters() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0-SNAPSHOT")
                .usingBinaryContent("Hello")
                .usingLabels()
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingLabels("built-successfully")
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.1")
                .usingBinaryContent("Hello World!")
                .usingLabels("tests-passed", "built-successfully")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0-SNAPSHOT")
                .and()
                .assertContainsVersion("1.0.0")
                .labeledWith("built-successfully")
                .and()
                .assertContainsVersion("1.0.1")
                .labeledWith("built-successfully", "tests-passed");
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingLabels("built-successfully")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0")
                .labeledWith("built-successfully")
                .and()
                .assertContainsVersion("1.0.1")
                .labeledWith("built-successfully", "tests-passed");
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingLabels("tests-passed")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.1")
                .labeledWith("built-successfully", "tests-passed");
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0-SNAPSHOT")
                .execute()
                .assertContentEquals("Hello");
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .execute()
                .assertContentEquals("Hello World");
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.1")
                .execute()
                .assertContentEquals("Hello World!");
    }

    @Test
    void uploadArtifactWithTag() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingTags("current-staging")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0")
                .taggedWith("current-staging");
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .execute()
                .assertContentEquals("Hello World");
    }

    @Test
    void uploadArtifactWithTags() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingTags("current-staging", "current-dev")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0")
                .taggedWith("current-dev", "current-staging");
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .execute()
                .assertContentEquals("Hello World");
    }

    @Test
    void uploadArtifactWithTagsUpdatesTagsRemovesOneTag() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingTags("current-staging", "current-dev")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0")
                .taggedWith("current-dev", "current-staging");
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .execute()
                .assertContentEquals("Hello World");
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingTags("current-staging")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0")
                .taggedWith("current-staging");
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .execute()
                .assertContentEquals("Hello World");
    }

    @Test
    void uploadArtifactWithTagsUpdatesTagsRemovesAllTags() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingTags("current-staging", "current-dev")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0")
                .taggedWith("current-dev", "current-staging");
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .execute()
                .assertContentEquals("Hello World");
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingTags()
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0")
                .taggedWith();
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .execute()
                .assertContentEquals("Hello World");
    }

    @Test
    void uploadArtifactWithTagsUpdatesTagsReplacesVersionsOfTags() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingTags("current-staging", "current-dev")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0")
                .taggedWith("current-dev", "current-staging");
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .execute()
                .assertContentEquals("Hello World");
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.1-Nune")
                .usingBinaryContent("Your king loves you mor than anything else in the world! - Gooey")
                .usingTags("current-staging", "current-dev")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0")
                .taggedWith();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.1-Nune")
                .taggedWith("current-dev", "current-staging");
        this.downloadVersionUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.1-Nune")
                .execute()
                .assertContentEquals("Your king loves you mor than anything else in the world! - Gooey");
    }
}
