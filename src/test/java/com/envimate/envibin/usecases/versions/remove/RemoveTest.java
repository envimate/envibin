/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.versions.remove;

import com.envimate.envibin.usecases.support.AbstractTest;
import com.envimate.envibin.usecases.support.ListVersionsUseCaseBuilder;
import com.envimate.envibin.usecases.support.RemoveVersionsUseCaseBuilder;
import com.envimate.envibin.usecases.support.UploadUseCaseBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

class RemoveTest extends AbstractTest {
    private static final int THREE = 3;
    @Autowired
    private UploadUseCaseBuilder uploadUseCaseBuilder;
    @Autowired
    private ListVersionsUseCaseBuilder listVersionsUseCaseBuilder;
    @Autowired
    private RemoveVersionsUseCaseBuilder removeVersionsUseCaseBuilder;

    @Test
    void removeArtifactVersion() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.1")
                .usingBinaryContent("Hello World")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute()
                .assertLengthIs(2);
        this.removeVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.1")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute()
                .assertLengthIs(1);
    }

    @Test
    void removeArtifact() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.1")
                .usingBinaryContent("Hello World")
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.2")
                .usingBinaryContent("Hello World")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute()
                .assertLengthIs(THREE);
        this.removeVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute()
                .assertLengthIs(0);
    }
}
