/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.versions.list;

import com.envimate.envibin.usecases.support.AbstractTest;
import com.envimate.envibin.usecases.support.ListVersionsUseCaseBuilder;
import com.envimate.envibin.usecases.support.UploadUseCaseBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

class ListVersionsTest extends AbstractTest {
    @Autowired
    private UploadUseCaseBuilder uploadUseCaseBuilder;
    @Autowired
    private ListVersionsUseCaseBuilder listVersionsUseCaseBuilder;

    @Test
    void listVersionsOfArtifact() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingTags()
                .usingLabels()
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingTags()
                .usingLabels()
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0")
                .labeledWith()
                .taggedWith();
    }

    @Test
    void listVersionsOfArtifactStartingWithTheSameName() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingTags()
                .usingLabels()
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base.some-name")
                .usingVersionNumber("1.0.1")
                .usingBinaryContent("Hello World")
                .usingTags()
                .usingLabels()
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingTags()
                .usingLabels()
                .execute()
                .assertDoesNotContainVersion("1.0.1")
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0")
                .labeledWith()
                .taggedWith();
    }

    @Test
    void listVersionsOfArtifactWithLabels() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("0.0.9-SNAPSHOT")
                .usingBinaryContent("Hello World")
                .usingTags()
                .usingLabels()
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0-SNAPSHOT")
                .usingBinaryContent("Hello World")
                .usingTags()
                .usingLabels("built-success")
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingTags("current-staging")
                .usingLabels("built-success", "test-success")
                .execute();
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.docker")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingTags()
                .usingLabels("built-success", "test-success")
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingTags()
                .usingLabels("built-success")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0")
                .labeledWith("built-success", "test-success")
                .taggedWith("current-staging")
                .and()
                .assertDoesNotContainVersion("0.0.9-SNAPSHOT");
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingTags()
                .usingLabels("test-success")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0")
                .labeledWith("built-success", "test-success")
                .taggedWith("current-staging")
                .and()
                .assertDoesNotContainVersion("1.0.0-SNAPSHOT");
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingTags()
                .usingLabels("built-success", "test-success")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0")
                .labeledWith("built-success", "test-success")
                .taggedWith("current-staging")
                .and()
                .assertDoesNotContainVersion("1.0.0-SNAPSHOT");
    }

    @Test
    void listVersionsOfArtifactWithTags() {
        this.uploadUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .usingVersionNumber("1.0.0")
                .usingBinaryContent("Hello World")
                .usingTags("current-staging", "current-production")
                .usingLabels()
                .execute();
        this.listVersionsUseCaseBuilder
                .usingArtifactName("com.envimate.testdata.ubuntu-xenial.base")
                .execute()
                .assertArtifactNameIs("com.envimate.testdata.ubuntu-xenial.base")
                .assertContainsVersion("1.0.0")
                .labeledWith()
                .taggedWith("current-production", "current-staging");
    }
}
