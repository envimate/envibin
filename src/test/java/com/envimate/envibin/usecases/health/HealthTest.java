/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin.usecases.health;

import com.envimate.envibin.usecases.support.AbstractTest;
import com.envimate.envibin.usecases.support.HealthUseCaseBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

class HealthTest extends AbstractTest {
    @Autowired
    private HealthUseCaseBuilder healthUseCaseBuilder;

    @Test
    void health() {
        this.healthUseCaseBuilder
                .execute()
                .assertHealthy()
                .assertContainsCurrentVersion()
                .assertContainsComponentState("BinaryRepository")
                .assertContainsComponentState("Neo4jConnection")
                .assertContainsComponentState("VersionRepository");
    }
}
