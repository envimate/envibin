/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin;

import com.envimate.envibin.domain.filesystem.ExistingDirectory;
import com.envimate.envibin.usecases.support.UploadUseCaseBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;

import javax.annotation.PostConstruct;
import java.util.HashMap;

@Configuration
@ComponentScan(basePackageClasses = UploadUseCaseBuilder.class)
public class EnvibinTestProperties {
    @Autowired
    private ConfigurableEnvironment configurableEnvironment;
    @Autowired
    private ApplicationContext applicationContext;

    @PostConstruct
    public void addDynamicAppConfigProperties() {
        final MavenProjectDirectory mavenProjectDirectory = MavenProjectDirectory.mavenProjectDirectory();
        final ExistingDirectory binaryRepositoryPath =
                mavenProjectDirectory.subDirectory("playground", "data", "binary-repository");
        final String stringPath = binaryRepositoryPath.asAbsolutePathString();
        final HashMap<String, Object> properties = new HashMap<>(2);
        properties.put("binaries.path", stringPath);
        properties.put("neo4j.url", "bolt://localhost");
        properties.put("external.address", "localhost");
        properties.put("external.protocol", "http");
        final MutablePropertySources propertySources = this.configurableEnvironment.getPropertySources();
        propertySources.addFirst(new MapPropertySource("dynamicTestProperties", properties) {
            @Override
            public Object getProperty(final String name) {
                if ("external.port".equals(name)) {
                    final Environment environment = EnvibinTestProperties.this.applicationContext.getEnvironment();
                    final String property = environment.getProperty("local.server.port");
                    return property;
                } else {
                    return super.getProperty(name);
                }
            }
        });
    }
}
