/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin;

import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class RealTimeTrackingExtension implements BeforeAllCallback, AfterAllCallback {
    private static final Logger LOGGER = LoggerFactory.getLogger(RealTimeTrackingExtension.class);
    private Long start;

    @Override
    public void beforeAll(final ExtensionContext context) {
        this.start = System.currentTimeMillis();
        LOGGER.info("Test suite started");
    }

    @Override
    public void afterAll(final ExtensionContext context) {
        final long duration = System.currentTimeMillis() - this.start;
        LOGGER.info("Test suite finished. Duration: {}ms", duration);
    }
}
