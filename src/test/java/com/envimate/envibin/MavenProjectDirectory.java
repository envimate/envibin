/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.envibin;

import com.envimate.envibin.domain.filesystem.ExistingDirectory;
import com.envimate.envibin.domain.filesystem.ExistingFile;
import com.envimate.envibin.domain.filesystem.FileSystemRootDirectoryReachedException;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;

import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.security.CodeSource;
import java.security.ProtectionDomain;

public final class MavenProjectDirectory {
    private final ExistingDirectory path;

    private MavenProjectDirectory(final ExistingDirectory path) {
        this.path = path;
    }

    public static MavenProjectDirectory mavenProjectDirectory() {
        final ExistingDirectory classesDirectory = pathOfThisClassFile();
        final ExistingDirectory projectDirectory;
        ExistingDirectory tmp = classesDirectory.parentDirectory();
        try {
            while (true) {
                if (tmp.asFile().toPath().getFileName().toString().equals("target")) {
                    projectDirectory = tmp.parentDirectory();
                    break;
                } else {
                    tmp = tmp.parentDirectory();
                }
            }
        } catch (final FileSystemRootDirectoryReachedException e) {
            throw new UnsupportedOperationException("Could not find the target subdirectory in the path of this " +
                    "classes binary class directory(" + classesDirectory + ")", e);
        }
        return new MavenProjectDirectory(projectDirectory);
    }

    public ExistingDirectory subDirectory(final String... subDirectories) {
        return ExistingDirectory.forPath(Paths.get(this.path.asAbsolutePathString(), subDirectories));
    }

    public String projectVersion() {
        final ExistingFile existingFile = this.path.existingFile("pom.xml");
        final MavenXpp3Reader reader = new MavenXpp3Reader();
        try {
            final Model model = reader.read(new FileReader(existingFile.asAbsolutePathString()));
            return model.getVersion();
        } catch (IOException | XmlPullParserException e) {
            throw new UnsupportedOperationException("Unexpected error reading project pom", e);
        }
    }

    private static ExistingDirectory pathOfThisClassFile() {
        final Class<MavenProjectDirectory> mavenProjectDirectoryClass = MavenProjectDirectory.class;
        final ProtectionDomain protectionDomain = mavenProjectDirectoryClass.getProtectionDomain();
        final CodeSource codeSource = protectionDomain.getCodeSource();
        final URL codeSourceLocation = codeSource.getLocation();
        final URI classesDirectoryUri;
        try {
            classesDirectoryUri = codeSourceLocation.toURI();
        } catch (final URISyntaxException e) {
            throw new UnsupportedOperationException("Could not convert codeSourceLocation to URI", e);
        }
        return ExistingDirectory.forPath(Paths.get(classesDirectoryUri));
    }

    @Override
    public String toString() {
        return "MavenProjectDirectory{" +
                "path='" + this.path + '\'' +
                '}';
    }
}
